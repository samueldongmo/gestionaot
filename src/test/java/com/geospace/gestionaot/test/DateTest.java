package com.geospace.gestionaot.test;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.junit.Assert;
import org.junit.Test;

public class DateTest {

	@Test
	public void testDateDiffMonths() {
		LocalDate fromDate = LocalDate.now();
		LocalDate toDateFuture = LocalDate.now().plusMonths(2);
		
		LocalDate toDatePass = LocalDate.now().minusDays(92);
		
		long between = ChronoUnit.MONTHS.between(fromDate, toDateFuture);
		Assert.assertEquals(between, 2);
		
		long between2 = ChronoUnit.MONTHS.between(fromDate, toDatePass);
		Assert.assertEquals(between2, -3);
	}
	

	@Test
	public void testDateDiffDays() {
		LocalDate fromDate = LocalDate.now();
		LocalDate toDateFuture = LocalDate.now().plusDays(2);
		
		LocalDate toDatePass = LocalDate.now().minusDays(92);
		
		long between = ChronoUnit.DAYS.between(fromDate, toDateFuture);
		Assert.assertEquals(between, 2);
		
		long between2 = ChronoUnit.DAYS.between(fromDate, toDatePass);
		Assert.assertEquals(between2, -92);
	}

	@Test
	public void testDateDiffDaysEpoch() {
		LocalDate fromDate = LocalDate.now();
		LocalDate toDateFuture = LocalDate.now().plusDays(2);
		
		LocalDate toDatePass = LocalDate.now().minusDays(92);
		long between = fromDate.toEpochDay() - toDateFuture.toEpochDay();
		Assert.assertEquals(between, -2);
		
		long between2 = fromDate.toEpochDay() - toDatePass.toEpochDay();
		Assert.assertEquals(between2, 92);
	}
}
