package com.geospace.gestionaot.web.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.geospace.gestionaot.GestionaotApp;
import com.geospace.gestionaot.domain.Echeancier;
import com.geospace.gestionaot.repository.EcheancierRepository;
import com.geospace.gestionaot.service.EcheancierService;
import com.geospace.gestionaot.web.rest.EcheancierResource;
import com.geospace.gestionaot.web.rest.errors.ExceptionTranslator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EcheancierResource REST controller.
 *
 * @see EcheancierResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GestionaotApp.class)
public class EcheancierResourceIntTest {

    private static final Long DEFAULT_ID_AMODIATAIRE = 1L;
    private static final Long UPDATED_ID_AMODIATAIRE = 2L;

    private static final String DEFAULT_REF = "AAAAAAAAAA";
    private static final String UPDATED_REF = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_MONTANT_HT = new BigDecimal(1);
    private static final BigDecimal UPDATED_MONTANT_HT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_MONTANT = new BigDecimal(1);
    private static final BigDecimal UPDATED_MONTANT = new BigDecimal(2);

    private static final LocalDate DEFAULT_DATE_DEBUT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_DEBUT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_FIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FIN = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private EcheancierRepository echeancierRepository;

    @Autowired
    private EcheancierService echeancierService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEcheancierMockMvc;

    private Echeancier echeancier;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        EcheancierResource echeancierResource = new EcheancierResource(echeancierService);
        this.restEcheancierMockMvc = MockMvcBuilders.standaloneSetup(echeancierResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Echeancier createEntity(EntityManager em) {
        Echeancier echeancier = new Echeancier()
            .idAmodiataire(DEFAULT_ID_AMODIATAIRE)
            .ref(DEFAULT_REF)
            .montantHT(DEFAULT_MONTANT_HT)
            .montant(DEFAULT_MONTANT)
            .dateDebut(DEFAULT_DATE_DEBUT)
            .dateFin(DEFAULT_DATE_FIN);
        return echeancier;
    }

    @Before
    public void initTest() {
        echeancier = createEntity(em);
    }

    @Test
    @Transactional
    public void createEcheancier() throws Exception {
        int databaseSizeBeforeCreate = echeancierRepository.findAll().size();

        // Create the Echeancier
        restEcheancierMockMvc.perform(post("/api/echeanciers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(echeancier)))
            .andExpect(status().isCreated());

        // Validate the Echeancier in the database
        List<Echeancier> echeancierList = echeancierRepository.findAll();
        assertThat(echeancierList).hasSize(databaseSizeBeforeCreate + 1);
        Echeancier testEcheancier = echeancierList.get(echeancierList.size() - 1);
        assertThat(testEcheancier.getIdAmodiataire()).isEqualTo(DEFAULT_ID_AMODIATAIRE);
        assertThat(testEcheancier.getRef()).isEqualTo(DEFAULT_REF);
        assertThat(testEcheancier.getMontantHT()).isEqualTo(DEFAULT_MONTANT_HT);
        assertThat(testEcheancier.getMontant()).isEqualTo(DEFAULT_MONTANT);
        assertThat(testEcheancier.getDateDebut()).isEqualTo(DEFAULT_DATE_DEBUT);
        assertThat(testEcheancier.getDateFin()).isEqualTo(DEFAULT_DATE_FIN);
    }

    @Test
    @Transactional
    public void createEcheancierWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = echeancierRepository.findAll().size();

        // Create the Echeancier with an existing ID
        echeancier.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEcheancierMockMvc.perform(post("/api/echeanciers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(echeancier)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Echeancier> echeancierList = echeancierRepository.findAll();
        assertThat(echeancierList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkIdAmodiataireIsRequired() throws Exception {
        int databaseSizeBeforeTest = echeancierRepository.findAll().size();
        // set the field null
        echeancier.setIdAmodiataire(null);

        // Create the Echeancier, which fails.

        restEcheancierMockMvc.perform(post("/api/echeanciers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(echeancier)))
            .andExpect(status().isBadRequest());

        List<Echeancier> echeancierList = echeancierRepository.findAll();
        assertThat(echeancierList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEcheanciers() throws Exception {
        // Initialize the database
        echeancierRepository.saveAndFlush(echeancier);

        // Get all the echeancierList
        restEcheancierMockMvc.perform(get("/api/echeanciers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(echeancier.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAmodiataire").value(hasItem(DEFAULT_ID_AMODIATAIRE.intValue())))
            .andExpect(jsonPath("$.[*].ref").value(hasItem(DEFAULT_REF.toString())))
            .andExpect(jsonPath("$.[*].montantHT").value(hasItem(DEFAULT_MONTANT_HT.intValue())))
            .andExpect(jsonPath("$.[*].montant").value(hasItem(DEFAULT_MONTANT.intValue())))
            .andExpect(jsonPath("$.[*].dateDebut").value(hasItem(DEFAULT_DATE_DEBUT.toString())))
            .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())));
    }

    @Test
    @Transactional
    public void getEcheancier() throws Exception {
        // Initialize the database
        echeancierRepository.saveAndFlush(echeancier);

        // Get the echeancier
        restEcheancierMockMvc.perform(get("/api/echeanciers/{id}", echeancier.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(echeancier.getId().intValue()))
            .andExpect(jsonPath("$.idAmodiataire").value(DEFAULT_ID_AMODIATAIRE.intValue()))
            .andExpect(jsonPath("$.ref").value(DEFAULT_REF.toString()))
            .andExpect(jsonPath("$.montantHT").value(DEFAULT_MONTANT_HT.intValue()))
            .andExpect(jsonPath("$.montant").value(DEFAULT_MONTANT.intValue()))
            .andExpect(jsonPath("$.dateDebut").value(DEFAULT_DATE_DEBUT.toString()))
            .andExpect(jsonPath("$.dateFin").value(DEFAULT_DATE_FIN.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingEcheancier() throws Exception {
        // Get the echeancier
        restEcheancierMockMvc.perform(get("/api/echeanciers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEcheancier() throws Exception {
        // Initialize the database
        echeancierService.save(echeancier);

        int databaseSizeBeforeUpdate = echeancierRepository.findAll().size();

        // Update the echeancier
        Echeancier updatedEcheancier = echeancierRepository.findOne(echeancier.getId());
        updatedEcheancier
            .idAmodiataire(UPDATED_ID_AMODIATAIRE)
            .ref(UPDATED_REF)
            .montantHT(UPDATED_MONTANT_HT)
            .montant(UPDATED_MONTANT)
            .dateDebut(UPDATED_DATE_DEBUT)
            .dateFin(UPDATED_DATE_FIN);

        restEcheancierMockMvc.perform(put("/api/echeanciers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEcheancier)))
            .andExpect(status().isOk());

        // Validate the Echeancier in the database
        List<Echeancier> echeancierList = echeancierRepository.findAll();
        assertThat(echeancierList).hasSize(databaseSizeBeforeUpdate);
        Echeancier testEcheancier = echeancierList.get(echeancierList.size() - 1);
        assertThat(testEcheancier.getIdAmodiataire()).isEqualTo(UPDATED_ID_AMODIATAIRE);
        assertThat(testEcheancier.getRef()).isEqualTo(UPDATED_REF);
        assertThat(testEcheancier.getMontantHT()).isEqualTo(UPDATED_MONTANT_HT);
        assertThat(testEcheancier.getMontant()).isEqualTo(UPDATED_MONTANT);
        assertThat(testEcheancier.getDateDebut()).isEqualTo(UPDATED_DATE_DEBUT);
        assertThat(testEcheancier.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
    }

    @Test
    @Transactional
    public void updateNonExistingEcheancier() throws Exception {
        int databaseSizeBeforeUpdate = echeancierRepository.findAll().size();

        // Create the Echeancier

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEcheancierMockMvc.perform(put("/api/echeanciers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(echeancier)))
            .andExpect(status().isCreated());

        // Validate the Echeancier in the database
        List<Echeancier> echeancierList = echeancierRepository.findAll();
        assertThat(echeancierList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteEcheancier() throws Exception {
        // Initialize the database
        echeancierService.save(echeancier);

        int databaseSizeBeforeDelete = echeancierRepository.findAll().size();

        // Get the echeancier
        restEcheancierMockMvc.perform(delete("/api/echeanciers/{id}", echeancier.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Echeancier> echeancierList = echeancierRepository.findAll();
        assertThat(echeancierList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Echeancier.class);
    }
}
