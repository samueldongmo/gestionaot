package com.geospace.gestionaot.web.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.geospace.gestionaot.GestionaotApp;
import com.geospace.gestionaot.domain.Echeance;
import com.geospace.gestionaot.repository.EcheanceRepository;
import com.geospace.gestionaot.service.EcheanceService;
import com.geospace.gestionaot.web.rest.EcheanceResource;
import com.geospace.gestionaot.web.rest.errors.ExceptionTranslator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EcheanceResource REST controller.
 *
 * @see EcheanceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GestionaotApp.class)
public class EcheanceResourceIntTest {

    private static final Long DEFAULT_ID_ECHEANCIER = 1L;
    private static final Long UPDATED_ID_ECHEANCIER = 2L;

    private static final Long DEFAULT_ID_AMODIATAIRE = 1L;
    private static final Long UPDATED_ID_AMODIATAIRE = 2L;

    private static final Long DEFAULT_ID_FACTURE = 1L;
    private static final Long UPDATED_ID_FACTURE = 2L;

    private static final String DEFAULT_REF = "AAAAAAAAAA";
    private static final String UPDATED_REF = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_MONTAN_HT = new BigDecimal(1);
    private static final BigDecimal UPDATED_MONTAN_HT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_MONTANT = new BigDecimal(1);
    private static final BigDecimal UPDATED_MONTANT = new BigDecimal(2);

    private static final LocalDate DEFAULT_DATE_DEBUT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_DEBUT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_FIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FIN = LocalDate.now(ZoneId.systemDefault());

    private static final Long DEFAULT_ID_REGLEMENT = 1L;
    private static final Long UPDATED_ID_REGLEMENT = 2L;

    private static final Boolean DEFAULT_EST_FACTURE = false;
    private static final Boolean UPDATED_EST_FACTURE = true;

    private static final Boolean DEFAULT_EST_REGLE = false;
    private static final Boolean UPDATED_EST_REGLE = true;

    @Autowired
    private EcheanceRepository echeanceRepository;

    @Autowired
    private EcheanceService echeanceService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEcheanceMockMvc;

    private Echeance echeance;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        EcheanceResource echeanceResource = new EcheanceResource(echeanceService);
        this.restEcheanceMockMvc = MockMvcBuilders.standaloneSetup(echeanceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Echeance createEntity(EntityManager em) {
        Echeance echeance = new Echeance()
            .idEcheancier(DEFAULT_ID_ECHEANCIER)
            .idAmodiataire(DEFAULT_ID_AMODIATAIRE)
            .idFacture(DEFAULT_ID_FACTURE)
            .ref(DEFAULT_REF)
            .montanHT(DEFAULT_MONTAN_HT)
            .montant(DEFAULT_MONTANT)
            .dateDebut(DEFAULT_DATE_DEBUT)
            .dateFin(DEFAULT_DATE_FIN)
            .idReglement(DEFAULT_ID_REGLEMENT)
            .estFacture(DEFAULT_EST_FACTURE)
            .estRegle(DEFAULT_EST_REGLE);
        return echeance;
    }

    @Before
    public void initTest() {
        echeance = createEntity(em);
    }

    @Test
    @Transactional
    public void createEcheance() throws Exception {
        int databaseSizeBeforeCreate = echeanceRepository.findAll().size();

        // Create the Echeance
        restEcheanceMockMvc.perform(post("/api/echeances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(echeance)))
            .andExpect(status().isCreated());

        // Validate the Echeance in the database
        List<Echeance> echeanceList = echeanceRepository.findAll();
        assertThat(echeanceList).hasSize(databaseSizeBeforeCreate + 1);
        Echeance testEcheance = echeanceList.get(echeanceList.size() - 1);
        assertThat(testEcheance.getIdEcheancier()).isEqualTo(DEFAULT_ID_ECHEANCIER);
        assertThat(testEcheance.getIdAmodiataire()).isEqualTo(DEFAULT_ID_AMODIATAIRE);
        assertThat(testEcheance.getIdFacture()).isEqualTo(DEFAULT_ID_FACTURE);
        assertThat(testEcheance.getRef()).isEqualTo(DEFAULT_REF);
        assertThat(testEcheance.getMontanHT()).isEqualTo(DEFAULT_MONTAN_HT);
        assertThat(testEcheance.getMontant()).isEqualTo(DEFAULT_MONTANT);
        assertThat(testEcheance.getDateDebut()).isEqualTo(DEFAULT_DATE_DEBUT);
        assertThat(testEcheance.getDateFin()).isEqualTo(DEFAULT_DATE_FIN);
        assertThat(testEcheance.getIdReglement()).isEqualTo(DEFAULT_ID_REGLEMENT);
        assertThat(testEcheance.isEstFacture()).isEqualTo(DEFAULT_EST_FACTURE);
        assertThat(testEcheance.isEstRegle()).isEqualTo(DEFAULT_EST_REGLE);
    }

    @Test
    @Transactional
    public void createEcheanceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = echeanceRepository.findAll().size();

        // Create the Echeance with an existing ID
        echeance.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEcheanceMockMvc.perform(post("/api/echeances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(echeance)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Echeance> echeanceList = echeanceRepository.findAll();
        assertThat(echeanceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkIdEcheancierIsRequired() throws Exception {
        int databaseSizeBeforeTest = echeanceRepository.findAll().size();
        // set the field null
        echeance.setIdEcheancier(null);

        // Create the Echeance, which fails.

        restEcheanceMockMvc.perform(post("/api/echeances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(echeance)))
            .andExpect(status().isBadRequest());

        List<Echeance> echeanceList = echeanceRepository.findAll();
        assertThat(echeanceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIdAmodiataireIsRequired() throws Exception {
        int databaseSizeBeforeTest = echeanceRepository.findAll().size();
        // set the field null
        echeance.setIdAmodiataire(null);

        // Create the Echeance, which fails.

        restEcheanceMockMvc.perform(post("/api/echeances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(echeance)))
            .andExpect(status().isBadRequest());

        List<Echeance> echeanceList = echeanceRepository.findAll();
        assertThat(echeanceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRefIsRequired() throws Exception {
        int databaseSizeBeforeTest = echeanceRepository.findAll().size();
        // set the field null
        echeance.setRef(null);

        // Create the Echeance, which fails.

        restEcheanceMockMvc.perform(post("/api/echeances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(echeance)))
            .andExpect(status().isBadRequest());

        List<Echeance> echeanceList = echeanceRepository.findAll();
        assertThat(echeanceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMontanHTIsRequired() throws Exception {
        int databaseSizeBeforeTest = echeanceRepository.findAll().size();
        // set the field null
        echeance.setMontanHT(null);

        // Create the Echeance, which fails.

        restEcheanceMockMvc.perform(post("/api/echeances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(echeance)))
            .andExpect(status().isBadRequest());

        List<Echeance> echeanceList = echeanceRepository.findAll();
        assertThat(echeanceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMontantIsRequired() throws Exception {
        int databaseSizeBeforeTest = echeanceRepository.findAll().size();
        // set the field null
        echeance.setMontant(null);

        // Create the Echeance, which fails.

        restEcheanceMockMvc.perform(post("/api/echeances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(echeance)))
            .andExpect(status().isBadRequest());

        List<Echeance> echeanceList = echeanceRepository.findAll();
        assertThat(echeanceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateDebutIsRequired() throws Exception {
        int databaseSizeBeforeTest = echeanceRepository.findAll().size();
        // set the field null
        echeance.setDateDebut(null);

        // Create the Echeance, which fails.

        restEcheanceMockMvc.perform(post("/api/echeances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(echeance)))
            .andExpect(status().isBadRequest());

        List<Echeance> echeanceList = echeanceRepository.findAll();
        assertThat(echeanceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateFinIsRequired() throws Exception {
        int databaseSizeBeforeTest = echeanceRepository.findAll().size();
        // set the field null
        echeance.setDateFin(null);

        // Create the Echeance, which fails.

        restEcheanceMockMvc.perform(post("/api/echeances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(echeance)))
            .andExpect(status().isBadRequest());

        List<Echeance> echeanceList = echeanceRepository.findAll();
        assertThat(echeanceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEcheances() throws Exception {
        // Initialize the database
        echeanceRepository.saveAndFlush(echeance);

        // Get all the echeanceList
        restEcheanceMockMvc.perform(get("/api/echeances?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(echeance.getId().intValue())))
            .andExpect(jsonPath("$.[*].idEcheancier").value(hasItem(DEFAULT_ID_ECHEANCIER.intValue())))
            .andExpect(jsonPath("$.[*].idAmodiataire").value(hasItem(DEFAULT_ID_AMODIATAIRE.intValue())))
            .andExpect(jsonPath("$.[*].idFacture").value(hasItem(DEFAULT_ID_FACTURE.intValue())))
            .andExpect(jsonPath("$.[*].ref").value(hasItem(DEFAULT_REF.toString())))
            .andExpect(jsonPath("$.[*].montanHT").value(hasItem(DEFAULT_MONTAN_HT.intValue())))
            .andExpect(jsonPath("$.[*].montant").value(hasItem(DEFAULT_MONTANT.intValue())))
            .andExpect(jsonPath("$.[*].dateDebut").value(hasItem(DEFAULT_DATE_DEBUT.toString())))
            .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())))
            .andExpect(jsonPath("$.[*].idReglement").value(hasItem(DEFAULT_ID_REGLEMENT.intValue())))
            .andExpect(jsonPath("$.[*].estFacture").value(hasItem(DEFAULT_EST_FACTURE.booleanValue())))
            .andExpect(jsonPath("$.[*].estRegle").value(hasItem(DEFAULT_EST_REGLE.booleanValue())));
    }

    @Test
    @Transactional
    public void getEcheance() throws Exception {
        // Initialize the database
        echeanceRepository.saveAndFlush(echeance);

        // Get the echeance
        restEcheanceMockMvc.perform(get("/api/echeances/{id}", echeance.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(echeance.getId().intValue()))
            .andExpect(jsonPath("$.idEcheancier").value(DEFAULT_ID_ECHEANCIER.intValue()))
            .andExpect(jsonPath("$.idAmodiataire").value(DEFAULT_ID_AMODIATAIRE.intValue()))
            .andExpect(jsonPath("$.idFacture").value(DEFAULT_ID_FACTURE.intValue()))
            .andExpect(jsonPath("$.ref").value(DEFAULT_REF.toString()))
            .andExpect(jsonPath("$.montanHT").value(DEFAULT_MONTAN_HT.intValue()))
            .andExpect(jsonPath("$.montant").value(DEFAULT_MONTANT.intValue()))
            .andExpect(jsonPath("$.dateDebut").value(DEFAULT_DATE_DEBUT.toString()))
            .andExpect(jsonPath("$.dateFin").value(DEFAULT_DATE_FIN.toString()))
            .andExpect(jsonPath("$.idReglement").value(DEFAULT_ID_REGLEMENT.intValue()))
            .andExpect(jsonPath("$.estFacture").value(DEFAULT_EST_FACTURE.booleanValue()))
            .andExpect(jsonPath("$.estRegle").value(DEFAULT_EST_REGLE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingEcheance() throws Exception {
        // Get the echeance
        restEcheanceMockMvc.perform(get("/api/echeances/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEcheance() throws Exception {
        // Initialize the database
        echeanceService.save(echeance);

        int databaseSizeBeforeUpdate = echeanceRepository.findAll().size();

        // Update the echeance
        Echeance updatedEcheance = echeanceRepository.findOne(echeance.getId());
        updatedEcheance
            .idEcheancier(UPDATED_ID_ECHEANCIER)
            .idAmodiataire(UPDATED_ID_AMODIATAIRE)
            .idFacture(UPDATED_ID_FACTURE)
            .ref(UPDATED_REF)
            .montanHT(UPDATED_MONTAN_HT)
            .montant(UPDATED_MONTANT)
            .dateDebut(UPDATED_DATE_DEBUT)
            .dateFin(UPDATED_DATE_FIN)
            .idReglement(UPDATED_ID_REGLEMENT)
            .estFacture(UPDATED_EST_FACTURE)
            .estRegle(UPDATED_EST_REGLE);

        restEcheanceMockMvc.perform(put("/api/echeances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEcheance)))
            .andExpect(status().isOk());

        // Validate the Echeance in the database
        List<Echeance> echeanceList = echeanceRepository.findAll();
        assertThat(echeanceList).hasSize(databaseSizeBeforeUpdate);
        Echeance testEcheance = echeanceList.get(echeanceList.size() - 1);
        assertThat(testEcheance.getIdEcheancier()).isEqualTo(UPDATED_ID_ECHEANCIER);
        assertThat(testEcheance.getIdAmodiataire()).isEqualTo(UPDATED_ID_AMODIATAIRE);
        assertThat(testEcheance.getIdFacture()).isEqualTo(UPDATED_ID_FACTURE);
        assertThat(testEcheance.getRef()).isEqualTo(UPDATED_REF);
        assertThat(testEcheance.getMontanHT()).isEqualTo(UPDATED_MONTAN_HT);
        assertThat(testEcheance.getMontant()).isEqualTo(UPDATED_MONTANT);
        assertThat(testEcheance.getDateDebut()).isEqualTo(UPDATED_DATE_DEBUT);
        assertThat(testEcheance.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testEcheance.getIdReglement()).isEqualTo(UPDATED_ID_REGLEMENT);
        assertThat(testEcheance.isEstFacture()).isEqualTo(UPDATED_EST_FACTURE);
        assertThat(testEcheance.isEstRegle()).isEqualTo(UPDATED_EST_REGLE);
    }

    @Test
    @Transactional
    public void updateNonExistingEcheance() throws Exception {
        int databaseSizeBeforeUpdate = echeanceRepository.findAll().size();

        // Create the Echeance

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEcheanceMockMvc.perform(put("/api/echeances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(echeance)))
            .andExpect(status().isCreated());

        // Validate the Echeance in the database
        List<Echeance> echeanceList = echeanceRepository.findAll();
        assertThat(echeanceList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteEcheance() throws Exception {
        // Initialize the database
        echeanceService.save(echeance);

        int databaseSizeBeforeDelete = echeanceRepository.findAll().size();

        // Get the echeance
        restEcheanceMockMvc.perform(delete("/api/echeances/{id}", echeance.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Echeance> echeanceList = echeanceRepository.findAll();
        assertThat(echeanceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Echeance.class);
    }
}
