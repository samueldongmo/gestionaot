package com.geospace.gestionaot.cucumber.stepdefs;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import com.geospace.gestionaot.GestionaotApp;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = GestionaotApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
