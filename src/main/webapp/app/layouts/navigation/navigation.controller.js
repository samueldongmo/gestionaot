(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('NavigationController', NavigationController);

    NavigationController.$inject = ['$state', '$scope', 'Principal', 'ProfileService', 'LoginService'];

    function NavigationController ($state, $scope, Principal, ProfileService, LoginService) {
        var vm = this;

        vm.isNavbarCollapsed = true;
        vm.isAuthenticated = null;

        vm.$state = $state;

        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.user = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }
    }
})();
