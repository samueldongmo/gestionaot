(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .config(stateConfig)
        
    .directive(dartCurrency);

    stateConfig.$inject = ['$stateProvider'];
    dartCurrency.$inject = ['$filter'];
    
    
    function stateConfig($stateProvider) {
        $stateProvider.state('app', {
            abstract: true,
            views: {
                'navbar@': {
                    templateUrl: 'app/layouts/navbar/navbar.html',
                    controller: 'NavbarController',
                    controllerAs: 'vm'
                },
                'navigation@': {
                    templateUrl: 'app/layouts/navigation/navigation.html',
                    controller: 'NavigationController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                authorize: ['Auth',
                    function (Auth) {
                        return Auth.authorize();
                    }
                ],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('global');
                }]
            }
        });
    }
    
    function dartCurrency($filter){
		 return {
			    scope: {
			      ngModel  : '='
			    },
			    link: function(scope, el, attrs){
			      el.val($filter('currency2')(scope.ngModel));
			      
			      el.bind('focus', function(){
			        el.val(scope.ngModel);
			      });
			      
			      el.bind('input', function(){
			        scope.ngModel = el.val();
			        scope.$apply();
			      });
			      
			      el.bind('blur', function(){
			        el.val($filter('currency2')(scope.ngModel));
			      });
			    }
			  }
		}
})();
