(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('MainController', MainController);

    MainController.$inject = ['Principal', '$scope'];

    function MainController (Principal, $scope) {
        $scope.isAuthenticated = Principal.isAuthenticated;
    }
})();
