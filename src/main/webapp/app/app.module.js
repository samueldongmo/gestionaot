
(function() {
    'use strict';

    angular
        .module('gestionaotApp', [
            'ngStorage',
            'tmh.dynamicLocale',
            'pascalprecht.translate',
            'ngResource',
            'ngCookies',
            'ngAria',
            'ngCacheBuster',
            'ngFileUpload',
            'ui.bootstrap',
            'ui.bootstrap.datetimepicker',
            'ui.router',
            'infinite-scroll',
            // jhipster-needle-angularjs-add-module JHipster will add new module here
            'angular-loading-bar',
            'openlayers-directive',
            'ngLocale',
            'FBAngular'
        ])
        .run(run);

    run.$inject = ['stateHandler', 'translationHandler', '$rootScope'];

    function run(stateHandler, translationHandler, $rootScope) {
        stateHandler.initialize();
        translationHandler.initialize();
        $rootScope.dateformat = 'dd/MM/yyyy';
    }
})();
