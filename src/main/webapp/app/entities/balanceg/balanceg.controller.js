'use strict';

angular.module('gestionaotApp')
    .controller('BalancegController', ['$scope', '$filter', 'BalancegService','$timeout' ,
    function ($scope, $filter, BalancegService, $timeout) {
        $scope.onChangeDate = function () {
            var dateFormat = 'yyyy-MM-dd';
            var fromDate = $filter('date')($scope.fromDate, dateFormat);
            //var toDate = $filter('date')($scope.toDate, dateFormat);

            BalancegService.findByDates(fromDate).then(function (data) {
            	$scope.balanceg = data;
                $scope.balancegitems = $scope.balanceg.balancegItemDTOs;
                $scope.balancegtotal = $scope.balanceg.totalItem;
            });
        };

        // Date picker configuration
        $scope.today = function () {
            $scope.fromDate = new Date();
        };

        $scope.today();
        $scope.onChangeDate();


        $scope.openCalendar = openCalendar;
        $scope.datePickerOpenStatus = {};
        $scope.datePickerOpenStatus.fromDate = false;
        $scope.dateformat = "dd/MM/yyyy";


        function openCalendar (date) {
            $scope.datePickerOpenStatus[date] = true;
        }

        $scope.formatCurrency = function addCommas(n){
            var rx=  /(\d+)(\d{3})/;
            return String(n).replace(/^\d+/, function(w){
                while(rx.test(w)){
                    w= w.replace(rx, '$1 $2');
                }
                return w;
            });
        };

        $scope.printPage = function() {
            $scope.isPrinting = true;
            $timeout(function(){
                window.print();
                $timeout(function(){
                    $scope.isPrinting = false;
                }, 15);
            }, 50);
        };
}]);
