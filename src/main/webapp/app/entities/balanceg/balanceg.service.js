'use strict';

angular.module('gestionaotApp')
    .factory('BalancegService', ['$http',function ($http) {
        return {
            findAll: function () {
                return $http.get('api/balanceg').then(function (response) {
                    return response.data;
                });
            },
            findByDates: function (fromDate) {

                var formatDate =  function (dateToFormat) {
                    if (dateToFormat !== undefined && !angular.isString(dateToFormat)) {
                        return dateToFormat.getYear() + '-' + dateToFormat.getMonth() + '-' + dateToFormat.getDay();
                    }
                    return dateToFormat;
                };

                return $http.get('api/balanceg/object', {params: {fromDate: formatDate(fromDate) }}).then(function (response) {
                    return response.data;
                });
            }
        };
    }]);
