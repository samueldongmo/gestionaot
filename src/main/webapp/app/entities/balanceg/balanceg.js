'use strict';

angular.module('gestionaotApp')
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('balanceg', {
                parent: 'entity',
                url: '/balanceg',
                data: {
                    authorities: ['CONSULTER_BALANCE_AGEE','GERER_BALANCEG','ROLE_ADMIN'],
                    pageTitle: 'balancegApp.balanceg.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/balanceg/balanceg.html',
                        controller: 'BalancegController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('balanceg');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            });
    }]);
