(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('echeancier', {
            parent: 'entity',
            url: '/echeancier?page&sort&search',
            data: {
                authorities: ['LISTER_ECHEANCIER','GERER_ECHEANCIER','ROLE_ADMIN'],
                pageTitle: 'gestionaotApp.echeancier.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/echeancier/echeanciers.html',
                    controller: 'EcheancierController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('echeancier');
                    $translatePartialLoader.addPart('global');
                    $translatePartialLoader.addPart('amodiataire');
                    return $translate.refresh();
                }]
            }
        })
        .state('echeancier-detail', {
            parent: 'echeancier',
            url: '/echeancier/{id}',
            data: {
                authorities: ['LISTER_ECHEANCIER','GERER_ECHEANCIER','ROLE_ADMIN'],
                pageTitle: 'gestionaotApp.echeancier.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/echeancier/echeancier-detail.html',
                    controller: 'EcheancierDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('echeancier');
                    $translatePartialLoader.addPart('echeance');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Echeancier', function($stateParams, Echeancier) {
                    return Echeancier.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'echeancier',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('echeancier-detail.edit', {
            parent: 'echeancier-detail',
            url: '/detail/edit',
            data: {
                authorities: ['MODIFIER_ECHEANCIER','GERER_ECHEANCIER','ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/echeancier/echeancier-dialog.html',
                    controller: 'EcheancierDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Echeancier', function(Echeancier) {
                            return Echeancier.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('echeancier.new', {
            parent: 'echeancier',
            url: '/new',
            data: {
                authorities: ['CREER_ECHEANCIER','GERER_ECHEANCIER','ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/echeancier/echeancier-dialog.html',
                    controller: 'EcheancierDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                idAmodiataire: null,
                                ref: null,
                                montantHT: null,
                                montant: null,
                                dateDebut: null,
                                dateFin: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('echeancier', null, { reload: 'echeancier' });
                }, function() {
                    $state.go('echeancier');
                });
            }]
        })
        .state('echeancier.edit', {
            parent: 'echeancier',
            url: '/{id}/edit',
            data: {
                authorities: ['MODIFIER_TASK','GERER_ECHEANCIER','ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/echeancier/echeancier-dialog.html',
                    controller: 'EcheancierDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Echeancier', function(Echeancier) {
                            return Echeancier.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('echeancier', null, { reload: 'echeancier' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('echeancier.delete', {
            parent: 'echeancier',
            url: '/{id}/delete',
            data: {
                authorities: ['SUPPRIMER_TASK','GERER_ECHEANCIER','ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/echeancier/echeancier-delete-dialog.html',
                    controller: 'EcheancierDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Echeancier', function(Echeancier) {
                            return Echeancier.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('echeancier', null, { reload: 'echeancier' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
