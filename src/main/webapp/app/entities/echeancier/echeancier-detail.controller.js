(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('EcheancierDetailController', EcheancierDetailController);

    EcheancierDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Echeancier'];

    function EcheancierDetailController($scope, $rootScope, $stateParams, previousState, entity, Echeancier) {
        var vm = this;

        vm.echeancier = entity;
        vm.previousState = previousState.name;
        
        vm.max = 10;
        vm.currentPage = 0;
        vm.page = {};
        vm.maxSize = 5;

        var unsubscribe = $rootScope.$on('gestionaotApp:echeancierUpdate', function(event, result) {
            vm.echeancier = result;
        });
        $scope.$on('$destroy', unsubscribe);
        
        findEcheances(entity);
        
        function findEcheances(echeancier) {
        	if(!echeancier || !echeancier.id) return [];
        	Echeancier.findEcheances({id: echeancier.id, page: vm.currentPage, max : vm.max}, function(result){
        		console.log(result);
        		vm.page = result;
        		vm.echeances = result.content;
        	});
        }
        
    }
})();
