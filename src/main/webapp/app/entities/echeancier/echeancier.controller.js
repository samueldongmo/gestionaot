(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('EcheancierController', EcheancierController);

    EcheancierController.$inject = ['$state', 'Echeancier', 'ParseLinks', 'AlertService', 'paginationConstants', 'pagingParams','Amodiataire'];

    function EcheancierController($state, Echeancier, ParseLinks, AlertService, paginationConstants, pagingParams, Amodiataire) {

        var vm = this;

        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.itemsPerPage = paginationConstants.itemsPerPage;


        // Filter
        vm.filter = {};
        // Search function
        vm.search = search;
        // Reset function;
        vm.reset = reset;
        // onChangeAmodiataire
        vm.onChangeAmodiataire = onChangeAmodiataire;
        
        loadAll();
        loadAmodiataires();
        function loadAll () {
            Echeancier.query({
                page: pagingParams.page - 1,
                size: vm.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.echeanciers = data;
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage(page) {
            vm.page = page;
            vm.transition();
        }

        function transition() {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }

        /*
         * Search function
         */
        function search () {
        	Echeancier.search(vm.filter, function (result) {
        		vm.echeanciers = result;
        	}, function (error) {
                AlertService.error(error.data.message);
        	});
        	/*$http.post('api/echeanciers/search', vm.filter).then(function(result) {
        		vm.echeanciers = result.data;
        	}, function(error ) {
        		console.log(error);
        	});*/
        }

        function reset () {
        	vm.filter = {}; // Clear the filter
        	loadAll() // Load all
        }
        
        function onChangeAmodiataire (amodiataire) {
        	if(!amodiataire) {
        		console.log('No amodiataire');
        		return;
        	}
        	vm.filter.idAmodiataire =  amodiataire.id;
        }
        
        function loadAmodiataires() {
        	vm.amodiataires = Amodiataire.query();
        }
    }
})();
