(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('EcheancierDeleteController',EcheancierDeleteController);

    EcheancierDeleteController.$inject = ['$uibModalInstance', 'entity', 'Echeancier'];

    function EcheancierDeleteController($uibModalInstance, entity, Echeancier) {
        var vm = this;

        vm.echeancier = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Echeancier.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
