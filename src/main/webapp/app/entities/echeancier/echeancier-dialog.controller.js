(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('EcheancierDialogController', EcheancierDialogController);

    EcheancierDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Echeancier'];

    function EcheancierDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Echeancier) {
        var vm = this;

        vm.echeancier = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.echeancier.id !== null) {
                Echeancier.update(vm.echeancier, onSaveSuccess, onSaveError);
            } else {
                Echeancier.save(vm.echeancier, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('gestionaotApp:echeancierUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateDebut = false;
        vm.datePickerOpenStatus.dateFin = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
