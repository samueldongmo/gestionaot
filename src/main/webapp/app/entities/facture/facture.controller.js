'use strict';

angular.module('gestionaotApp')
    .controller('FactureController', ['$scope', '$state','$filter', 'Facture', 'ParseLinks','$http',
        function ($scope, $state,$filter, Facture, ParseLinks, $http) {

        var vm = this;
        $scope.factures = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 0;

        $scope.loadAll = function() {
            Facture.query({page: $scope.page, size: 8, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                for (var i = 0; i < result.length; i++) {
                    $scope.factures.push(result[i]);
                }
            });
        };
        $scope.reset = function() {
            $scope.page = 0;
            $scope.factures = [];
            $scope.loadAll();
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            /*FactureSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.factures = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });*/
        };

        $scope.refresh = function () {
            $scope.reset();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.facture = {
                ref: null,
                libelle: null,
                dateFacture: null,
                dateEcheance: null,
                montant: null,
                reglement: null,
                dateReglement: null,
                id: null
            };
        };

        $scope.dayDifference = function (theDateFrom, theDateTo) {
        	var dateTo = new Date(theDateTo);
        	var dateFrom = new Date(theDateFrom);
        	var millisecondDiff =  dateTo.getTime() - dateFrom.getTime();
            return millisecondDiff/(24*60*60*1000);
        };

        $scope.enretard = function (dateEcheance) {

        	var dayretardnumber = $scope.dayRetard(dateEcheance);
        	if(dayretardnumber <= 0 ) return false;
        	return true;
        };

        $scope.dayRetard = function (dateEcheance) {

        	var today = new Date();
            var dateFormat = 'yyyy-MM-dd';
            var fromDate = $filter('date')(today, dateFormat);

        	var dateTo = new Date(fromDate);
        	var dateFrom = new Date(dateEcheance);
        	var millisecondDiff =  dateTo.getTime() - dateFrom.getTime();
            return millisecondDiff/(24*60*60*1000);
        };

        $scope.filterAmodiataires = function(filterValue) {
            return $http.get("api/amodiataires/filter/"+filterValue).then(function(result) {
                return result.data;
            });
        };

    }]);
