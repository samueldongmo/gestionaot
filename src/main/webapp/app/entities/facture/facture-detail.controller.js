'use strict';

angular.module('gestionaotApp')
    .controller('FactureDetailController', ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Facture', 'Client',function ($scope, $rootScope, $stateParams, previousState, entity, Facture, Client) {
        $scope.facture = entity;

        $scope.previousState = previousState;

        $scope.load = function (id) {
            Facture.get({id: id}, function(result) {
                $scope.facture = result;
            });
        };
        var unsubscribe = $rootScope.$on('gestionaotApp:factureUpdate', function(event, result) {
            $scope.facture = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }]);
