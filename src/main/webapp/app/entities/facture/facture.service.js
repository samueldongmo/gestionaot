'use strict';

angular.module('gestionaotApp')
    .factory('Facture', ['$resource', 'DateUtils', function ($resource, DateUtils) {
        return $resource('api/factures/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.dateFacture = DateUtils.convertLocalDateFromServer(data.dateFacture);
                    data.dateEcheance = DateUtils.convertLocalDateFromServer(data.dateEcheance);
                    data.dateDebut = DateUtils.convertLocalDateFromServer(data.dateDebut);
                    data.dateFin = DateUtils.convertLocalDateFromServer(data.dateFin);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.dateFacture = DateUtils.convertLocalDateToServer(data.dateFacture);
                    data.dateEcheance = DateUtils.convertLocalDateToServer(data.dateEcheance);
                    data.dateDebut = DateUtils.convertLocalDateToServer(data.dateDebut);
                    data.dateFin = DateUtils.convertLocalDateToServer(data.dateFin);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.dateFacture = DateUtils.convertLocalDateToServer(data.dateFacture);
                    data.dateEcheance = DateUtils.convertLocalDateToServer(data.dateEcheance);
                    data.dateDebut = DateUtils.convertLocalDateToServer(data.dateDebut);
                    data.dateFin = DateUtils.convertLocalDateToServer(data.dateFin);
                    return angular.toJson(data);
                }
            }
        });
    }]);
