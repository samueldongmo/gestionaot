'use strict';

angular.module('gestionaotApp').controller('FactureDialogController',
    ['$timeout', '$scope', '$state', '$stateParams', 'entity', 'Facture', 'Client', 'ClientCustom', '$filter', 'clients'
        , 'Echeance', 'DateUtils', 'previousState',
        function ($timeout, $scope, $state, $stateParams, entity, Facture, Client, ClientCustom, $filter, clients
            , Echeance, DateUtils, previousState) {


            $scope.facture = entity;
            $scope.clients = clients;
            $scope.openCalendar = openCalendar;
            $scope.echeances = [];
            $scope.selectedEcheance = null;

            // Date
            $scope.datePickerOpenStatus = {};
            $scope.datePickerOpenStatus.dateDebut = false;
            $scope.datePickerOpenStatus.dateFin = false;

            $timeout(function () {
                angular.element('.form-group:eq(1)>input').focus();
            });

            function openCalendar(date) {
                $scope.datePickerOpenStatus[date] = true;
            }


            $scope.loadClients = function () {
                $scope.clients = ClientCustom.findAll();
            };

            $scope.loadClients();
            $scope.clients = ClientCustom.findAll();
            $scope.clients.$promise.then(function (data) {
                $scope.clients = data;
            });
            $scope.selectedClient = null;
            $scope.load = function (id) {
                Facture.get({id: id}, function (result) {
                    $scope.facture = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('gestionaotApp:factureUpdate', result);
                $state.go('facture.detail', {id: result.id});
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
            	
            	console.log($scope.facture)
                $scope.isSaving = true;
                if ($scope.facture.montantRegle == null) $scope.facture.montantRegle = 0;
                if (!$scope.facture.clientRef) $scope.facture.clientRef = $scope.facture.client.niu;
                if ($scope.facture.estRegle == null) $scope.facture.estRegle = false;
                if (!$scope.facture.idEcheance) {
                    $scope.errorMessage = "Vous devez founir une echeance pour cette facture";
                    return;
                }
                if ($scope.facture.id != null) {
                    Facture.update($scope.facture, onSaveSuccess, onSaveError);
                } else {
                    Facture.save($scope.facture, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function () {
                $state.go(previousState.name, previousState.params);
                //$uibModalInstance.dismiss('cancel');
            };
            $scope.datePickerForDateFacture = {};

            $scope.datePickerForDateFacture.status = {
                opened: false
            };

            $scope.datePickerForDateFactureOpen = function ($event) {
                $scope.datePickerForDateFacture.status.opened = true;
            };
            $scope.datePickerForDateEcheance = {};

            $scope.datePickerForDateEcheance.status = {
                opened: false
            };

            $scope.datePickerForDateEcheanceOpen = function ($event) {
                $scope.datePickerForDateEcheance.status.opened = true;
            };

            $scope.datePickerForDateReglement = {};

            $scope.datePickerForDateReglement.status = {
                opened: false
            };

            $scope.datePickerForDateReglementOpen = function ($event) {
                $scope.datePickerForDateReglement.status.opened = true;
            };

            $scope.onChangeDateFacture = function () {

                $scope.facture.dateEcheance = addDays($scope.facture.dateFacture, $scope.facture.delaiEcheance);
                // $filter('date')(fromDate, dateFormat);
                // $scope.facture.dateEcheance.setDate(fromDate);
            };

            $scope.onChangeClient = function (selected) {
                $scope.selectedClient = selected;
                if (!$scope.selectedClient || !$scope.selectedClient.id) {
                    console.log('no selected client value');
                    return;
                }
                console.log($scope.selectedClient);
                $scope.facture.clientId = $scope.selectedClient.id;
                $scope.facture.clientRef = $scope.selectedClient.niu;
                $scope.facture.amodiataire = $scope.selectedClient;
                $scope.facture.delaiEcheance = $scope.selectedClient.delaiEcheance;

                // Load echeance non reglee
                Echeance.findByAmodiataire({id: $scope.selectedClient.id}, function (result) {
                    $scope.echeances = result;
                });

            }

            function addDays(theDate, days) {
                return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
            }

            $scope.dayDifference = function (theDateFrom, theDateTo) {
                var dateTo = new Date(theDateTo);
                var dateFrom = new Date(theDateFrom);
                var millisecondDiff = dateTo.getTime() - dateFrom.getTime();
                return millisecondDiff / (24 * 60 * 60 * 1000);
            }

            $scope.updateDateEcheance = function () {
                $scope.facture.dateEcheance = $scope.facture.dateFin;
            };

            $scope.onChangeEcheance = function () {
                if (!$scope.selectedEcheance) return;
                $scope.facture.idEcheance = $scope.selectedEcheance.id;
                $scope.facture.montant = $scope.selectedEcheance.montant;
                $scope.facture.montantHorsTaxe = $scope.selectedEcheance.montanHT;
                $scope.facture.dateDebut = DateUtils.convertLocalDateFromServer($scope.selectedEcheance.dateDebut);
                $scope.facture.dateFin = DateUtils.convertLocalDateFromServer($scope.selectedEcheance.dateFin);
                $scope.facture.dateEcheance = $scope.facture.dateFin;
            };

            //var newDate = addDays(new Date(), 5);

            $scope.formValid = function () {
                if (!$scope.facture.ref) return false;
                if (!$scope.facture.libelle) return false;
                if (!$scope.selectedClient || !$scope.selectedClient.id) return false;
                if (!$scope.facture.idEcheance) return false;
                if (!$scope.facture.dateFacture) return false;
                return true;
            };
        }]);
