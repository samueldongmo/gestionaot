'use strict';

angular.module('gestionaotApp')
	.controller('FactureDeleteController', ['$scope', '$uibModalInstance', 'entity', 'Facture',function($scope, $uibModalInstance, entity, Facture) {

        $scope.facture = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Facture.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    }]);
