'use strict';

angular.module('gestionaotApp')
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('facture', {
                parent: 'entity',
                url: '/factures',
                data: {
                    authorities: ['LISTER_FACTURE','GERER_FACTURE','ROLE_ADMIN'],
                    pageTitle: 'gestionaotApp.facture.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/facture/factures.html',
                        controller: 'FactureController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('facture');
                        $translatePartialLoader.addPart('global');
                        $translatePartialLoader.addPart('echeance');
                        return $translate.refresh();
                    }]
                }
            })
            .state('facture.detail', {
                parent: 'entity',
                url: '/facture/{id}',
                data: {
                    authorities: ['LISTER_FACTURE','GERER_FACTURE','ROLE_ADMIN'],
                    pageTitle: 'gestionaotApp.facture.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/facture/facture-detail.html',
                        controller: 'FactureDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('facture');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Facture', function($stateParams, Facture) {
                        return Facture.get({id : $stateParams.id});
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'facture',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('facture.new', {
                parent: 'facture',
                url: '/new',
                data: {
                    authorities: ['CREER_FACTURE','GERER_FACTURE','ROLE_ADMIN'],
                },

                views: {
                    'content@': {
                        templateUrl: 'app/entities/facture/facture-dialog.html',
                        controller: 'FactureDialogController',
                        controllerAs: 'vm'
                    }
                },

                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('facture');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return {
                            ref: null,
                            libelle: null,
                            dateFacture: null,
                            dateEcheance: null,
                            montant: 0,
                            id: null,
                            montantHorsTaxe:0
                        };
                    },
                    clients: ['ClientCustom', function(ClientCustom) {
                    	return ClientCustom.findAll();                                
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'facture',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }

                /*onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/facture/facture-dialog.html',
                        controller: 'FactureDialogController',
                        controllerAs: 'vm',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    ref: null,
                                    libelle: null,
                                    dateFacture: null,
                                    dateEcheance: null,
                                    montant: 0,
                                    id: null,
                                    montantHorsTaxe:0
                                };
                            },
                            clients: ['ClientCustom', function(ClientCustom) {
                            	return ClientCustom.findAll();
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('facture', null, { reload: true });
                    }, function() {
                        $state.go('facture');
                    })
                }]*/
            })
            .state('facture.edit', {
                parent: 'facture',
                url: '/{id}/edit',
                data: {
                    authorities: ['MODIFIER_FACTURE','GERER_FACTURE','ROLE_ADMIN'],
                },

                views: {
                    'content@': {
                        templateUrl: 'app/entities/facture/facture-dialog.html',
                        controller: 'FactureDialogController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('facture');
                        return $translate.refresh();
                    }],
                    entity: ['Facture', '$stateParams', function(Facture, $stateParams) {
                        return Facture.get({id : $stateParams.id}).$promise;
                    }],
                    clients: ['ClientCustom', function(ClientCustom) {
                    	return ClientCustom.findAll();                                
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'facture',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }

                /*onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/facture/facture-dialog.html',
                        controller: 'FactureDialogController',
                        controllerAs: 'vm',
                        size: 'lg',
                        resolve: {
                            entity: ['Facture', function(Facture) {
                                return Facture.get({id : $stateParams.id});
                            }],
                            clients: ['ClientCustom', function(ClientCustom) {
                            	return ClientCustom.findAll();
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('facture', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]*/
            })
            .state('facture.editEcheance', {
                parent: 'facture',
                url: '/{id}/editEcheance',
                data: {
                    authorities: ['MODIFIER_ECHEANCE_FACTURE','GERER_FACTURE','ROLE_ADMIN'],
                },
                
                views: {
                    'content@': {
                    	templateUrl: 'app/entities/facture/facture-edit-echeance.html',
                        controller: 'FactureEditEcheanceController',
                        size: 'lg',
                        controllerAs: 'vm'
                    }
                },
                
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('facture');
                        return $translate.refresh();
                    }],
                    entity: ['Facture', '$stateParams', function(Facture, $stateParams) {
                        return Facture.get({id : $stateParams.id}).$promise;
                    }],
                    clients: ['ClientCustom', function(ClientCustom) {
                    	return ClientCustom.findAll();                                
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'facture',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                },
            })
            .state('facture.delete', {
                parent: 'facture',
                url: '/{id}/delete',
                data: {
                    authorities: ['SUPPRIMER_FACTURE','GERER_FACTURE','ROLE_ADMIN'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/facture/facture-delete-dialog.html',
                        controller: 'FactureDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Facture', function(Facture) {
                                return Facture.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('facture', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    }]);
