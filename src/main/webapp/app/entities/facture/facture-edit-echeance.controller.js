'use strict';

angular.module('gestionaotApp').controller('FactureEditEcheanceController',
    ['$scope', '$stateParams', 'entity', 'Facture', 'Client', '$filter','$state','previousState',
        function($scope, $stateParams, entity, Facture, Client, $filter,$state,previousState) {

    	entity.$promise.then(function(data){
    		$scope.facture = entity;
    	    $scope.ancienecheance = $scope.facture.dateEcheance;    		
    		$scope.load(entity.id);
    	});
    	
       
        $scope.load = function(id) {
            Facture.get({id : id}, function(result) {
                $scope.facture = result;
                $scope.ancienecheance = $scope.facture.dateEcheance;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('balancegApp:factureUpdate', result);
            $state.go('facture.detail', {id: result.id});
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if($scope.facture.montantRegle == null) $scope.facture.montantRegle = 0;
            if($scope.facture.estRegle == null) $scope.facture.estRegle = false;
            if ($scope.facture.id != null) {
                Facture.update($scope.facture, onSaveSuccess, onSaveError);
            } else {
                Facture.save($scope.facture, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
        	$state.go(previousState.name, previousState.params);
        };
        $scope.datePickerForDateFacture = {};

        $scope.datePickerForDateFacture.status = {
            opened: false
        };

        $scope.datePickerForDateFactureOpen = function($event) {
            $scope.datePickerForDateFacture.status.opened = true;
        };
        $scope.datePickerForDateEcheance = {};

        $scope.datePickerForDateEcheance.status = {
            opened: false
        };

        $scope.datePickerForDateEcheanceOpen = function($event) {
            $scope.datePickerForDateEcheance.status.opened = true;
        };
        
        $scope.datePickerForDateReglement = {};

        $scope.datePickerForDateReglement.status = {
            opened: false
        };

        $scope.datePickerForDateReglementOpen = function($event) {
            $scope.datePickerForDateReglement.status.opened = true;
        };
        
      
        $scope.onChangeClient = function () {
        	
        	$scope.facture.clientId = $scope.selectedClient.id;
        	$scope.facture.clientRef = $scope.selectedClient.ref;
        	$scope.facture.delaiEcheance = $scope.selectedClient.delaiEcheance;
        	
        }
        function addDays(theDate, days) {
            return new Date(theDate.getTime() + days*24*60*60*1000);
        }
        
        $scope.dayDifference = function (theDateFrom, theDateTo) {
        	var dateTo = new Date(theDateTo);
        	var dateFrom = new Date(theDateFrom);
        	var millisecondDiff =  dateTo.getTime() - dateFrom.getTime();
            return millisecondDiff/(24*60*60*1000);
        }

        //var newDate = addDays(new Date(), 5);
}]);
