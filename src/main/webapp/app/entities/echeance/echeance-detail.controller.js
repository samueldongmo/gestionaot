(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('EcheanceDetailController', EcheanceDetailController);

    EcheanceDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Echeance'];

    function EcheanceDetailController($scope, $rootScope, $stateParams, previousState, entity, Echeance) {
        var vm = this;

        vm.echeance = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('gestionaotApp:echeanceUpdate', function(event, result) {
            vm.echeance = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
