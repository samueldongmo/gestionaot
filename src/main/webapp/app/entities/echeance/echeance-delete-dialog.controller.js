(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('EcheanceDeleteController',EcheanceDeleteController);

    EcheanceDeleteController.$inject = ['$uibModalInstance', 'entity', 'Echeance'];

    function EcheanceDeleteController($uibModalInstance, entity, Echeance) {
        var vm = this;

        vm.echeance = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Echeance.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
