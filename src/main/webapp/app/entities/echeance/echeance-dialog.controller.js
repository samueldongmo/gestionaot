(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('EcheanceDialogController', EcheanceDialogController);

    EcheanceDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Echeance'];

    function EcheanceDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Echeance) {
        var vm = this;

        vm.echeance = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.echeance.id !== null) {
                Echeance.update(vm.echeance, onSaveSuccess, onSaveError);
            } else {
                Echeance.save(vm.echeance, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('gestionaotApp:echeanceUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateDebut = false;
        vm.datePickerOpenStatus.dateFin = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
