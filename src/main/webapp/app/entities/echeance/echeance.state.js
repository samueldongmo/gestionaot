(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('echeance', {
            parent: 'entity',
            url: '/echeance?page&sort&search',
            data: {
                authorities: ['LISTER_REGLEMENT','GERER_ECHEANCE','ROLE_ADMIN'],
                pageTitle: 'gestionaotApp.echeance.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/echeance/echeances.html',
                    controller: 'EcheanceController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('echeance');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('echeance-detail', {
            parent: 'echeance',
            url: '/echeance/{id}',
            data: {
                authorities: ['LISTER_REGLEMENT','GERER_ECHEANCE','ROLE_ADMIN'],
                pageTitle: 'gestionaotApp.echeance.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/echeance/echeance-detail.html',
                    controller: 'EcheanceDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('echeance');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Echeance', function($stateParams, Echeance) {
                    return Echeance.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'echeance',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('echeance-detail.edit', {
            parent: 'echeance-detail',
            url: '/detail/edit',
            data: {
                authorities: ['LISTER_REGLEMENT','GERER_ECHEANCE','ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/echeance/echeance-dialog.html',
                    controller: 'EcheanceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Echeance', function(Echeance) {
                            return Echeance.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('echeance.new', {
            parent: 'echeance',
            url: '/new',
            data: {
                authorities: ['CREER_ECHEANCE','GERER_ECHEANCE','ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/echeance/echeance-dialog.html',
                    controller: 'EcheanceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                idEcheancier: null,
                                idAmodiataire: null,
                                idFacture: null,
                                ref: null,
                                montanHT: null,
                                montant: null,
                                dateDebut: null,
                                dateFin: null,
                                idReglement: null,
                                estFacture: null,
                                estRegle: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('echeance', null, { reload: 'echeance' });
                }, function() {
                    $state.go('echeance');
                });
            }]
        })
        .state('echeance.edit', {
            parent: 'echeance',
            url: '/{id}/edit',
            data: {
                authorities: ['MODIFIER_ECHEANCE','GERER_ECHEANCE','ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/echeance/echeance-dialog.html',
                    controller: 'EcheanceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Echeance', function(Echeance) {
                            return Echeance.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('echeance', null, { reload: 'echeance' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('echeance.delete', {
            parent: 'echeance',
            url: '/{id}/delete',
            data: {
                authorities: ['SUPPRIMER_ECHEANCE','GERER_ECHEANCE','ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/echeance/echeance-delete-dialog.html',
                    controller: 'EcheanceDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Echeance', function(Echeance) {
                            return Echeance.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('echeance', null, { reload: 'echeance' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
