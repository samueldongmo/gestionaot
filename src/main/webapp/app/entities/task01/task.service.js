(function() {
    'use strict';
    angular
        .module('gestionaotApp')
        .factory('Task', Task);

    Task.$inject = ['$resource', 'DateUtils'];

    function Task ($resource, DateUtils) {
        var resourceUrl =  'api/tasks/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.lastStatusDate = DateUtils.convertDateTimeFromServer(data.lastStatusDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
