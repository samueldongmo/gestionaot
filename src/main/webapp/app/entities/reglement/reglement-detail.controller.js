'use strict';

angular.module('gestionaotApp')
    .controller('ReglementDetailController', ['$scope', '$rootScope', '$stateParams', 'entity', 'reglementFactures', 'Reglement', 'Facture', 'ReglementFactureService',
                                              function ($scope, $rootScope, $stateParams, entity, reglementFactures, Reglement, Facture, ReglementFactureService) {
        $scope.reglement = entity;
        $scope.reglementFactures = reglementFactures;
        $scope.load = function (id) {
            Reglement.get({id: id}, function(result) {
                $scope.reglement = result;
            });
            ReglementFactureService.findByReglement({id: id}, function(result){
            	$scope.reglementFactures = result;
            })
        };
        var unsubscribe = $rootScope.$on('gestionaotApp:reglementUpdate', function(event, result) {
            $scope.reglement = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }]);
