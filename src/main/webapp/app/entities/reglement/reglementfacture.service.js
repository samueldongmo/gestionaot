'use strict';

angular.module('gestionaotApp')
    .factory('ReglementFactureService', ['$http',function ($http) {
        return {
            
            findByReglement: function (reglement) {      	
                return $http.get('api/reglementfactures/reglement/'+ reglement.id).then(function (response) {
                    return response.data;
                });
            },
            findByFacture: function (facture) {

                return $http.get('api/reglementfactures/facture/'+ facture.id).then(function (response) {
                    return response.data;
                });
            }
        };
    }]);
