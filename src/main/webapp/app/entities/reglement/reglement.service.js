'use strict';

angular.module('gestionaotApp')
    .factory('Reglement', ['$resource', 'DateUtils', function ($resource, DateUtils) {
        return $resource('api/reglements/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.dateReglement = DateUtils.convertLocalDateFromServer(data.dateReglement);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.dateReglement = DateUtils.convertLocalDateToServer(data.dateReglement);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.dateReglement = DateUtils.convertLocalDateToServer(data.dateReglement);
                    return angular.toJson(data);
                }
            }
        });
    }]);
