'use strict';

angular.module('gestionaotApp')
	.controller('ReglementDeleteController', ['$scope', '$uibModalInstance', 'entity', 'Reglement', function($scope, $uibModalInstance, entity, Reglement) {

        $scope.reglement = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Reglement.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    }]);
