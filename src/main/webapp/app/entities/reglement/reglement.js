'use strict';

angular.module('gestionaotApp')
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('reglement', {
                parent: 'entity',
                url: '/reglements',
                data: {
                    authorities: ['LISTER_REGLEMENT','GERER_REGLEMENT','ROLE_ADMIN'],
                    pageTitle: 'gestionaotApp.reglement.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/reglement/reglements.html',
                        controller: 'ReglementController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('reglement');
                        $translatePartialLoader.addPart('global');
                        $translatePartialLoader.addPart('facture');
                        return $translate.refresh();
                    }]
                }
            })
            .state('reglement.detail', {
                parent: 'entity',
                url: '/reglement/{id}',
                data: {
                    authorities: ['ROLE_USER_2','ROLE_USER_3','GERER_REGLEMENT','ROLE_ADMIN'],
                    pageTitle: 'gestionaotApp.reglement.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/reglement/reglement-detail.html',
                        controller: 'ReglementDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('reglement');
                        $translatePartialLoader.addPart('facture');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Reglement', function($stateParams, Reglement) {
                        return Reglement.get({id : $stateParams.id});
                    }],
                    reglementFactures : ['$stateParams', 'ReglementFactureService', function($stateParams, ReglementFactureService) {
                        return ReglementFactureService.findByReglement({id : $stateParams.id});
                    }],
                // Ajout de ma part
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'reglement',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
                }
            })
            .state('reglement.new', {
                parent: 'reglement',
                url: '/new',
                data: {
                    authorities: ['CREER_REGLEMENT','GERER_REGLEMENT','ROLE_ADMIN'],
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/reglement/reglement-dialog.html',
                        controller: 'ReglementDialogController',
                        size: 'lg'
                        //controllerAs: 'vm'
                    }
                },

                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('reglement');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return {
                            ref: null,
                            libelle: null,
                            montant: null,
                            dateReglement: null,
                            id: null
                        };
                    },
                    clients: ['ClientCustom', function(ClientCustom) {
                        return ClientCustom.findAll();
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'reglement',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }


                /*onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/reglement/reglement-dialog.html',
                        controller: 'ReglementDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    ref: null,
                                    libelle: null,
                                    montant: null,
                                    dateReglement: null,
                                    id: null
                                };
                            },
                            clients: ['ClientCustom', function(ClientCustom) {
                            	return ClientCustom.findAll();
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('reglement', null, { reload: true });
                    }, function() {
                        $state.go('reglement');
                    })
                }]*/
            })
            .state('reglement.edit', {
                parent: 'reglement',
                url: '/{id}/edit',
                data: {
                    authorities: ['MODIFIER_REGLEMENT','GERER_REGLEMENT','ROLE_ADMIN'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/reglement/reglement-dialog.html',
                        controller: 'ReglementDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Reglement', function(Reglement) {
                                return Reglement.get({id : $stateParams.id});
                            }],
                            clients: ['ClientCustom', function(ClientCustom) {
                            	return ClientCustom.findAll();
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('reglement', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('reglement.delete', {
                parent: 'reglement',
                url: '/{id}/delete',
                data: {
                    authorities: ['SUPPRIMER_REGLEMENT','GERER_REGLEMENT','ROLE_ADMIN'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/reglement/reglement-delete-dialog.html',
                        controller: 'ReglementDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Reglement', function(Reglement) {
                                return Reglement.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('reglement', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    }]);
