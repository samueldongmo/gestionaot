'use strict';

angular.module('gestionaotApp').controller('ReglementDialogController',
    ['$scope', '$state', '$stateParams', '$filter', 'entity','clients', 'Reglement', 'Facture','Client','ClientReleveService','DateUtils', 'previousState',
        function($scope, $state, $stateParams, $filter, entity,clients, Reglement, Facture,Client,ClientReleveService, DateUtils, previousState) {


    	$scope.clients=clients;
        $scope.reglement = entity;
        $scope.reglementfactures = [];
        $scope.montantvalide = false;
        $scope.modeReglementAutreSelected = false;
        
        $scope.load = function(id) {
            Reglement.get({id : id}, function(result) {
                $scope.reglement = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('gestionaotApp:reglementUpdate', result);
            //$uibModalInstance.close(result);
            $state.go('reglement-detail', {id: result.id});
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            $scope.reglement.reglementFactures = $scope.reglementfactures;
            if ($scope.reglement.id != null) {
                Reglement.update($scope.reglement, onSaveSuccess, onSaveError);
            } else {
                Reglement.save($scope.reglement, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $state.go(previousState.name, previousState.params);
            //$uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForDateReglement = {};

        $scope.datePickerForDateReglement.status = {
            opened: false
        };

        $scope.datePickerForDateReglementOpen = function($event) {
            $scope.datePickerForDateReglement.status.opened = true;
        };

        $scope.onChangeClient = function(item){
            $scope.reglement.clientId = item.id;
        	$scope.selectedId = $scope.reglement.clientId;
        	$scope.fillFacture();
        };

        $scope.fillFacture = function(){
        	ClientReleveService.getFacturesFromClient($scope.selectedId).then(function(data){
        		var factures = data;
        		$scope.reglementfactures = [];
        		$scope.reglementfactures.length = factures.length;

        		for(var i = 0; i < factures.length; i++){
        			var reglementfacture ={};

                    //var facture = factures[i];
                    reglementfacture.facture=factures[i];
                    reglementfacture.montant=0;
                    $scope.reglementfactures[i] = reglementfacture;
                }
        	});
        };


        $scope.dayRetard = function (dateEcheance) {

        	var today = new Date();
        	 var dateFormat = 'yyyy-MM-dd';
             var fromDate = $filter('date')(today, dateFormat);

        	var dateTo = new Date(fromDate);
        	var dateFrom = new Date(dateEcheance);
        	var millisecondDiff =  dateTo.getTime() - dateFrom.getTime();
            return millisecondDiff/(24*60*60*1000);
        };

        $scope.getTotalMontantFacture = function(){
            var total = 0;
            for(var i = 0; i < $scope.reglementfactures.length; i++){
                var facture = $scope.reglementfactures[i].facture;
                total += parseInt(facture.montant);
            }
            return total;
        };

        $scope.getTotalMontantRegle = function(){
            var total = 0;
            for(var i = 0; i < $scope.reglementfactures.length; i++){
                var facture = $scope.reglementfactures[i].facture;
                total += parseInt(facture.montantRegle);
            }
            return total;
        };

        $scope.getTotalMontantARegler = function(){
            var total = 0;
            for(var i = 0; i < $scope.reglementfactures.length; i++){
                var reglementfacture = $scope.reglementfactures[i];
                if(!angular.isUndefined(reglementfacture.montant)){
                  total += parseInt(reglementfacture.montant);
                }
            }
            return total;
        };

        $scope.onChangeMontantARegler = function(reglementfacture){
        	if(reglementfacture.montant > (reglementfacture.facture.montant - reglementfacture.facture.montantRegle)){
        		$scope.reglerFacture(reglementfacture);
        	}
        	$scope.getTotalMontantARegler();
        };

        $scope.reglerFacture = function(reglementfacture){
        	reglementfacture.montant = reglementfacture.facture.montant - reglementfacture.facture.montantRegle;
        	//$scope.getTotalMontantARegler();
        };

        $scope.getTexteValidation = function(){
        	var source = $scope.reglement.montant;
        	var target = $scope.getTotalMontantARegler();
        	if(source != target){
        		$scope.montantvalide = false;
        		return "Le montant du reglement [" + $filter('currency')(source) + "] est différent de la somme des reglements des factures " + $filter('currency')(target);
        	}else{
        		$scope.montantvalide = true;
        		return "";
        	}
        };

        $scope.modeReglementSelected = function(modeReglement) {
        	if(modeReglement && 'AUTRE' == modeReglement) {
        		$scope.modeReglementAutreSelected = true;
        	} else {
        		$scope.modeReglementAutreSelected = false;
        	}
        };

        $scope.saveDisabled = function () {
        	if(!$scope.reglement.ref) return true;
        	if(!$scope.reglement.montant) return true;
        	if(!$scope.reglement.modeReglement) return true;
        	if(($scope.modeReglementAutreSelected===true) && !$scope.reglement.infoReglement) return true;
        };

}]);
