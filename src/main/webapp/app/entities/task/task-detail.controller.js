(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('TaskDetailController', TaskDetailController);

    TaskDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Task', 'User', '$state'];

    function TaskDetailController($scope, $rootScope, $stateParams, previousState, entity, Task, User, $state) {
        var vm = this;

        vm.task = entity;
        vm.previousState = previousState;
        vm.execute = execute;

        var unsubscribe = $rootScope.$on('gestionaotApp:taskUpdate', function(event, result) {
            vm.task = result;
        });
        $scope.$on('$destroy', unsubscribe);


        function execute (task) {
        	if(angular.isUndefined(task)) return;
        	if((task.action == 'VALIDATE') && (task.entityName == 'Amodiataire')) {
        		$state.go('amodiataire-detail-validate', {id : task.entityId, validate: true});
        		return;
        	}
        }
    }
})();
