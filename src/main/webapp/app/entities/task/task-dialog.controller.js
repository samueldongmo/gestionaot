(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('TaskDialogController', TaskDialogController);

    TaskDialogController.$inject = ['$timeout', '$scope', '$state', '$stateParams', 'entity', 'Task', 'User', 'previousState'];

    function TaskDialogController ($timeout, $scope, $state, $stateParams, entity, Task, User, previousState) {
        var vm = this;

        vm.task = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            // $uibModalInstance.dismiss('cancel');
            $state.go(previousState.name, previousState.params);
        }

        function save () {
            vm.isSaving = true;
            vm.task.creator.authorities = [];
            vm.task.assignee.authorities = [];
            if (vm.task.id !== null) {
                Task.update(vm.task, onSaveSuccess, onSaveError);
            } else {
                Task.save(vm.task, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('gestionaotApp:taskUpdate', result);
            $state.go('task-detail', {id: result.id});
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.lastStatusDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
