(function() {
    'use strict';
    angular
        .module('gestionaotApp')
        .factory('Point', Point);

    Point.$inject = ['$resource'];

    function Point ($resource) {
        var resourceUrl =  'api/points/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
