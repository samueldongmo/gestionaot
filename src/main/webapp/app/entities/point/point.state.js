(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('point', {
            parent: 'entity',
            url: '/point?page&sort&search',
            data: {
                authorities: ['LISTER_POINT','GERER_POINT','ROLE_ADMIN'],
                pageTitle: 'gestionaotApp.point.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/point/points.html',
                    controller: 'PointController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('point');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('point-detail', {
            parent: 'point',
            url: '/point/{id}',
            data: {
                authorities: ['LISTER_POINT','GERER_POINT','ROLE_ADMIN'],
                pageTitle: 'gestionaotApp.point.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/point/point-detail.html',
                    controller: 'PointDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('point');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Point', function($stateParams, Point) {
                    return Point.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'point',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('point-detail.edit', {
            parent: 'point-detail',
            url: '/detail/edit',
            data: {
                authorities: ['MODIFIER_POINT','GERER_POINT','ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/point/point-dialog.html',
                    controller: 'PointDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Point', function(Point) {
                            return Point.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('point.new', {
            parent: 'point',
            url: '/new',
            data: {
                authorities: ['CREER_POINT','GERER_POINT','ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/point/point-dialog.html',
                    controller: 'PointDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                lat: null,
                                lng: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('point', null, { reload: 'point' });
                }, function() {
                    $state.go('point');
                });
            }]
        })
        .state('point.edit', {
            parent: 'point',
            url: '/{id}/edit',
            data: {
                authorities: ['MODIFIER_POINT','GERER_POINT','ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/point/point-dialog.html',
                    controller: 'PointDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Point', function(Point) {
                            return Point.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('point', null, { reload: 'point' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('point.delete', {
            parent: 'point',
            url: '/{id}/delete',
            data: {
                authorities: ['SUPPRIMER_POINT','GERER_POINT','ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/point/point-delete-dialog.html',
                    controller: 'PointDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Point', function(Point) {
                            return Point.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('point', null, { reload: 'point' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
