(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('PointDialogController', PointDialogController);

    PointDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Point', 'Amodiataire'];

    function PointDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Point, Amodiataire) {
        var vm = this;

        vm.point = entity;
        vm.clear = clear;
        vm.save = save;
        vm.amodiataires = Amodiataire.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.point.id !== null) {
                Point.update(vm.point, onSaveSuccess, onSaveError);
            } else {
                Point.save(vm.point, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('gestionaotApp:pointUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
