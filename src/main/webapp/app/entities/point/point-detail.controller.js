(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('PointDetailController', PointDetailController);

    PointDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Point', 'Amodiataire'];

    function PointDetailController($scope, $rootScope, $stateParams, previousState, entity, Point, Amodiataire) {
        var vm = this;

        vm.point = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('gestionaotApp:pointUpdate', function(event, result) {
            vm.point = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
