(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('PointDeleteController',PointDeleteController);

    PointDeleteController.$inject = ['$uibModalInstance', 'entity', 'Point'];

    function PointDeleteController($uibModalInstance, entity, Point) {
        var vm = this;

        vm.point = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Point.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
