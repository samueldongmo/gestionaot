'use strict';

angular.module('gestionaotApp')
    .factory('ClientReleveService', ['$http',function ($http) {
        return {
        	getFacturesFromClient: function (idclient) {
                return $http.get('api/factures/client/',{params: {idclient: idclient}}).then(function (response) {
                    return response.data;
                });
            }
        };
    }]);
