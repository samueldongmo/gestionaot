'use strict';

angular.module('gestionaotApp')
    .factory('Client', ['$resource', 'DateUtils', function ($resource, DateUtils) {
        return $resource('api/amodiataires/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }])
    .factory('ClientCustom', ['$http','Amodiataire',function ($http,Amodiataire) {
        return {
            findAll: function () {
                /*return $http.get('api/clients/withoutpage').then(function (response) {
                    return response.data;
                });*/
            	return Amodiataire.query();
            }
        };
    }]);
