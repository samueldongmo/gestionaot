(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('AmodiataireDialogController', AmodiataireDialogController);

    AmodiataireDialogController.$inject = ['$timeout', '$scope', '$stateParams', 'entity', '$state', 'previousState', 'Amodiataire'];

    function AmodiataireDialogController ($timeout, $scope, $stateParams, entity, $state, previousState, Amodiataire) {
        var vm = this;

        vm.amodiataire = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.points = entity.points || [];

        vm.point = {};
        vm.loadPoints = loadPoints;
        vm.editPoint = editPoint;
        vm.removePoint = removePoint;
        vm.addPoint = addPoint;

        vm.dateOk = false;
        vm.validateExpirationDate = validateExpirationDate;
        vm.computeAmounts = computeAmounts;
        vm.annuler = annuler;
        vm.isDisabled = isDisabled;

        $scope.dateformat = "dd/MM/yyyy";

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        $scope.$watch('vm.points', function(points, oldPoints) {
            // On ne peux pas calculer la surface de zero a trois points
            if(!points || points.length <= 3) return;

            // Trier le tableau
            points = sortPoints(points);

            // Construire le lineStringe
            var lineString = createLineString(points);

            // Calculer la surface
            vm.computedArea = turf.area(lineString);

            // Comparer
            vm.differenceSuperficie = 0;
            if( vm.amodiataire && vm.amodiataire.superficie && vm.computedArea) {
                vm.differenceSuperficie = vm.amodiataire.superficie - vm.computedArea;
            }
        });

        function sortPoints(points) {
            points.sort(function(p1, p2) {
                if(p1.numBorne > p2.numBorne) return 1;
                if(p1.numBorne < p2.numBorne) return -1;
                return 0;
            });
            return points;
        }
        function createLineString(points) {
            // Creer le tableau de points [lat, long]
            var pointArray = createPositions(points);
            var polygon = [];
            polygon.push(pointArray);
            // Creer le lineString
            var lineString = turf.polygon(polygon);
            // Retourner le lineString
            return lineString;
        }

        function createPositions(points) {
            if(!points || points.length == 0) return [];
            var result = [];
            angular.forEach(points, function(point) {
                var val = [point.lat, point.lng];
                result.push(val);
            });
            return result;
        }

        function clear () {
            $state.go(previousState.name, previousState.params);
        }

        function save () {
        	vm.pointErrorMessage = "";
        	vm.pointWarningMessage = "";
        	if(hasValue(vm.point)) {
        		vm.pointWarningMessage = "Vous avez un point en edition. Veuillez l'ajouter ou l'annuller et enregistrer de nouveau !";
        		return;
        	}
            vm.isSaving = true;

            vm.amodiataire.points = vm.points;
            if (vm.amodiataire.id) {
                Amodiataire.update(vm.amodiataire, onSaveSuccess, onSaveError);
            } else {
                Amodiataire.saveDto(vm.amodiataire, onSaveSuccess, onSaveError);
            }
        }

        function hasValue(point) {
        	return point && (point.numBorne || point.lat || point.lng);
        }



        function onSaveSuccess (result) {
            $scope.$emit('gestionaotApp:amodiataireUpdate', result);
            $state.go('amodiataire-detail', {id: result.id});
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateSouscription = false;
        vm.datePickerOpenStatus.dateEcheance = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }

        /* ========================================= */
        function validateExpirationDate () {
        	// check end date match issuance date
        	updateExpirationDate(vm.amodiataire.duree, vm.amodiataire.dateSignature);
        }

        function updateExpirationDate(duree, dateSignature) {
        	if(angular.isNumber(duree) && dateSignature) {
        		vm.amodiataire.dateExpiration = moment(dateSignature).add(duree,'years').toDate();
        	}
        }

        function computeAmounts() {
        	computeAmount2s(vm.amodiataire.superficie, vm.amodiataire.tarifHorsTaxe);
        }
        function computeAmount2s(superficie, tarifHorsTaxe) {
        	if(angular.isNumber(superficie) && angular.isNumber(tarifHorsTaxe)) {
        		var nbTrimestre = 4;
        		var tarifTTC = tarifHorsTaxe + ( (tarifHorsTaxe * 19.25)/100);
        		var montantAnnuelTTC = tarifTTC * superficie;

        		vm.amodiataire.montantTrimestrielHT = Math.ceil((superficie * tarifHorsTaxe)/nbTrimestre);
        		vm.amodiataire.montantTrimestrielTTC = Math.ceil(montantAnnuelTTC/nbTrimestre);
        	}
        }
        /* ========================================= */

        /**
         * Load points of an amodiataire
         */
        function loadPoints(amodiataireId) {
        	Amodiataire.loadPoints(amodiataireId, function(result) {
        		vm.points = result;
        	})
        };

        /**
         * Comparer two points
         */
        function compare(p1, p2) {
        	if(!p1 || !p2) return false;
        	if( (angular.isNumber(p1.id) && angular.isNumber(p2.id))) {
        		return p1.id == p2.id;
        	}
        	if( angular.isNumber (p1.key) && angular.isNumber(p2.key)) {
        		return p1.key === p2.key;
        	}
        	return false;
        }

        /**
         * Select a point for edition.
         */
        function editPoint(point) {
        	var copy = angular.copy(point);
        	vm.point = copy;
        }

        /**
         * Remove a point
         * @param point The point to edit
         * @confirm boolean Ask for confirmation before you edit
         */
        function removePoint(point, confirm) {
        	var index = -1;
        	for(var i = 0; i < vm.points.length; i++) {
        		var p = vm.points[i];
        		if(compare(point, p)) {
        			index = i;
        			break;
        		}
        	}
        	if(index === -1) return;
        	if(confirm) {
        		// Ask for confirmation
            	if(window.confirm('Supprimer ce point ?')) {
            		vm.points.splice(index, 1);
            	}
        	} else {
        		vm.points.splice(index, 1);
        	}
        }

        /**
         * Add a point to the list
         */
        function addPoint() {
        	vm.pointErrorMessage = "";
        	vm.pointWarningMessage = "";
        	if(vm.point && vm.point.numBorne && vm.point.lng && vm.point.lat) {
        		if(vm.point.id) {
        			vm.point.key = vm.point.id;
        		}
        		if(!angular.isNumber(vm.point.key)) {
        			vm.point.key = vm.points.length;
        		}
        		var confirm = false;
        		removePoint(vm.point, confirm);
        		var copy = angular.copy(vm.point);
        		vm.points.push(copy);
        		vm.point = {};
        	} else {
            	vm.pointErrorMessage = "Champs Insuffisants";
        	}
        }

        function annuler () {
        	vm.pointErrorMessage = "";
        	vm.pointWarningMessage = "";
        	vm.point = {};
        }

        /*
         * Return true if the user can edit, and false otherwise
         */
        function isDisabled () {
        	return vm.amodiataire && vm.amodiataire.id;
        }

    }
})();
