(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('amodiataire', {
            parent: 'entity',
            url: '/amodiataire?page&sort&search',
            data: {
                authorities: ['CREER_AOT','LISTER_AOT','GERER_AOT','ROLE_ADMIN'],
                pageTitle: 'gestionaotApp.amodiataire.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/amodiataire/amodiataires.html',
                    controller: 'AmodiataireController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('amodiataire');
                    $translatePartialLoader.addPart('global');
                    $translatePartialLoader.addPart('point');
                    return $translate.refresh();
                }]
            }
        })
        .state('amodiataire-detail', {
            parent: 'amodiataire',
            url: '/amodiataire/{id}',
            data: {
                authorities: ['LISTER_AOT','GERER_AOT','ROLE_ADMIN'],
                pageTitle: 'gestionaotApp.amodiataire.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/amodiataire/amodiataire-detail.html',
                    controller: 'AmodiataireDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('amodiataire');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Amodiataire', function($stateParams, Amodiataire) {
                    return Amodiataire.findAmodiataireDto({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'amodiataire',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('amodiataire-detail-validate', {
            parent: 'amodiataire',
            url: '/amodiataire/{id}/validate/{validate}',
            data: {
                authorities: ['VALIDER_AOT','GERER_AOT','ROLE_ADMIN'],
                pageTitle: 'gestionaotApp.amodiataire.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/amodiataire/amodiataire-detail.html',
                    controller: 'AmodiataireDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('amodiataire');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Amodiataire', function($stateParams, Amodiataire) {
                    return Amodiataire.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'amodiataire',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        /*.state('amodiataire-detail.edit', {
            parent: 'amodiataire-detail',
            url: '/detail/edit',
            data: {
                authorities: ['CREER_AOT','MODIFIER_AOT','GERER_AOT','ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/amodiataire/amodiataire-dialog.html',
                    controller: 'AmodiataireDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Amodiataire', function(Amodiataire) {
                            return Amodiataire.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })*/
        .state('amodiataire.new', {
            parent: 'amodiataire',
            url: '/new',
            data: {
                authorities: ['CREER_AOT', 'GERER_AOT', 'ROLE_ADMIN']
            },

            views: {
                'content@': {
                    templateUrl: 'app/entities/amodiataire/amodiataire-dialog.html',
                    controller: 'AmodiataireDialogController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('amodiataire');
                    return $translate.refresh();
                }],
                entity: function () {
                    return {
                        numeroAutorisation: null,
                        nomSociete: null,
                        bp: null,
                        ville: null,
                        numeroTelephone: null,
                        niu: null,
                        rccm: null,
                        superficie: null,
                        duree: null,
                        tarifHorsTaxe: null,
                        montantTrimestrielHT: null,
                        montantTrimestrielTTC: null,
                        montantCaution: null,
                        dateSouscription: null,
                        id: null,
                        points: [],
                        dateEcheance: null,
                        dateExpiration: null,
                        dateSignature: null,
                        dateCaution: null
                    };
                },
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'amodiataire',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })

        .state('amodiataire.edit', {
            parent: 'amodiataire',
            url: '/{id}/edit',
            data: {
                authorities: ['CREER_AOT','MODIFIER_AOT','GERER_AOT','ROLE_ADMIN']
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/amodiataire/amodiataire-dialog.html',
                    controller: 'AmodiataireDialogController',
                    controllerAs: 'vm'
                }
            },

            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('amodiataire');
                    return $translate.refresh();
                }],
                entity: ['Amodiataire', '$stateParams', function(Amodiataire, $stateParams) {
                    return Amodiataire.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'amodiataire',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('amodiataire.delete', {
            parent: 'amodiataire',
            url: '/{id}/delete',
            data: {
                authorities: ['SUPPRIMER_AOT','GERER_AOT','ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/amodiataire/amodiataire-delete-dialog.html',
                    controller: 'AmodiataireDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Amodiataire', function(Amodiataire) {
                            return Amodiataire.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('amodiataire', null, { reload: 'amodiataire' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('amodiataire.releve', {
            parent: 'amodiataire',
            url: '/{id}/releve',
            data: {
                authorities: ['CONSULTER_RELEVER','GERER_AOT','ROLE_ADMIN'],
                pageTitle: 'balancegApp.client.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/amodiataire/amodiataire-releve.html',
                    controller: 'ClientReleveController'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('amodiataire');
                    $translatePartialLoader.addPart('global');
                    $translatePartialLoader.addPart('facture');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Client', function($stateParams, Client) {
                    return Client.get({id : $stateParams.id});
                }]
            }
        })
        .state('amodiataire.annuler', {
            parent: 'amodiataire',
            url: '/{id}/annuler',
            data: {
                authorities: ['ANNULER_AOT','GERER_AOT','ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/amodiataire/amodiataire-annuler-dialog.html',
                    controller: 'AmodiataireAnnulerController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Amodiataire', function(Amodiataire) {
                            return Amodiataire.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('amodiataire', null, { reload: 'amodiataire' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
