(function() {
	'use strict';

	angular
	.module('gestionaotApp')
	.controller('AmodiataireController', AmodiataireController);

	AmodiataireController.$inject = ['$state', 'Amodiataire', 'ParseLinks', 'AlertService'
		, 'paginationConstants', 'pagingParams', '$scope','$timeout','olData','$http'];

	function AmodiataireController($state, Amodiataire, ParseLinks, AlertService
			, paginationConstants, pagingParams,$scope,$timeout, olData,$http) {

		var vm = this;
		vm.createAmodiataireLayers = createAmodiataireLayers;
		vm.createMarkers = createMarkers;

		vm.loadPage = loadPage;
		vm.predicate = pagingParams.predicate;
		vm.reverse = pagingParams.ascending;
		vm.transition = transition;
		vm.itemsPerPage = paginationConstants.itemsPerPage;
		vm.viewType = 'list';
		var extent= [668227.0213100001,567169.14612,670921.11539,568864.4641000001];
//		var projection = new ol.proj.Projection({
//		// code: 'EPSG:32632',
//		extent: extent
//		});

		vm.years = initYears(1970); // Débuter l'année avec 1970

		// Projection
		vm.projection = null;

		// Marker
		vm.markers = [];

		// Layers
		$scope.layers = [];
		$scope.markers = [];

		// Filter
		vm.filter = {};
		// Search function
		vm.search = search;
		// Reset function;
		vm.reset = reset;

		// Load data
		loadAll();

		// Load function
		function loadAll () {
			Amodiataire.query({
				page: pagingParams.page - 1,
				size: vm.itemsPerPage,
				sort: sort()
			}, onSuccess, onError);
			function sort() {
				var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
				if (vm.predicate !== 'id') {
					result.push('id');
				}
				return result;
			}
			function onSuccess(data, headers) {
				vm.links = ParseLinks.parse(headers('link'));
				vm.totalItems = headers('X-Total-Count');
				vm.queryCount = vm.totalItems;
				vm.amodiataires = data;
				vm.page = pagingParams.page;
				$scope.layers = createAmodiataireLayers(vm.amodiataires);
				createMarkers(vm.amodiataires);
				$timeout(function(){
//					extendScope();
					$scope.markers = vm.markers;
				}, 10000);
			}
			function onError(error) {
				AlertService.error(error.data.message);
			}
		}

		// Extend scope.; init open layer
		extendScope(vm.projection,$scope,extent);

		// Load page
		function loadPage(page) {
			vm.page = page;
			vm.transition();
		}

		// Transition
		function transition() {
			$state.transitionTo($state.$current, {
				page: vm.page,
				sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
				search: vm.currentSearch
			});
		}

		// Create amodiataire layer
		function createAmodiataireLayers(amodiataires) {

			var geoLayers = [];
			var length = amodiataires ? amodiataires.length : 0;
			var now = moment();
			for(var i = 0; i<length; i++) {
				var amodiataire = amodiataires[i];
				var geoLayer = {};
				geoLayer.name = amodiataire.nomSociete;
				geoLayer.source = {
						type: 'GeoJSON',
						url: 'http://localhost:8082/api/amodiataires/ol/'+amodiataire.id+'/geojson'
				};

				var color ;
				var dateEcheance = amodiataire.dateEcheance;
				var dateEcheanceceM = moment(dateEcheance);
				var diff = dateEcheanceceM.diff(now,'days');
				if(diff <= 0) {
					color = 'rgba(255, 2, 355, 0.9)'; //red color
				} else if( (diff > 0) && (diff <= 20) ) {
					color = 'rgba(255,255,0,0.3)'; // yellow color
				} else {
					color = 'rgba(0,255,0,0.3)'; // Green color. When date null or greatter than 20d
				}
				geoLayer.style = {
						fill: {
							color: color
						},
						stroke: {
							color: 'white',
							width: 3
						}
				};
				geoLayer.label = {
						message: amodiataire.nomSociete+'/'+amodiataire.numeroAutorisation,
						show: true,
						showOnMouseOver: true
				};
				geoLayers.push(geoLayer);
			}
			return geoLayers;
		}

		/*
		 * Create Markers
		 */
		function createMarkers(amodiataires) {
			angular.forEach(amodiataires, function(amodiataire) {
				createMarker(amodiataire);
			});
		}

		/*
		 * Create marker
		 */
		function createMarker(amodiataire) {
			Amodiataire.geojson({id:amodiataire.id}, function(geoson) {
				// Compute the area
				var area = turf.area(geoson);

				// Compute the centroid
				var centroidPt = turf.centerOfMass(geoson);

				var marker = {};
				var features = geoson.features;
				marker.lon = centroidPt.geometry.coordinates[0];
				marker.lat = centroidPt.geometry.coordinates[1];


				marker.label = {
						message : "<span style='color: red;'>"+amodiataire.nomSociete+"</span> ("+area.toFixed(2)+" &#x33a1;)",
						show: true,
						showOnMouseOver: true
				};
				vm.markers.push(marker);
			}, function(error){
				console.log(error);
			});
		}


		/*
		 * Extend scope
		 * @param projection
		 */
		function extendScope(projection,$scope,extent) {
			console.log(projection);
			angular.extend($scope, {
				center: {
					lat: 5.137693083,
					lon: 10.53471683,
					zoom: 17
					/*  lat: 37.7,
                lon: -96.67,
                zoom: 5*/

            },
            customLayer:{
                source:{
                    type: 'ImageWMS',
                    url:'http://localhost:8080/geoserver/cite/wms',
                    params: {LAYERS: 'cite:geotiff_coverage'}
                    //params: {LAYERS: 'cite:geotiff_pad'}
                }

            },
            defaults: {
                // Setting up the

                interactions: {
                    mouseWheelZoom: true
                }
            },
            controls: [
                { name: 'zoom', active: true },
                { name: 'fullscreen', active: true }
            ]


			});
		}


		/*
		 * Extend scope
		 * @param projection
		 */
//		function extendScope(projection) {
//		console.log(projection);
//		angular.extend($scope, {
//		center: {
//		lat: 5.137693083,
//		lon: 10.53471683,
//		zoom: 5
//		},
//		customLayer: {

//		source:{
//		type: 'ImageWMS',
//		url:'http://localhost:8080/geoserver/cite/wms',
//		params: {LAYERS: 'cite:geotiff_coverage'}
//		}

//		},
//		layers : $scope.layers,
//		markers: $scope.markers,
//		defaults: {
//		// Setting up the
//		center: {
//		projection: projection
//		},
//		interactions: {
//		mouseWheelZoom: true
//		},

//		},

//		});
//		}

		/*
		 * Search function
		 */
		function search () {

			$http.post('api/amodiataires/search', vm.filter).then(function(result) {
				vm.amodiataires = result.data;
				$timeout(function(){
					$scope.layers = [];
					$scope.layers = createAmodiataireLayers(vm.amodiataires);
					$scope.markers = vm.markers;
				}, 10000);
			}, function(error ) {
				console.log(error);
			});
//			For some reason, data where not sent to the server using this method.
//			Amodiataire.search(vm.filter, function(data) {
//			vm.amodiataires = data;
//			});
		}

		function reset () {
			vm.filter = {}; // Clear the filter
			loadAll() // Load all
		}

		 /*
         * Init the select field with dates starting from 'startY' to date.
         */
        function initYears(startY) {
        	var currentY = (new Date()).getFullYear();
        	var years = [];

        	if(!startY || startY > currentY) return years;
            var size = currentY - startY; // Start 20 years before
            for(var i = 0; i<= size; i ++) {
            	years[i] = startY + i;
            }
            return years;
        }
	}
})();
