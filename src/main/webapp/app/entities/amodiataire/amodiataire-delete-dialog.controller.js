(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('AmodiataireDeleteController',AmodiataireDeleteController);

    AmodiataireDeleteController.$inject = ['$uibModalInstance', 'entity', 'Amodiataire'];

    function AmodiataireDeleteController($uibModalInstance, entity, Amodiataire) {
        var vm = this;

        vm.amodiataire = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Amodiataire.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
