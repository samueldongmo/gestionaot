(function() {
	'use strict';
	angular.module('gestionaotApp').factory('Amodiataire', Amodiataire);

	Amodiataire.$inject = [ '$resource', 'DateUtils' ];

	function Amodiataire($resource, DateUtils) {
		var resourceUrl = 'api/amodiataires/:id';

		return $resource(
				resourceUrl,
				{},
				{
					'query' : {
						method : 'GET',
						isArray : true
					},
					'get' : {
						method : 'GET',
						transformResponse : function(data) {
							if (data) {
								data = angular.fromJson(data);
								data.dateSouscription = DateUtils
										.convertLocalDateFromServer(data.dateSouscription);
								data.dateEcheance = DateUtils
										.convertLocalDateFromServer(data.dateEcheance);
								data.dateCaution = DateUtils
										.convertLocalDateFromServer(data.dateCaution);
								data.dateSignature = DateUtils
										.convertLocalDateFromServer(data.dateSignature);
								data.dateExpiration = DateUtils
										.convertLocalDateFromServer(data.dateExpiration);
							}
							return data;
						}
					},
					'update' : {
						method : 'PUT',
						transformRequest : function(data) {
							var copy = angular.copy(data);
							copy.dateSouscription = DateUtils
									.convertLocalDateToServer(copy.dateSouscription);
							return angular.toJson(copy);
						}
					},
					'save' : {
						method : 'POST',
						transformRequest : function(data) {
							var copy = angular.copy(data);
							copy.dateSouscription = DateUtils
									.convertLocalDateToServer(copy.dateSouscription);
							return angular.toJson(copy);
						}
					},
					'saveDto' : {
						url: 'api/amodiataires/dto',
						method : 'POST',
						transformRequest : function(data) {
							var copy = angular.copy(data);
							copy.dateSouscription = DateUtils
									.convertLocalDateToServer(copy.dateSouscription);
							return angular.toJson(copy);
						}
					},
					'search' : {
						method : 'POST',
						url : 'api/amodiataires/search',
						isArray : true,
						transformRequest : function(data) {
							console.log(data);
							return data;
						}
					},
					'geojson' : {
						method : 'GET',
						url : 'api/amodiataires/ol/:id/geojson'
					},
					'loadPoints' : {
						method : 'GET',
						url : 'api/amodiataires/:id/points',
						isArray: true
					},
					'validerAot' : {
						method : 'POST',
						url : 'api/amodiataires/valider',
						isArray: true
					},
					'findAllDto': {
						method: 'GET',
						url: 'api/amodiataires/dto',
						isArray: true
					},
					'findEnAttente': {
						method: 'GET',
						url: 'api/amodiataires/enattente',
						isArray: true
					},
					'findAmodiataireDto': {
						method: 'GET',
						url: 'api/amodiataires/:id/dto',
					},
					'annuler': {
						method: 'PUT',
						url: 'api/amodiataires/:id/annuler',
					}
				});
	}
})();
