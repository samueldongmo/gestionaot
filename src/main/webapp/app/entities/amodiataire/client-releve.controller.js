'use strict';

angular.module('gestionaotApp')
    .controller('ClientReleveController', ['$scope', '$rootScope', '$filter', 'entity', 'Client', 'ClientReleveService',
                                           function ($scope, $rootScope, $filter, entity, Client, ClientReleveService) {
    	
    	//var param = $routeParams.id;
    	
    	entity.$promise.then(function(data){
    		$scope.selectedClient = data;	
    		$scope.onChangeClient();
    	});
    	$scope.clients = Client.query();
    	//$scope.factures = ClientReleveService.getFacturesFromClient($scope.selectedClient.id);
    	//$scope.selectedId = null;
        $scope.onChangeClient = function () {
        	//if($scope.selectedClient == null) return;
        	$scope.selectedId = $scope.selectedClient.id;
        	//if($scope.selectedId == null || $scope.selectedId == "") return;
        	ClientReleveService.getFacturesFromClient($scope.selectedId).then(function(data){
        	    $scope.factures = data;	
        	});
          
        };

        
        $scope.load = function (id) {
            Client.get({id: id}, function(result) {
                $scope.selectedClient = result;
                $scope.onChangeClient();
            });
        };
        var unsubscribe = $rootScope.$on('balancegApp:clientUpdate', function(event, result) {
            $scope.client = result;
        });
        $scope.$on('$destroy', unsubscribe);
        
        
        $scope.dayRetard = function (dateEcheance) {
        	
        	var today = new Date();
        	 var dateFormat = 'yyyy-MM-dd';
             var fromDate = $filter('date')(today, dateFormat);
             
        	var dateTo = new Date(fromDate);
        	var dateFrom = new Date(dateEcheance);
        	var millisecondDiff =  dateTo.getTime() - dateFrom.getTime();
            return millisecondDiff/(24*60*60*1000);
        };
       
        $scope.getTotalMontantFacture = function(){
        	if(!$scope.factures)return 0;
            var total = 0;
            for(var i = 0; i < $scope.factures.length; i++){
                var facture = $scope.factures[i];
                total += facture.montant;
            }
            return total;
        }
        
        $scope.getTotalMontantRegle = function(){
        	if(!$scope.factures)return 0;
            var total = 0;
            for(var i = 0; i < $scope.factures.length; i++){
                var facture = $scope.factures[i];
                total += facture.montantRegle;
            }
            return total;
        }
        
        $scope.getSolde = function(){
        	var totalFacture = $scope.getTotalMontantFacture();
        	var totalRegle = $scope.getTotalMontantRegle();
        	
        	return totalFacture - totalRegle;
        	
        }
        
      
    }]);
