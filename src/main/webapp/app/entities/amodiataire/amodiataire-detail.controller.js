(function() {
	'use strict';
	angular
	.module('gestionaotApp')
	.controller('AmodiataireDetailController', AmodiataireDetailController);

	AmodiataireDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState',
		'entity', 'Amodiataire', '$timeout','$window'];

	function AmodiataireDetailController($scope, $rootScope,
			$stateParams, previousState, entity, Amodiataire, $timeout,$window) {
		var vm = this;

		vm.amodiataire = entity;
		vm.previousState = previousState;

		vm.validate = $stateParams.validate;
		vm.validerAot = validerAot;
		var unsubscribe = $rootScope.$on('gestionaotApp:amodiataireUpdate', function(event, result) {
			vm.amodiataire = result;
		});

		var projection = null
		vm.marker = {};
		$rootScope.marker = {};
		$scope.$on('$destroy', unsubscribe);
		
		var projection = null
		vm.marker = {};
		$rootScope.marker = {};
		$scope.$on('$destroy', unsubscribe);
		
		var extent= [668227.0213100001,567169.14612,670921.11539,568864.4641000001];
//		var projection = new ol.proj.Projection({
//		// code: 'EPSG:32632',
//		extent: extent
//		});

		var now = moment();
		var color ;
		var dateEcheance = vm.amodiataire.dateEcheance;
		var dateEcheanceceM = moment(dateEcheance);
		var diff = dateEcheanceceM.diff(now,'days');
		if(diff <= 0) {
			color = 'rgba(255, 2, 355, 0.9)';
		} else if( (diff > 0) && (diff <= 20)) {
			color = 'rgba(255,255,0,0.3)';
		} else {
			color = 'rgba(0,255,0,0.3)';
		}

		//Create marker
		createMarker(vm.amodiataire);

		//RETRIEVE VALUE
		$scope.marker =  JSON.parse($window.sessionStorage.getItem("marker"));
		
		//$scope.marker = JSON.parse(window.localStorage.get("marker"));
		
		
		
		//console.log($scope.marker)
		
		angular.extend($scope, {
			center: {
                    lat: 5.137693,
                    lon: 10.53471,
                    zoom: 17
                },

			/*center: {
				lat: $scope.marker.lat,
				lon: $scope.marker.lon,
				zoom: 19
			},*/
			customLayer: {
//				source: {
//	type: 'ImageWMS',
//url: 'http://localhost:8080/geoserver/wms',
//				params: {
//				LAYERS:'tracking-port:bangante_compresse_10',
//				serverType: 'geoserver'
//				},
//				extent: extent,
//				attribution: 'State Boundaries &copy; <a href="http://www.dummyurl.com/" target="_blank">Data Provider1</a>'
//				}

				source:{
					type: 'ImageWMS',
					url:'http://localhost:8080/geoserver/cite/wms',
					params: {LAYERS: 'cite:geotiff_coverage'}
				}
			},
			geoData: {
				name: 'Lot',
				source: {
					type: 'GeoJSON',
					url: 'http://localhost:8082/api/amodiataires/ol/'+entity.id+'/geojson'
				},
				style: {
					fill: {
						color: color
					},
					stroke: {
						color: 'white',
						width: 3
					}
				}
			},
			defaults: {
				// Setting up the
				center: {
					projection: projection
				},
				interactions: {
					mouseWheelZoom: true
				},
				view: {
					center: [5.137693083, 10.53471683],
					//center: [vm.marker.lon, vm.marker.lat],
					zoom: 19,
					projection: projection
				}
			},
			view: {
				center: [vm.marker.lon, vm.marker.lat],
				zoom: 19,
				projection: projection
			}
		});


		// Create marker
		// createMarker(vm.amodiataire);

		/*
		 * Create marker
		 */
		function createMarker(amodiataire) {
			Amodiataire.geojson({id:amodiataire.id}, function(geoson) {
				// Compute the area
				var area = turf.area(geoson);

				// Compute the centroid
				var centroidPt = turf.centerOfMass(geoson);

				//vm.marker = {};
				var features = geoson.features;
				vm.marker.lon = centroidPt.geometry.coordinates[0];
				vm.marker.lat = centroidPt.geometry.coordinates[1];

				//$window.sessionStorage.setItem("marker",JSON.stringify(vm.marker));

				//window.localStorage.set("marker",JSON.stringify(vm.marker));
				
				$window.sessionStorage.setItem("marker",JSON.stringify(vm.marker));
				
				vm.marker.label = {
						message : "<span style='color: red;'>"+amodiataire.nomSociete+"</span> ("+area.toFixed(2)+" &#x33a1;)",
						show: true,
						showOnMouseOver: true
				};
			}, function(error){
				console.log(error);
			});
		}

		//createMarker(vm.amodiataire);

		$timeout(function(){
			$scope.marker = vm.marker;
		}, 1000);
		
		function validerAot () {
			
        	Amodiataire.validerAot({id:vm.amodiataire.id}, successCallback);

        	function successCallback(response) {
        		vm.validate = false;
        	}
        }

	}
})();