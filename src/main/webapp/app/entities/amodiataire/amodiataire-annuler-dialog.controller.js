(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('AmodiataireAnnulerController',AmodiataireAnnulerController);

    AmodiataireAnnulerController.$inject = ['$uibModalInstance', 'entity', 'Amodiataire'];

    function AmodiataireAnnulerController($uibModalInstance, entity, Amodiataire) {
        var vm = this;

        vm.amodiataire = entity;
        vm.clear = clear;
        vm.confirmAnnuler = confirmAnnuler;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmAnnuler (id) {
            Amodiataire.annuler(vm.amodiataire,
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
