(function() {
    /*jshint bitwise: false*/
    'use strict';

    angular
        .module('gestionaotApp')
        .factory('UrlUtil', UrlUtil);

    function UrlUtil () {

        var service = {
            getHostName : getHostName,
            getGeoServerLocation : getGeoServerLocation,
            getGeoServiceLocation : getGeoServiceLocation,
        };

        return service;

        function getHostName () {
            var url = 'http://'+window.location.hostname;
            return url;
        }

        function getGeoServerLocation() {
            return getHostName()+':8080';
        }

        function getGeoServiceLocation() {
            return getHostName()+':8081';
        }
    }
})();
