/**
 * A color utils file
 */

var AotColor = (function() {
	var color = {};
	
	color.red = '#ff0000';
	color.orange = '#ff7f00';
	color.yellow = '#FFFF00';
	color.green = '#00FF00';
	
	return color;
})();