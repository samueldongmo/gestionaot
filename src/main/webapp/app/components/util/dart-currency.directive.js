(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .directive('dartCurrency', dartCurrency);
    
    dartCurrency.$inject = ['$filter'];

    function dartCurrency($filter) {
    	return {
		    scope: {
		      ngModel  : '='
		    },
		    link: function(scope, el, attrs){
		      el.val($filter('currency')(scope.ngModel));
		      
		      el.bind('focus', function(){
		        el.val(scope.ngModel);
		      });
		      
		      el.bind('input', function(){
		        scope.ngModel = el.val();
		        scope.$apply();
		      });
		      
		      el.bind('blur', function(){
		        el.val($filter('currency')(scope.ngModel,'',0));
		      });
		    }
		  }
    }
})();
