(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
