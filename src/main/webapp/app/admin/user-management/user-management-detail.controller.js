(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('UserManagementDetailController', UserManagementDetailController);

    UserManagementDetailController.$inject = ['$stateParams', 'User', 'previousState'];

    function UserManagementDetailController ($stateParams, User, previousState) {
        var vm = this;

        vm.load = load;
        vm.user = {};
        vm.previousState = previousState;

        vm.load($stateParams.login);

        function load (login) {
            User.get({login: login}, function(result) {
                vm.user = result;
            });
        }
    }
})();
