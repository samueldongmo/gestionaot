(function() {
    'use strict';

    angular
        .module('gestionaotApp')
        .controller('UserManagementDialogController',UserManagementDialogController);

    UserManagementDialogController.$inject = ['$stateParams', 'entity', 'User', 'JhiLanguageService',
                                              'Role', '$scope', 'previousState', '$state'];

    function UserManagementDialogController ($stateParams, entity, User, JhiLanguageService,
    		Role, $scope, previousState, $state) {
        var vm = this;

        vm.authorities = ['ROLE_ADMIN','CREER_AOT','LISTER_AOT','MODIFIER_AOT','SUPPRIMER_AOT','VALIDER_AOT','CONSULTER_RELEVER','CREER_FACTURE','LISTER_FACTURE','MODIFIER_FACTURE','SUPPRIMER_FACTURE',
            'MODIFIER_ECHEANCE_FACTURE','CREER_REGLEMENT','LISTER_REGLEMENT','MODIFIER_REGLEMENT','SUPPRIMER_REGLEMENT','CREER_POINT','LISTER_POINT','MODIFIER_POINT',
            'SUPPRIMER_POINT','CREER_TASK','LISTER_TASK','MODIFIER_TASK','SUPPRIMER_TASK','CREER_ECHEANCIER','LISTER_ECHEANCIER','MODIFIER_ECHEANCIER','SUPPRIMER_ECHEANCIER',
            'CREER_ECHEANCE','LISTER_ECHEANCE','MODIFIER_ECHEANCE','SUPPRIMER_ECHEANCE','CONSULTER_BALANCE_AGEE','CREER_USER','LISTER_USER','MODIFIER_USER',
            'SUPPRIMER_USER','CREER_ROLE','LISTER_ROLE','MODIFIER_ROLE','SUPPRIMER_ROLE','GERER_AOT','GERER_FACTURE','GERER_REGLEMENT','GERER_POINT',
            'GERER_TASK','GERER_ECHEANCIER','GERER_ECHEANCE','GERER_BALANCEG','GERER_USERS','GERER_ROLE','ROLE_SYSTEM','ROLE_USER'];

        vm.clear = clear;
        vm.languages = null;
        vm.save = save;
        vm.user = entity;
        vm.roles = Role.query();


        JhiLanguageService.getAll().then(function (languages) {
            vm.languages = languages;
        });

        function clear () {
            $state.go(previousState.name, previousState.params);
            //$uibModalInstance.dismiss('cancel');
        }

        function onSaveSuccess (result) {
            $scope.$emit('gestaotApp:userUpdate', result);
            $state.go('user-management-detail', {login: result.login});
            vm.isSaving = false;
            /*vm.isSaving = false;
            $uibModalInstance.close(result);*/
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        function save () {
            vm.isSaving = true;
            if (vm.user.id !== null) {
                User.update(vm.user, onSaveSuccess, onSaveError);
            } else {
                User.save(vm.user, onSaveSuccess, onSaveError);
            }
        }

        // Functions

        vm.roleChanged = function(role) {
            console.log(role);
        }
    }
})();
