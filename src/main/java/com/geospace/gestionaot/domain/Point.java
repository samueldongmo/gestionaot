package com.geospace.gestionaot.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * The Point entity.
 * 
 * Un Point est une Borne en language de Geometre.
 * @author bwa
 */
@ApiModel(description = "The Point entity. @author bwa")
@Entity
@Table(name = "point")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Point implements Serializable, Comparable<Point> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @ApiModelProperty(value = "Numero Borne")
    @Column(name = "num_borne", nullable = false)
    private Integer numBorne;

    /**
     * lattitude
     */
    @ApiModelProperty(value = "lattitude")
    @Column(name = "lat", nullable = false)
    private Double lat;

    /**
     * Longitude
     */
    @ApiModelProperty(value = "Longitude")
    @Column(name = "lng", nullable = false)
    private Double lng;

    @ManyToOne
    private Amodiataire amodiataire;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public Integer getNumBorne() {
		return numBorne;
	}
    
    public Point numBorne (Integer numBorne) {
    	this.numBorne = numBorne;
    	return this;
    }

	public void setNumBorne(Integer numBorne) {
		this.numBorne = numBorne;
	}

	public Double getLat() {
        return lat;
    }

    public Point lat(Double lat) {
        this.lat = lat;
        return this;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public Point lng(Double lng) {
        this.lng = lng;
        return this;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Amodiataire getAmodiataire() {
        return amodiataire;
    }

    public Point amodiataire(Amodiataire amodiataire) {
        this.amodiataire = amodiataire;
        return this;
    }

    public void setAmodiataire(Amodiataire amodiataire) {
        this.amodiataire = amodiataire;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Point point = (Point) o;
        if (point.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, point.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Point{" +
            "id=" + id +
            ", numBorne='" + numBorne + "'" +
            ", lat='" + lat + "'" +
            ", lng='" + lng + "'" +
            '}';
    }

	@Override
	public int compareTo(Point o) {
		if(o == null) return -1;
		return this.numBorne.compareTo(o.numBorne);
	}
}
