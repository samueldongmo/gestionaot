package com.geospace.gestionaot.domain;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Amodiataire entity.
 * @author samuel
 */
@ApiModel(description = "The Amodiataire entity. @author samuel")
@Entity
@Table(name = "amodiataire")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Amodiataire extends BaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * numAutorisation
	 */
	@ApiModelProperty(value = "numAutorisation")
	@Column(name = "numero_autorisation")
	private String numeroAutorisation;

	@Column(name = "nom_societe")
	private String nomSociete;

	@Column(name = "bp")
	private String bp;

	@Column(name = "ville")
	private String ville;

	@Column(name = "numero_telephone")
	private String numeroTelephone;

	//modification nui (numéro d'indification unique) en numero de contribuable
	@Column(name = "num_contribuable")
	private String numContribuable;

	@Column(name = "rccm")
	private String rccm;

	@Column(name = "superficie")
	private Double superficie;

	@Column(name = "duree")
	private Integer duree;

	@Column(name = "tarif_hors_taxe", precision=10, scale=2)
	private BigDecimal tarifHorsTaxe;

	@Column(name = "montant_trimestriel_ht", precision=10, scale=2)
	private BigDecimal montantTrimestrielHT;

	@Column(name = "montant_trimestriel_ttc", precision=10, scale=2)
	private BigDecimal montantTrimestrielTTC;

	@Column(name = "montant_caution", precision=10, scale=2)
	private BigDecimal montantCaution;

	@Column(name = "date_echeance")
	private LocalDate dateEcheance;

	@Column(name="maitrise_caution")
	private String maitriseCaution;

	@Column(name="date_caution")
	private LocalDate dateCaution;

	@Column(name="nom_signataire_aot")
	private String nomSignataireAOT;

	@Column(name="date_signature")
	private LocalDate dateSignature;

	@Column(name="date_expiration")
	private LocalDate dateExpiration;

	@Enumerated(EnumType.STRING)
	@Column(name="statut")
	private StatutAOT statut;

	@Column(name = "date_souscription")
	private LocalDate dateSouscription;

	/*
	 * New fields
	 */
	@Column(name = "representant_societe")
	private String representantsociete;

	@Column(name = "directeur_operation")
	private String directeurOperation;

	@Column(name = "date_enregistrement_aot")
	private LocalDate dateEnregistrementAot;

	//Existence d'un contrat d'assurence OUI ou NON - DA
	@Column(name = "exist_ca")
	private String existCA;

	@Column(name = "debut_ca")
	private LocalDate debutCA;

	@Column(name = "fin_ca")
	private LocalDate finCA;
	
	//Existence d'un dossier Fiscal OUI ou NON - DA
		@Column(name = "exist_df")
		private String existDF;

		@Column(name = "debut_df")
		private LocalDate debutDF;

		@Column(name = "fin_df")
		private LocalDate finDF;

	public String getNumeroAutorisation() {
		return numeroAutorisation;
	}

	public Amodiataire numeroAutorisation(String numeroAutorisation) {
		this.numeroAutorisation = numeroAutorisation;
		return this;
	}

	public void setNumeroAutorisation(String numeroAutorisation) {
		this.numeroAutorisation = numeroAutorisation;
	}

	public String getNomSociete() {
		return nomSociete;
	}

	public Amodiataire nomSociete(String nomSociete) {
		this.nomSociete = nomSociete;
		return this;
	}

	public void setNomSociete(String nomSociete) {
		this.nomSociete = nomSociete;
	}

	public String getBp() {
		return bp;
	}

	public Amodiataire bp(String bp) {
		this.bp = bp;
		return this;
	}

	public void setBp(String bp) {
		this.bp = bp;
	}

	public String getVille() {
		return ville;
	}

	public Amodiataire ville(String ville) {
		this.ville = ville;
		return this;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getNumeroTelephone() {
		return numeroTelephone;
	}

	public Amodiataire numeroTelephone(String numeroTelephone) {
		this.numeroTelephone = numeroTelephone;
		return this;
	}

	public void setNumeroTelephone(String numeroTelephone) {
		this.numeroTelephone = numeroTelephone;
	}

	public String getNumContribuable() {
		return numContribuable;
	}

	public Amodiataire numContribuable(String numContribuable) {
		this.numContribuable = numContribuable;
		return this;
	}

	public void setNumContribuable(String numContribuable) {
		this.numContribuable = numContribuable;
	}

	public String getRepresentantsociete() {
		return representantsociete;
	}

	public Amodiataire representantsociete(String representantsociete) {
		this.representantsociete = representantsociete;
		return this;
	}

	public void setRepresentantsociete(String representantsociete) {
		this.representantsociete = representantsociete;
	}

	public String getDirecteurOperation() {
		return directeurOperation;
	}

	public Amodiataire directeurOperation(String directeurOperation) {
		this.directeurOperation = directeurOperation;
		return this;
	}

	public void setDirecteurOperation(String directeurOperation) {
		this.directeurOperation = directeurOperation;
	}

	public LocalDate getDateEnregistrementAot() {
		return dateEnregistrementAot;
	}

	public Amodiataire dateEnregistrementAot(LocalDate dateEnregistrementAot) {
		this.dateEnregistrementAot = dateEnregistrementAot;
		return this;
	}

	public void setDateEnregistrementAot(LocalDate dateEnregistrementAot) {
		this.dateEnregistrementAot = dateEnregistrementAot;
	}

	public String getExistCA() {
		return existCA;
	}

	public Amodiataire existCA(String existCA) {
		this.directeurOperation = existCA;
		return this;
	}

	public void setExistCA(String existCA) {
		this.existCA = existCA;
	}

	public LocalDate getDebutCA() {
		return debutCA;
	}

	public void debutCA(LocalDate debutCA) {
		this.debutCA = debutCA;
	}

	public void setDebutCA(LocalDate debutCA) {
		this.debutCA = debutCA;
	}

	public LocalDate getFinCA() {
		return finCA;
	}

	public void finCA(LocalDate finCA) {
		this.finCA = finCA;
	}

	public void setFinCA(LocalDate finCA) {
		this.finCA = finCA;
	}

	public String getRccm() {
		return rccm;
	}

	public Amodiataire rccm(String rccm) {
		this.rccm = rccm;
		return this;
	}

	public void setRccm(String rccm) {
		this.rccm = rccm;
	}

	public Double getSuperficie() {
		return superficie;
	}

	public Amodiataire superficie(Double superficie) {
		this.superficie = superficie;
		return this;
	}

	public void setSuperficie(Double superficie) {
		this.superficie = superficie;
	}

	public Integer getDuree() {
		return duree;
	}

	public Amodiataire duree(Integer duree) {
		this.duree = duree;
		return this;
	}

	public void setDuree(Integer duree) {
		this.duree = duree;
	}

	public BigDecimal getTarifHorsTaxe() {
		return tarifHorsTaxe;
	}

	public Amodiataire tarifHorsTaxe(BigDecimal tarifHorsTaxe) {
		this.tarifHorsTaxe = tarifHorsTaxe;
		return this;
	}

	public void setTarifHorsTaxe(BigDecimal tarifHorsTaxe) {
		this.tarifHorsTaxe = tarifHorsTaxe;
	}

	public BigDecimal getMontantTrimestrielHT() {
		return montantTrimestrielHT;
	}

	public Amodiataire montantTrimestrielHT(BigDecimal montantTrimestrielHT) {
		this.montantTrimestrielHT = montantTrimestrielHT;
		return this;
	}

	public void setMontantTrimestrielHT(BigDecimal montantTrimestrielHT) {
		this.montantTrimestrielHT = montantTrimestrielHT;
	}

	public BigDecimal getMontantTrimestrielTTC() {
		return montantTrimestrielTTC;
	}

	public Amodiataire montantTrimestrielTTC(BigDecimal montantTrimestrielTTC) {
		this.montantTrimestrielTTC = montantTrimestrielTTC;
		return this;
	}

	public void setMontantTrimestrielTTC(BigDecimal montantTrimestrielTTC) {
		this.montantTrimestrielTTC = montantTrimestrielTTC;
	}

	public BigDecimal getMontantCaution() {
		return montantCaution;
	}

	public Amodiataire montantCaution(BigDecimal montantCaution) {
		this.montantCaution = montantCaution;
		return this;
	}

	public void setMontantCaution(BigDecimal montantCaution) {
		this.montantCaution = montantCaution;
	}

	public LocalDate getDateEcheance() {
		return dateEcheance;
	}

	public void setDateEcheance(LocalDate dateEcheance) {
		this.dateEcheance = dateEcheance;
	}

	public String getMaitriseCaution() {
		return maitriseCaution;
	}

	public void setMaitriseCaution(String maitriseCaution) {
		this.maitriseCaution = maitriseCaution;
	}

	public LocalDate getDateCaution() {
		return dateCaution;
	}

	public void setDateCaution(LocalDate dateCaution) {
		this.dateCaution = dateCaution;
	}

	public String getNomSignataireAOT() {
		return nomSignataireAOT;
	}

	public void setNomSignataireAOT(String nomSignataireAOT) {
		this.nomSignataireAOT = nomSignataireAOT;
	}

	public LocalDate getDateSignature() {
		return dateSignature;
	}

	public void setDateSignature(LocalDate dateSignature) {
		this.dateSignature = dateSignature;
	}

	public LocalDate getDateExpiration() {
		return dateExpiration;
	}

	public void setDateExpiration(LocalDate dateExpiration) {
		this.dateExpiration = dateExpiration;
	}

	public StatutAOT getStatut() {
		return statut;
	}

	public void setStatut(StatutAOT statut) {
		this.statut = statut;
	}


	public LocalDate getDateSouscription() {
		return dateSouscription;
	}

	public Amodiataire dateSouscription(LocalDate dateSouscription) {
		this.dateSouscription = dateSouscription;
		return this;
	}

	public void setDateSouscription(LocalDate dateSouscription) {
		this.dateSouscription = dateSouscription;
	}
	
	

	public String getExistDF() {
		return existDF;
	}
	
	public Amodiataire existDF(String existDF) {
		this.existDF = existDF;
		return this;
	}

	public void setExistDF(String existDF) {
		this.existDF = existDF;
	}

	public LocalDate getDebutDF() {
		return debutDF;
	}
	
	public Amodiataire debutDF(LocalDate debutDF) {
		this.debutDF = debutDF;
		return this;
	}

	public void setDebutDF(LocalDate debutDF) {
		this.debutDF = debutDF;
	}

	public LocalDate getFinDF() {
		return finDF;
	}
	
	public Amodiataire finDF(LocalDate finDF) {
		this.finDF = finDF;
		return this;
	}

	public void setFinDF(LocalDate finDF) {
		this.finDF = finDF;
	}

	@Override
	public String toString() {
		return "Amodiataire [id= "+ getId()+ "numeroAutorisation=" + numeroAutorisation + ", nomSociete=" + nomSociete + ", bp=" + bp
				+ ", ville=" + ville + ", numeroTelephone=" + numeroTelephone + ", numContribuable=" + numContribuable
				+ ", rccm=" + rccm + ", superficie=" + superficie + ", duree=" + duree + ", tarifHorsTaxe="
				+ tarifHorsTaxe + ", montantTrimestrielHT=" + montantTrimestrielHT + ", montantTrimestrielTTC="
				+ montantTrimestrielTTC + ", montantCaution=" + montantCaution + ", dateEcheance=" + dateEcheance
				+ ", maitriseCaution=" + maitriseCaution + ", dateCaution=" + dateCaution + ", nomSignataireAOT="
				+ nomSignataireAOT + ", dateSignature=" + dateSignature + ", dateExpiration=" + dateExpiration
				+ ", statut=" + statut + ", dateSouscription=" + dateSouscription + ", representantsociete="
				+ representantsociete + ", directeurOperation=" + directeurOperation + ", dateEnregistrementAot="
				+ dateEnregistrementAot + ", existCA=" + existCA + ", debutCA=" + debutCA + ", finCA=" + finCA + "]";
	}
}
