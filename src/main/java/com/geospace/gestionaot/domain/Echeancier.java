package com.geospace.gestionaot.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Echeancier.
 */
@Entity
@Table(name = "echeancier")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Echeancier extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "id_amodiataire", nullable = false)
    private Long idAmodiataire;

    @Column(name = "ref")
    private String ref;

    @Column(name = "montant_ht", precision=10, scale=2)
    private BigDecimal montantHT;

    @Column(name = "montant", precision=10, scale=2)
    private BigDecimal montant;

    @Column(name = "date_debut")
    private LocalDate dateDebut;

    @Column(name = "date_fin")
    private LocalDate dateFin;

    private transient String nomSociete;

    public Long getIdAmodiataire() {
        return idAmodiataire;
    }

    public Echeancier idAmodiataire(Long idAmodiataire) {
        this.idAmodiataire = idAmodiataire;
        return this;
    }

    public void setIdAmodiataire(Long idAmodiataire) {
        this.idAmodiataire = idAmodiataire;
    }

    public String getRef() {
        return ref;
    }

    public Echeancier ref(String ref) {
        this.ref = ref;
        return this;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public BigDecimal getMontantHT() {
        return montantHT;
    }

    public Echeancier montantHT(BigDecimal montantHT) {
        this.montantHT = montantHT;
        return this;
    }

    public void setMontantHT(BigDecimal montantHT) {
        this.montantHT = montantHT;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public Echeancier montant(BigDecimal montant) {
        this.montant = montant;
        return this;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public Echeancier dateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
        return this;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public Echeancier dateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
        return this;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public String getNomSociete() {
        return nomSociete;
    }

    public Echeancier nomSociete(String nomSociete) {
        this.nomSociete = nomSociete;
        return this;
    }

    public void setNomSociete(String nomSociete) {
        this.nomSociete = nomSociete;
    }

    @Override
    public String toString() {
        return "Echeancier{" +
            "id=" + getId() +
            ", idAmodiataire='" + idAmodiataire + "'" +
            ", ref='" + ref + "'" +
            ", montantHT='" + montantHT + "'" +
            ", montant='" + montant + "'" +
            ", dateDebut='" + dateDebut + "'" +
            ", dateFin='" + dateFin + "'" +
            '}';
    }
}
