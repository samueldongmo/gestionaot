package com.geospace.gestionaot.domain.enumeration;

/**
 * The TaskAction enumeration.
 */
public enum TaskAction {
    CREATE,EDIT,DELETE,VALIDATE,REDO,UNDO
}
