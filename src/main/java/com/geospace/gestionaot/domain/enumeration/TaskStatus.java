package com.geospace.gestionaot.domain.enumeration;

/**
 * The TaskAction enumeration.
 */
public enum TaskStatus {
    NEW,SEEN,EXECUTED,CANCELED
}
