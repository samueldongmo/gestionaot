package com.geospace.gestionaot.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Reglement.
 */
@Entity
@Table(name = "reglement_facture")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ReglementFacture extends BaseEntity{

    @NotNull
    @Column(name = "ref", nullable = false, unique=true)
    private String ref;
    
    @NotNull
    @Min(value = 0)
    @Column(name = "montant", nullable = false)
    private Double montant;
    
    @NotNull
    @Column(name = "date_reglement", nullable = false)
    private LocalDate dateReglement;
    
    @ManyToOne
    @JoinColumn(name = "facture_id")
    private Facture facture;
    
    @ManyToOne
    @JoinColumn(name = "reglement_id")
    private Reglement reglement;
    
    public void setRef(String ref) {
        this.ref = ref;
    }

    public Double getMontant() {
        return montant;
    }
    
    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public LocalDate getDateReglement() {
        return dateReglement;
    }
    
    public void setDateReglement(LocalDate dateReglement) {
        this.dateReglement = dateReglement;
    }

    public Facture getFacture() {
        return facture;
    }

    public void setFacture(Facture facture) {
        this.facture = facture;
    }

    public Reglement getReglement() {
		return reglement;
	}

	public void setReglement(Reglement reglement) {
		this.reglement = reglement;
	}

    @Override
	public String toString() {
		return "ReglementFacture [id=" + getId() + ", ref=" + ref + ", montant="
				+ montant + ", dateReglement=" + dateReglement + ", facture="
				+ facture + ", reglement=" + reglement + "]";
	}
}
