package com.geospace.gestionaot.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Facture.
 */
@Entity
@Table(name = "facture")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Facture extends BaseEntity {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotNull
    @Column(name = "ref", nullable = false, unique=true)
    private String ref;
    
    @NotNull
    @Column(name = "libelle", nullable = false)
    private String libelle;
    
    @NotNull
    @Column(name = "date_facture", nullable = false)
    private LocalDate dateFacture;
    
    @NotNull
    @Column(name = "date_echeance", nullable = false)
    private LocalDate dateEcheance;
    
    @NotNull
    @Min(value = 0)
    @Column(name = "montant", nullable = false)
    private Double montant;
    
    @ManyToOne
    @JoinColumn(name = "amodiataire_id")
    private Amodiataire amodiataire;
    
    @Column(name = "date_reglement", nullable = true)
    private LocalDate dateReglement;
    

    @NotNull
    @Min(value = 0)
    @Column(name = "montant_ht", nullable = false)
    private Double montantHorsTaxe;
    
    @NotNull
    @Min(value = 0)
    @Column(name = "montant_regle", nullable = false)
    private Double montantRegle;
    
    @Column(name = "est_regle", nullable = false)
    private boolean estRegle = false;
    
    @Column(name = "date_debut", nullable = true)
    private LocalDate dateDebut;

    @Column(name = "date_fin", nullable = true)
    private LocalDate dateFin;

    @Column(name = "id_echeance", nullable = false)
    private Long idEcheance;
    
    
    public String getRef() {
        return ref;
    }
    
    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getLibelle() {
        return libelle;
    }
    
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public LocalDate getDateFacture() {
        return dateFacture;
    }
    
    public void setDateFacture(LocalDate dateFacture) {
        this.dateFacture = dateFacture;
    }

    public LocalDate getDateEcheance() {
        return dateEcheance;
    }
    
    public void setDateEcheance(LocalDate dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public Double getMontant() {
        return montant;
    }
    
    public void setMontant(Double montant) {
        this.montant = montant;
    }
    
	public Amodiataire getAmodiataire() {
		return amodiataire;
	}

	public void setAmodiataire(Amodiataire amodiataire) {
		this.amodiataire = amodiataire;
	}

	public LocalDate getDateReglement() {
		return dateReglement;
	}

	public void setDateReglement(LocalDate dateReglement) {
		this.dateReglement = dateReglement;
	}

	public Double getMontantHorsTaxe() {
		return montantHorsTaxe;
	}

	public void setMontantHorsTaxe(Double montantHorsTaxe) {
		this.montantHorsTaxe = montantHorsTaxe;
	}

	public Double getMontantRegle() {
		return montantRegle;
	}

	public void setMontantRegle(Double montantRegle) {
		this.montantRegle = montantRegle;
	}

	public boolean isEstRegle() {
		return estRegle;
	}

	public void setEstRegle(boolean estRegle) {
		this.estRegle = estRegle;
	}

	public Double getMontantRestant(){
		return montant - montantRegle; 
	}

    public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}
	
	public Long getIdEcheance() {
		return idEcheance;
	}
	
	public Facture idEcheance(Long idEcheance) {
		this.idEcheance = idEcheance;
		return this;
	}
	
	public void setIdEcheance(Long idEcheance) {
		this.idEcheance = idEcheance;
	}
	
	public boolean estFacturee() {
		return this.idEcheance != null;
	}
	@Override
	public String toString() {
		return "Facture [ref=" + ref + ", libelle=" + libelle + ", dateFacture=" + dateFacture + ", dateEcheance="
				+ dateEcheance + ", montant=" + montant + ", amodiataire=" + amodiataire + ", dateReglement="
				+ dateReglement + ", montantHorsTaxe=" + montantHorsTaxe + ", montantRegle=" + montantRegle
				+ ", estRegle=" + estRegle + ", dateDebut=" + dateDebut + ", dateFin=" + dateFin + ", idEcheance="
				+ idEcheance + "]";
	}
}
