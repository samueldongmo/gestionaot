package com.geospace.gestionaot.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Reglement.
 */
@Entity
@Table(name = "reglement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Reglement extends BaseEntity {
    
    @NotNull
    @Column(name = "ref", nullable = false, unique=true)
    private String ref;
    
    @NotNull
    @Column(name = "libelle")
    private String libelle;     
    
    @NotNull
    @Min(value = 1)
    @Column(name = "montant", nullable = false)
    private Double montant;
    
    @NotNull
    @Column(name = "date_reglement", nullable = false)
    private LocalDate dateReglement;
    
    @Enumerated(EnumType.STRING)
    @Column(name="mode_reglement", nullable=false)
    private ModeReglement modeReglement;
    
    @Column(name="info_reglement")
    private String infoReglement;
    
    @ManyToOne
    @JoinColumn(name = "amodiataire_id")
    private Amodiataire amodiataire;
    
    
    public String getRef() {
        return ref;
    }
    
    public void setRef(String ref) {
        this.ref = ref;
    }

    public Double getMontant() {
        return montant;
    }
    
    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public LocalDate getDateReglement() {
        return dateReglement;
    }
    
	public void setDateReglement(LocalDate dateReglement) {
        this.dateReglement = dateReglement;
    }

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Amodiataire getAmodiataire() {
		return amodiataire;
	}

	public void setAmodiataire(Amodiataire amodiataire) {
		this.amodiataire = amodiataire;
	}

    public ModeReglement getModeReglement() {
		return modeReglement;
	}

	public void setModeReglement(ModeReglement modeReglement) {
		this.modeReglement = modeReglement;
	}
	
	public String getInfoReglement() {
		return infoReglement;
	}

	public void setInfoReglement(String infoReglement) {
		this.infoReglement = infoReglement;
	}

	@Override
    public String toString() {
        return "Reglement{" +
            "id=" + getId() +
            ", ref='" + ref + "'" +
            ", montant='" + montant + "'" +
            ", dateReglement='" + dateReglement + "'" +
            ", modeReglement='" + modeReglement + "'" +
            ", infoReglement='" + infoReglement + "'" +
            '}';
    }
}
