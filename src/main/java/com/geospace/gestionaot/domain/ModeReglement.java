package com.geospace.gestionaot.domain;

public enum ModeReglement {
	CHEQUE,
	ESPECE,
	TRAITE,
	AUTRE;
}
