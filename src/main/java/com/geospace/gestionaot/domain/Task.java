package com.geospace.gestionaot.domain;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.geospace.gestionaot.domain.enumeration.TaskAction;
import com.geospace.gestionaot.domain.enumeration.TaskStatus;

import io.swagger.annotations.ApiModelProperty;

/**
 * A Task.
 */
@Entity
@Table(name = "task")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Task extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "action", nullable = false)
    private TaskAction action;

    @NotNull
    @Column(name = "entity_id", nullable = false)
    private Long entityId;

    /**
     * Entityname will act as an entity name
     */
    @NotNull
    @ApiModelProperty(value = "Entityname will act as an entity name", required = true)
    @Column(name = "entity_name", nullable = false)
    private String entityName;

    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private TaskStatus status;

    @Column(name = "last_status_date")
    private ZonedDateTime lastStatusDate;

    @ManyToOne
    private User creator;

    @ManyToOne
    private User assignee;


    public TaskAction getAction() {
        return action;
    }

    public Task action(TaskAction action) {
        this.action = action;
        return this;
    }

    public void setAction(TaskAction action) {
        this.action = action;
    }

    public Long getEntityId() {
        return entityId;
    }

    public Task entityId(Long entityId) {
        this.entityId = entityId;
        return this;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getEntityName() {
        return entityName;
    }

    public Task entityName(String entityName) {
        this.entityName = entityName;
        return this;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getDescription() {
        return description;
    }

    public Task description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public Task status(TaskStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public ZonedDateTime getLastStatusDate() {
        return lastStatusDate;
    }

    public Task lastStatusDate(ZonedDateTime lastStatusDate) {
        this.lastStatusDate = lastStatusDate;
        return this;
    }

    public void setLastStatusDate(ZonedDateTime lastStatusDate) {
        this.lastStatusDate = lastStatusDate;
    }

    public User getCreator() {
        return creator;
    }

    public Task creator(User user) {
        this.creator = user;
        return this;
    }

    public void setCreator(User user) {
        this.creator = user;
    }

    public User getAssignee() {
        return assignee;
    }

    public Task assignee(User user) {
        this.assignee = user;
        return this;
    }

    public void setAssignee(User user) {
        this.assignee = user;
    }

    @Override
    public String toString() {
        return "Task{" +
            "id=" + getId() +
            ", action='" + action + "'" +
            ", entityId='" + entityId + "'" +
            ", entityName='" + entityName + "'" +
            ", description='" + description + "'" +
            ", status='" + status + "'" +
            ", lastStatusDate='" + lastStatusDate + "'" +
            '}';
    }
}
