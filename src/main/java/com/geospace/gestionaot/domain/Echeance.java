package com.geospace.gestionaot.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Echeance.
 */
@Entity
@Table(name = "echeance")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Echeance extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "id_echeancier", nullable = false)
    private Long idEcheancier;

    @NotNull
    @Column(name = "id_amodiataire", nullable = false)
    private Long idAmodiataire;

    @Column(name = "id_facture")
    private Long idFacture;

    @NotNull
    @Column(name = "ref", nullable = false)
    private String ref;

    @NotNull
    @Column(name = "montan_ht", precision=10, scale=2, nullable = false)
    private BigDecimal montanHT;

    @NotNull
    @Column(name = "montant", precision=10, scale=2, nullable = false)
    private BigDecimal montant;

    @NotNull
    @Column(name = "date_debut", nullable = false)
    private LocalDate dateDebut;

    @NotNull
    @Column(name = "date_fin", nullable = false)
    private LocalDate dateFin;

    @Column(name = "id_reglement")
    private Long idReglement;

    @Column(name = "est_facture")
    private Boolean estFacture = Boolean.FALSE;

    @Column(name = "est_regle")
    private Boolean estRegle = Boolean.FALSE;

    private transient String nomSociete;
    private transient String refEcheancier;
    private transient String refReglement;
    private transient String refFacture;

    public Long getIdEcheancier() {
        return idEcheancier;
    }

    public Echeance idEcheancier(Long idEcheancier) {
        this.idEcheancier = idEcheancier;
        return this;
    }

    public void setIdEcheancier(Long idEcheancier) {
        this.idEcheancier = idEcheancier;
    }

    public Long getIdAmodiataire() {
        return idAmodiataire;
    }

    public Echeance idAmodiataire(Long idAmodiataire) {
        this.idAmodiataire = idAmodiataire;
        return this;
    }

    public void setIdAmodiataire(Long idAmodiataire) {
        this.idAmodiataire = idAmodiataire;
    }

    public Long getIdFacture() {
        return idFacture;
    }

    public Echeance idFacture(Long idFacture) {
        this.idFacture = idFacture;
        return this;
    }

    public void setIdFacture(Long idFacture) {
        this.idFacture = idFacture;
    }

    public String getRef() {
        return ref;
    }

    public Echeance ref(String ref) {
        this.ref = ref;
        return this;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public BigDecimal getMontanHT() {
        return montanHT;
    }

    public Echeance montanHT(BigDecimal montanHT) {
        this.montanHT = montanHT;
        return this;
    }

    public void setMontanHT(BigDecimal montanHT) {
        this.montanHT = montanHT;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public Echeance montant(BigDecimal montant) {
        this.montant = montant;
        return this;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public Echeance dateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
        return this;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public Echeance dateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
        return this;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public Long getIdReglement() {
        return idReglement;
    }

    public Echeance idReglement(Long idReglement) {
        this.idReglement = idReglement;
        return this;
    }

    public void setIdReglement(Long idReglement) {
        this.idReglement = idReglement;
    }

    public Boolean isEstFacture() {
        return estFacture;
    }

    public Echeance estFacture(Boolean estFacture) {
        this.estFacture = estFacture;
        return this;
    }

    public void setEstFacture(Boolean estFacture) {
        this.estFacture = estFacture;
    }

    public Boolean isEstRegle() {
        return estRegle;
    }

    public Echeance estRegle(Boolean estRegle) {
        this.estRegle = estRegle;
        return this;
    }

    public void setEstRegle(Boolean estRegle) {
        this.estRegle = estRegle;
    }

    public String getNomSociete() {
        return nomSociete;
    }

    public Echeance nomSociete(String nomSociete) {
        this.nomSociete = nomSociete;
        return this;
    }

    public void setNomSociete(String nomSociete) {
        this.nomSociete = nomSociete;
    }

    public String getRefEcheancier() {
        return this.refEcheancier;
    }

    public Echeance refEcheancier(String refEcheancier) {
        this.refEcheancier = refEcheancier;
        return this;
    }

    public void setRefEcheancier(String refEcheancier) {
        this.refEcheancier = refEcheancier;
    }


    public String getRefReglement() {
        return this.refReglement;
    }

    public Echeance refReglement(String refReglement) {
        this.refReglement = refReglement;
        return this;
    }

    public void setRefReglement(String refReglement) {
        this.refReglement = refReglement;
    }

    public String getRefFacture() {
        return this.refFacture;
    }

    public Echeance refFacture(String refFacture) {
        this.refFacture = refFacture;
        return this;
    }

    public void setRefFacture(String refFacture) {
        this.refFacture = refFacture;
    }

    @Override
    public String toString() {
        return "Echeance{" +
            "id=" + getId() +
            ", idEcheancier='" + idEcheancier + "'" +
            ", idAmodiataire='" + idAmodiataire + "'" +
            ", idFacture='" + idFacture + "'" +
            ", ref='" + ref + "'" +
            ", montanHT='" + montanHT + "'" +
            ", montant='" + montant + "'" +
            ", dateDebut='" + dateDebut + "'" +
            ", dateFin='" + dateFin + "'" +
            ", idReglement='" + idReglement + "'" +
            ", estFacture='" + estFacture + "'" +
            ", estRegle='" + estRegle + "'" +
            '}';
    }
}
