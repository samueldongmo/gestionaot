package com.geospace.gestionaot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.geospace.gestionaot.domain.ReglementFacture;

public interface ReglementFactureRepository extends JpaRepository<ReglementFacture, String>{

	@Query("SELECT a FROM ReglementFacture a WHERE (a.reglement.id = ?1)")
	List<ReglementFacture> findReglementFactureByReglement(Long reglementId);

	@Query("SELECT a FROM ReglementFacture a WHERE (a.facture.id = ?1)")
	List<ReglementFacture> findReglementFactureByFacture(Long factureId);
}
