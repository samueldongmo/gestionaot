package com.geospace.gestionaot.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import com.geospace.gestionaot.domain.Amodiataire;
import com.geospace.gestionaot.domain.StatutAOT;

import java.util.List;

/**
 * Spring Data JPA repository for the Amodiataire entity.
 */
@SuppressWarnings("unused")
public interface AmodiataireRepository extends JpaRepository<Amodiataire,Long> {

	Page<Amodiataire> findByStatut(StatutAOT statut, Pageable pageable);

	Page<Amodiataire> findByCreatedByAndStatut(String currentUserLogin, StatutAOT enAttente, Pageable pageable);

    @Query("SELECT a FROM Amodiataire AS a WHERE LOWER(a.nomSociete) LIKE LOWER(?1) AND a.statut = ?2")
    List<Amodiataire> filterAmodiataires(String query, StatutAOT statut);
}
