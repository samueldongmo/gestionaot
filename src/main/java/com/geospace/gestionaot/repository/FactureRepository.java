package com.geospace.gestionaot.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.geospace.gestionaot.domain.Facture;

/**
 * Spring Data JPA repository for the Facture entity.
 */
public interface FactureRepository extends JpaRepository<Facture,Long> {
	
	@Query("SELECT a FROM Facture a WHERE (a.dateReglement > ?1) or (a.dateReglement is null) or (a.montant<>a.montantRegle)")
	List<Facture> getFactureImpaye(LocalDate fromDate);
	
	@Query("SELECT a FROM Facture a WHERE (a.amodiataire.id = ?1)")
	List<Facture> getFactureOfClient(Long idClient);

	@Query("SELECT a FROM Facture a WHERE (a.amodiataire.id = ?1) and ((a.dateReglement is null) or (a.montant<>a.montantRegle))")
	List<Facture> getFactureImpayeOfClient(Long idAmodiataire);
}
