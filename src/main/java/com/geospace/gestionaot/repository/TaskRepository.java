package com.geospace.gestionaot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.geospace.gestionaot.domain.StatutAOT;
import com.geospace.gestionaot.domain.Task;
import com.geospace.gestionaot.domain.User;
import com.geospace.gestionaot.domain.enumeration.TaskStatus;

/**
 * Spring Data JPA repository for the Task entity.
 */
@SuppressWarnings("unused")
public interface TaskRepository extends JpaRepository<Task,Long> {

    @Query("select task from Task task where task.creator.login = ?#{principal.username}")
    List<Task> findByCreatorIsCurrentUser();

    @Query("select task from Task task where task.assignee.login = ?#{principal.username}")
    List<Task> findByAssigneeIsCurrentUser();
    
    @Query("select task from Task task where task.assignee = ?1 and task.status = ?2")
    List<Task> findByUserAndStatus(User user, TaskStatus status);


    @Query("select task from Task task where task.assignee.login = ?#{principal.username} and task.status = ?2")
	List<Task> findByAssigneeIsCurrentUserAndStatus(TaskStatus status);
    
    @Query("select task from Task task where task.entityId = ?1 and task.entityName = ?2 and task.status = ?3")
    List<Task> find(Long id, String entityName, TaskStatus status);
}
