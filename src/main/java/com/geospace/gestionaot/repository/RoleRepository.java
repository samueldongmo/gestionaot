package com.geospace.gestionaot.repository;

import org.springframework.data.jpa.repository.*;

import com.geospace.gestionaot.domain.Authority;
import com.geospace.gestionaot.domain.Role;

import java.util.List;

/**
 * Spring Data JPA repository for the Role entity.
 */
@SuppressWarnings("unused")
public interface RoleRepository extends JpaRepository<Role,Long> {
	

    @EntityGraph(attributePaths = "authorities")
	Role findOneWithRolesById(Long id);
    
    @Query(value = "SELECT r FROM Role AS r WHERE ?1 in r.authorities")
    List<Role> findWithAuthority(Authority authority);
}
