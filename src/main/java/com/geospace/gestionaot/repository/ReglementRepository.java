package com.geospace.gestionaot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.geospace.gestionaot.domain.Reglement;

/**
 * Spring Data JPA repository for the Reglement entity.
 */
public interface ReglementRepository extends JpaRepository<Reglement,Long> {
	
	@Query("SELECT r FROM Reglement r WHERE r.amodiataire.id = ?1")
	List<Reglement> findByIdAmodiataire(Long idAmodiataire);

}
