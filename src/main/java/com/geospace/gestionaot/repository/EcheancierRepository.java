package com.geospace.gestionaot.repository;

import org.springframework.data.jpa.repository.*;

import com.geospace.gestionaot.domain.Echeancier;

import java.util.List;

/**
 * Spring Data JPA repository for the Echeancier entity.
 */
@SuppressWarnings("unused")
public interface EcheancierRepository extends JpaRepository<Echeancier,Long> {
	
	List<Echeancier> findByIdAmodiataire (Long idAmodiataire);
}
