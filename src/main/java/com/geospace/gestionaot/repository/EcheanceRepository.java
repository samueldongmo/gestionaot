package com.geospace.gestionaot.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.geospace.gestionaot.domain.Echeance;
import com.geospace.gestionaot.domain.Echeancier;

/**
 * Spring Data JPA repository for the Echeance entity.
 */
@SuppressWarnings("unused")
public interface EcheanceRepository extends JpaRepository<Echeance,Long> {

	@Query("SELECT a FROM Echeance a WHERE a.idAmodiataire = ?1 AND a.estRegle = ?2 ORDER BY a.dateDebut ASC")
	List<Echeance> findEcheanceNonReglee(Long id, Boolean estRegle);

	@Query("SELECT a FROM Echeance a WHERE a.idAmodiataire = ?1 AND a.estFacture = ?2 ORDER BY a.dateDebut ASC")
	List<Echeance> findNonFactureeByAmodiataire(Long idAmodiataire, Boolean false1);
	
	Optional<Echeance> findByIdFacture(Long idFacture);

	Optional<Echeance> findByIdReglement(Long idReglement);
	
	List<Echeance> findByIdAmodiataire(Long idAmodiataire);
	
	List<Echeance> findByIdEcheancier(Long idEcheancier);

	Page<Echeance> findByIdEcheancier(Long id, Pageable pageRequest);

}
