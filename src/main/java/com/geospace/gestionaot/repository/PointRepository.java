package com.geospace.gestionaot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.geospace.gestionaot.domain.Amodiataire;
import com.geospace.gestionaot.domain.Point;

/**
 * Spring Data JPA repository for the Point entity.
 */
@SuppressWarnings("unused")
public interface PointRepository extends JpaRepository<Point,Long> {
	
	@Query("SELECT p FROM Point p WHERE p.amodiataire = ?1 ORDER BY p.numBorne ASC")
    List<Point> findByAmodiataire(Amodiataire amodiataire);
}
