package com.geospace.gestionaot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.geospace.gestionaot.domain.Authority;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
