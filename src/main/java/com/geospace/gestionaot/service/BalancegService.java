package com.geospace.gestionaot.service;

import java.time.LocalDate;
import java.util.List;

import com.geospace.gestionaot.service.dto.BalancegDTO;
import com.geospace.gestionaot.service.dto.BalancegItemDTO;

public interface BalancegService {	
	
	public List<BalancegItemDTO> findByDates(LocalDate fromDate);

	BalancegDTO findBanlancegByDates(LocalDate fromDate);
	
}
