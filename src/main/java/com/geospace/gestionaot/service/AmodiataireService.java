package com.geospace.gestionaot.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Year;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geospace.gestionaot.domain.Amodiataire;
import com.geospace.gestionaot.domain.Authority;
import com.geospace.gestionaot.domain.Echeance;
import com.geospace.gestionaot.domain.Echeancier;
import com.geospace.gestionaot.domain.Point;
import com.geospace.gestionaot.domain.Role;
import com.geospace.gestionaot.domain.StatutAOT;
import com.geospace.gestionaot.domain.Task;
import com.geospace.gestionaot.domain.User;
import com.geospace.gestionaot.domain.enumeration.TaskAction;
import com.geospace.gestionaot.domain.enumeration.TaskStatus;
import com.geospace.gestionaot.repository.AmodiataireRepository;
import com.geospace.gestionaot.repository.AuthorityRepository;
import com.geospace.gestionaot.repository.EcheanceRepository;
import com.geospace.gestionaot.repository.EcheancierRepository;
import com.geospace.gestionaot.repository.FactureRepository;
import com.geospace.gestionaot.repository.PointRepository;
import com.geospace.gestionaot.repository.ReglementRepository;
import com.geospace.gestionaot.repository.RoleRepository;
import com.geospace.gestionaot.repository.TaskRepository;
import com.geospace.gestionaot.repository.UserRepository;
import com.geospace.gestionaot.security.SecurityUtils;
import com.geospace.gestionaot.service.builder.SearchQueryBuilder;
import com.geospace.gestionaot.service.dto.AmodiataireCreateDTO;
import com.geospace.gestionaot.service.dto.AmodiataireDto;
import com.geospace.gestionaot.service.dto.AmodiataireSearchRequest;
import com.geospace.gestionaot.service.dto.StatutEcheance;
import com.geospace.gestionaot.web.rest.errors.GeoServiceException;

/**
 * Service Implementation for managing Amodiataire.
 */
@Service
@Transactional
public class AmodiataireService {

    private final Logger log = LoggerFactory.getLogger(AmodiataireService.class);

    private final AmodiataireRepository amodiataireRepository;
    private final PointRepository pointRepository;
    private final EntityManager em;
    private final TaskRepository taskRepository;
    private final UserService userService;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final AuthorityRepository authorityRepository;
    private final EcheancierRepository echeancierRepository;
    private final EcheanceRepository echeanceRepository;
    private final FactureRepository factureRepository;
    private final ReglementRepository reglementRepository;
    

	public AmodiataireService(AmodiataireRepository amodiataireRepository, PointRepository pointRepository,
			EntityManager em, TaskRepository taskRepository, UserService userService, UserRepository userRepository,
			RoleRepository roleRepository, AuthorityRepository authorityRepository,
			EcheancierRepository echeancierRepository, EcheanceRepository echeanceRepository,
			FactureRepository factureRepository, ReglementRepository reglementRepository) {
		super();
		this.amodiataireRepository = amodiataireRepository;
		this.pointRepository = pointRepository;
		this.em = em;
		this.taskRepository = taskRepository;
		this.userService = userService;
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.authorityRepository = authorityRepository;
		this.echeancierRepository = echeancierRepository;
		this.echeanceRepository = echeanceRepository;
		this.factureRepository = factureRepository;
		this.reglementRepository = reglementRepository;
	}


	/**
     * Save a amodiataire.
     *
     * @param amodiataire the entity to save
     * @return the persisted entity
     */
    public Amodiataire save(AmodiataireCreateDTO amodiataire) {
        log.debug("Request to save Amodiataire : {}", amodiataire);
        Amodiataire result = amodiataireRepository.save(amodiataire);
        return result;
    }


	/**
	 * @param amodiataireDto
	 * @return
	 */
	public Amodiataire saveCustom(AmodiataireCreateDTO amodiataireDto) {
		Amodiataire amodiataire = new Amodiataire();
		BeanUtils.copyProperties(amodiataireDto, amodiataire);

		log.debug("Creation d'un amodiataire : {}", amodiataire);
        amodiataire.setStatut(StatutAOT.EN_ATTENTE);
		amodiataireRepository.save(amodiataire);
		log.debug("Amodiataire cree : {}", amodiataire);

		List<Point> existingPoints = pointRepository.findByAmodiataire(amodiataire);
		// Delete existing points
		pointRepository.delete(existingPoints);
		// Get those comming from the client
		List<Point> pointsToSave = amodiataireDto.getPoints();
		for (Point point : pointsToSave) {
			point.setAmodiataire(amodiataire);
		}

		// Save all
		pointRepository.save(pointsToSave);

		// Create validate tasks
		createValidateTasks(amodiataire);

		return amodiataire;
	}

    /**
     * @param echeancier
     * @param echeances
     * @return
     */
    private Echeancier mettreAjourMontantsEcheancier(Echeancier echeancier, List<Echeance> echeances) {
    	BigDecimal montant = BigDecimal.ZERO;
    	BigDecimal montantHT = BigDecimal.ZERO;

    	for (Echeance echeance : echeances) {
			montant.add(echeance.getMontant() == null ? BigDecimal.ZERO : echeance.getMontant());
			montantHT.add(echeance.getMontanHT() == null ? BigDecimal.ZERO : echeance.getMontanHT());
		}

    	echeancier.montant(montant)
    				.montantHT(montantHT);
    	echeancierRepository.save(echeancier);
		return echeancier;
	}


	private List<Echeance> creerEcheances(Echeancier echeancier, BigDecimal montantHt, BigDecimal montantTTC) {
    	// Date debut de l'echeancier
    	LocalDate dateDebut = echeancier.getDateDebut();
    	// Date de fin de l'echeancier
    	LocalDate dateFin = echeancier.getDateFin();

    	long dureeEnJours = ChronoUnit.DAYS.between(dateDebut, dateFin);
    	long resteJours = dureeEnJours%90;

    	long nombreDeTimestres = (dureeEnJours - resteJours)/90;

    	LocalDate dateDebutEcheance = dateDebut;
    	LocalDate dateFinEcheance = null;

    	// Creer les echeances de 90jours
    	List<Echeance> echeances = new ArrayList<>();
    	for (int i = 1; i <= nombreDeTimestres; i++) {
    		dateFinEcheance = dateDebutEcheance.plusDays(90);
    		Echeance echeance = new Echeance();
    		echeance.dateDebut(dateDebutEcheance)
    			.dateFin(dateFinEcheance)
    			.estFacture(Boolean.FALSE)
    			.estRegle(Boolean.FALSE)
    			.idAmodiataire(echeancier.getIdAmodiataire())
    			.idEcheancier(echeancier.getId())
    			.montanHT(montantHt)
    			.montant(montantTTC)
    			.ref("ECH-"+dateDebutEcheance.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));

    		echeanceRepository.save(echeance);

    		dateDebutEcheance = dateFinEcheance;
    		echeances.add(echeance);
    	}
    	// Ajouter l'echeance des jours restants
    	if(resteJours > 0) {
    		dateFinEcheance = dateDebutEcheance.plusDays(resteJours);
    		Echeance echeance = new Echeance();
    		echeance.dateDebut(dateDebutEcheance)
    			.dateFin(dateFinEcheance)
    			.estFacture(Boolean.FALSE)
    			.estRegle(Boolean.FALSE)
    			.idAmodiataire(echeancier.getIdAmodiataire())
    			.idEcheancier(echeancier.getId())
    			.montanHT(montantHt)
    			.montant(montantTTC)
    			.ref("ECH-"+dateDebutEcheance.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));

    		echeanceRepository.save(echeance);
    		echeances.add(echeance);
    	}
		return echeances;
	}


	private Echeancier creerEcheancier(Amodiataire amodiataire) {
    	Echeancier echeancier = new Echeancier();
    	DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
		echeancier.dateDebut(amodiataire.getDateSignature())
    			  .dateFin(amodiataire.getDateSignature().plusYears(amodiataire.getDuree()))
    			  .idAmodiataire(amodiataire.getId())
    			  .ref("ECHR-"+echeancier.getDateDebut().format(dateTimeFormatter));
    	echeancierRepository.save(echeancier);
		return echeancier;
	}

	/**
     *  Get all the amodiataires.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Amodiataire> findAll(Pageable pageable) {
        log.debug("Request to get all Amodiataires");
        Page<Amodiataire> result = amodiataireRepository.findAll(pageable);
        //Page<Amodiataire> result = amodiataireRepository.findByStatut(StatutAOT.VALIDE, pageable);
        return result;
    }

    /**
     *  Get one amodiataire by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Amodiataire findOne(Long id) {
        log.debug("Request to get Amodiataire : {}", id);
        Amodiataire amodiataire = amodiataireRepository.findOne(id);
        return amodiataire;
    }

    /**
     *  Delete the  amodiataire by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Amodiataire : {}", id);
        // Chercher les factures
        int nbFactures = factureRepository.getFactureImpayeOfClient(id).size();
        // Si une facture existe (meme non payee), declencher une erreure.
        if(nbFactures > 0) throw new GeoServiceException("gestionaotApp.amodiataire.error.delete.factureExist");
        // Chercher les reglements
        int nbReglement = reglementRepository.findByIdAmodiataire(id).size();
        // Si un reglement existe, declencher une errreure
        if(nbReglement > 0) throw new GeoServiceException("gestionaotApp.amodiataire.error.delete.reglementExist");
        // Supprimer ses echeances
        List<Echeance> echeances = echeanceRepository.findByIdAmodiataire(id);
        echeanceRepository.delete(echeances);
        // Supprimer son echeancier
        List<Echeancier> echeanciers = echeancierRepository.findByIdAmodiataire(id);
        echeancierRepository.delete(echeanciers);
        // Supprimer ses points
        Amodiataire amodiataire = amodiataireRepository.findOne(id);
        List<Point> points = pointRepository.findByAmodiataire(amodiataire);
        pointRepository.delete(points);
        // Supprimer l'entite
        amodiataireRepository.delete(amodiataire);
    }

	/**
	 * <p> Create 'VALIDATE' action to all the users with the role 'VALIDATE_AOT' </p>
	 *
	 * @param amodiataire
	 */
	private void createValidateTasks(Amodiataire amodiataire) {
		// Find the user
		String userLogin = SecurityUtils.getCurrentUserLogin();
		Optional<User> login = userRepository.findOneByLogin(userLogin);
		if(!login.isPresent()) return;
		User creator = login.get();

		Authority authority = authorityRepository.findOne("VALIDER_AOT");
		List<Role> roles = findRoles(authority);
		List<User> enabledUsers = userService.findUsersInRoles(roles);

		// Creer la tache pour chaque utilisateur ayant le droit
		for (User assignee : enabledUsers) {
			createTaskToValidate(amodiataire, creator, assignee);
		}
	}

	private void createTaskToValidate(Amodiataire amodiataire, User creator, User assignee) {
		Task task = new Task();
		task.setAction(TaskAction.VALIDATE);
		task.setAssignee(assignee);
		task.setEntityId(amodiataire.getId());
		task.setEntityName(Amodiataire.class.getSimpleName());
		task.setDescription(amodiataire.getNomSociete());
		task.setStatus(TaskStatus.NEW);
		task.creator(creator);
		taskRepository.save(task);
	}

	public List<Amodiataire> search(AmodiataireSearchRequest searchDTO) {
		String ville = searchDTO.getVille();
		String numeroAutorisation = searchDTO.getNumeroAutorisation();
		String nomSociete = searchDTO.getNomSociete();
		String numTelephone = searchDTO.getNumeroTelephone();
		String niu = searchDTO.getNiu();
		String rccm = searchDTO.getRccm();
		int year = searchDTO.getYear();
		year = year < 1975 ? Year.now().getValue() : year;
		StatutAOT statut = searchDTO.getStatut() == null ? StatutAOT.VALIDE : searchDTO.getStatut();
		
		ZonedDateTime date = ZonedDateTime.now();
		ZonedDateTime from = clean(date,year, 01, 01, 00, 00, 01); // 1st Jan of $year
		ZonedDateTime to = clean(date, year, 12, 31, 23, 59, 59); // 31st Dec of $year
		
		log.debug("From : {}", from);
		log.debug("to : {}", to);
		
		String baseQuery = "SELECT a FROM Amodiataire AS a WHERE a.deleted = :deleted and a.statut = :statut";
		TypedQuery<Amodiataire> typedQuery = SearchQueryBuilder.getInstance(em, baseQuery)
					.andStringIgnoreCaseProperty("ville", ville)
					.andStringIgnoreCaseProperty("numeroAutorisation", numeroAutorisation)
					.andStringIgnoreCaseProperty("nomSociete", nomSociete)
					.andStringIgnoreCaseProperty("numeroTelephone", numTelephone)
					.andStringIgnoreCaseProperty("niu", niu)
					.andStringIgnoreCaseProperty("rccm", rccm)
					.andDateBetween("createdDate", from, to, "from", "to")
					.andEqualsProperty("statut", statut)
					.build(Amodiataire.class);
		
		typedQuery.setParameter("statut", statut);
		typedQuery.setParameter("deleted", false);

		List<Amodiataire> resultList = typedQuery.getResultList();
		return resultList;
	}
	
	private ZonedDateTime clean(ZonedDateTime date,int year, int month, int day, int hours, int minute, int seconds) {
		date = date.withYear(year);
		date = date.withMonth(month);
		date = date.withDayOfMonth(day);
		date = date.withHour(hours);
		date = date.withMinute(minute);
		date = date.withSecond(seconds);
		return date;
	}

	public List<Point> loadByAmodiataireId(Long amodiataireId) {
		Amodiataire amodiataire = amodiataireRepository.findOne(amodiataireId);
		return loadByAmodiataire(amodiataire);
	}

	public List<Point> loadByAmodiataire(Amodiataire amodiataire) {
		return pointRepository.findByAmodiataire(amodiataire);
	}

	public Amodiataire save(Amodiataire amodiataire) {
		return amodiataireRepository.save(amodiataire);
	}

	public Amodiataire validerAOT(Long id) {
		Amodiataire amodiataire = amodiataireRepository.findOne(id);
		amodiataire.setStatut(StatutAOT.VALIDE);
		amodiataireRepository.save(amodiataire);

		// Creer l'Echeancier
		Echeancier echeancier = creerEcheancier(amodiataire);

		// Creer les Echeances
		List<Echeance> echeances = creerEcheances(echeancier, amodiataire.getMontantTrimestrielHT(), amodiataire.getMontantTrimestrielTTC());

		// Mettre a jour les montants de l'echeancier
		echeancier = mettreAjourMontantsEcheancier(echeancier, echeances);

		List<Task> tasks = chercherTaches(id);
		closeTasks(tasks);
		return amodiataire;
	}

	private void closeTasks(List<Task> tasks) {
		for (Task task : tasks) {
			task.setStatus(TaskStatus.EXECUTED);
			task.setLastStatusDate(ZonedDateTime.now());
			taskRepository.save(task);
		}
	}

	/**
	 * Liste des taches a fermer
	 * @param id
	 * @return
	 */
	private List<Task> chercherTaches(Long id) {
		List<Task> tasks = taskRepository.find(id, Amodiataire.class.getSimpleName(), TaskStatus.NEW);
		return tasks;
	}

	List<Role> findRoles(Authority authority) {
		List<Role> results = new ArrayList<>();
		List<Role> roles = roleRepository.findAll();
		for (Role role : roles) {
			Set<Authority> authorities = role.getAuthorities();
			for (Authority auth : authorities) {
				if(auth.equals(authority)) {
					results.add(role);
				}
			}
		}
		return results;
	}


	/**
	 * Build GeoJson for a given amodiataire and its list of points.
	 * @param amodiataire
	 * @param points
	 * @return
	 * @throws JSONException
	 */
	protected JSONObject getAmodiataireGeoJson(Amodiataire amodiataire, List<Point> points) throws JSONException {
		// Creer l'object json
        JSONObject main = new JSONObject();
        // Ajouter le type
        main.put("type", "FeatureCollection");
        // Ajouter le tableau de feature
        JSONArray features = new JSONArray();
        // Creer un feature
        JSONObject feature = new JSONObject();
        // Ajouter l'id
        feature.put("id",amodiataire.getId());
        // Ajouter le type
        feature.put("type", "Feature");
        // Ajouter les properties
        JSONObject properties = new JSONObject();
        properties.put("name", amodiataire.getNomSociete());
        properties.put("Category", "AOT");
        // Ajouter la geometrie
        JSONObject geometry = new JSONObject();
        // Ajouter le type de la geometrie
        geometry.put("type", "Polygon");
        // Ajouter les Tableau de tableaux de Coordonees
        JSONArray coordinates = new JSONArray();
        // Ajouter tableaux de coordonees
        JSONArray coordinates0 = new JSONArray();
        for (Point point : points) {
            JSONArray coordinate = new JSONArray();
            coordinate.put(point.getLng());
            coordinate.put(point.getLat());
            coordinates0.put(coordinate);
        }
        coordinates.put(coordinates0);
        geometry.put("coordinates", coordinates);
        feature.put("geometry", geometry);
        feature.put("properties", properties);
        features.put(feature);
        main.put("features", features);
        log.debug("GeoJSON created : {}", main.toString());
		return main;
	}

	/**
	 * GeoJson for one amodiataire
	 * @param amodiataire
	 * @return
	 * @throws JSONException
	 */
	public JSONObject getAmodiataireGeoJson(Amodiataire amodiataire) throws JSONException {
		List<Point> points =  pointRepository.findByAmodiataire(amodiataire);
        JSONObject main = getAmodiataireGeoJson(amodiataire, points);
		return main;
	}

	/**
	 * GeoJson for all Amodiataires
	 * @return
	 * @throws JSONException
	 */
	public List<JSONObject> getAmodiataireGeoJsons() throws JSONException {
		List<Amodiataire> amodiataires = amodiataireRepository.findAll();
		List<JSONObject> jsonObjects = new ArrayList<>();
		for (Amodiataire amodiataire : amodiataires) {
			JSONObject jsonObj = getAmodiataireGeoJson(amodiataire);
			jsonObjects.add(jsonObj);
		}
		return jsonObjects;
	}


	/**
	 * @param pageable
	 * @return
	 */
	public Page<AmodiataireDto> findAllDto(Pageable pageable) {
		log.debug("Request to get all Amodiataires");
        Page<Amodiataire> result = findAll(pageable);

        // Convertir en dto
        Page<AmodiataireDto> pageDto = result.map(amodiataire -> {
        	return convertToDto(amodiataire);
        });
        return pageDto; // Return
	}

    private AmodiataireDto convertToDto(Amodiataire amodiataire) {
        AmodiataireDto amodiataireDto = new AmodiataireDto();

        // Copier les proprietes
        BeanUtils.copyProperties(amodiataire, amodiataireDto);

        // Rechercher les echeances non reglees uniquement
        List<Echeance> echeances = echeanceRepository.findEcheanceNonReglee(amodiataire.getId(), Boolean.FALSE);

        // Determiner le statut de l'echeance
        StatutEcheance statutEcheance = statutEcheance(echeances);
        amodiataireDto.setStatutEcheance(statutEcheance);

        // return
        return amodiataireDto;
    }

    public AmodiataireDto findOneAmodiataireDto(Long id) {
        Amodiataire amodiataire = findOne(id);
        return convertToDto(amodiataire);
    }

	/**
	 * @param echeances
	 * @return
	 */
	private StatutEcheance statutEcheance(List<Echeance> echeances) {
		StatutEcheance statutEchance =  null;
		if(echeances.isEmpty()) {
			statutEchance = StatutEcheance.PAYEE;
		} else {
			Echeance echeance = echeances.iterator().next();
			long dureeEcart = ChronoUnit.DAYS.between(LocalDate.now(), echeance.getDateFin());
			if(dureeEcart < 0) {
				statutEchance = StatutEcheance.ECHUE;
			}
			if(dureeEcart >0 && dureeEcart <= 90 && echeance.isEstFacture()) {
				statutEchance = StatutEcheance.FACTUREE;
			}

			if(dureeEcart >0 && dureeEcart <= 90 && !echeance.isEstFacture()) {
				statutEchance = StatutEcheance.NON_FACTUREE;
			}

			if(dureeEcart > 90) {
				statutEchance = StatutEcheance.PAYEE;
			}
		}
		return statutEchance;
	}


	public Page<Amodiataire> findAmodiataireEnAttente(Pageable pageable) {
		Page<Amodiataire> result = amodiataireRepository.findByCreatedByAndStatut(SecurityUtils.getCurrentUserLogin(), StatutAOT.EN_ATTENTE, pageable);
		return result;
	}
	
	public Page<Amodiataire> findAmodiataireValider(Pageable pageable) {
		Page<Amodiataire> result = amodiataireRepository.findByCreatedByAndStatut(SecurityUtils.getCurrentUserLogin(), StatutAOT.VALIDE, pageable);
		return result;
	}
	

    public List<Amodiataire> filterAmodiataires(String query) {
        query = StringUtils.trimToEmpty(query);
        if(StringUtils.isBlank(query)) return new ArrayList<>();
        query = "%"+query+"%";
        List<Amodiataire> amodiataires = amodiataireRepository.filterAmodiataires(query, StatutAOT.VALIDE);
        return amodiataires;
    }


	public Amodiataire annulerAmodiataire(Amodiataire amodiataire) {
		if(amodiataire == null) return amodiataire;
		Long id = amodiataire.getId();
		Amodiataire existing = amodiataireRepository.findOne(id);
		if(existing != null) existing.setStatut(StatutAOT.ANNULER);
		existing = amodiataireRepository.save(existing);
		return existing;
	}
}
