package com.geospace.gestionaot.service.builder;

import org.apache.commons.lang3.StringUtils;

/**
 * <p> {@link Contract} is a general purpose class used to quickly do some
 * 
 * assertions </p>
 */
public class Contract {
	
	/**
	 * This method test if the parameter object is null and throw a new
	 * 
	 * {@link IllegalStateException}.
	 * 
	 * @param object
	 */
	public static void assertNotNull(Object object) {
		if(object == null) throw new IllegalStateException();
	}

	public static void assertNotEmpty(String value) {
		if(StringUtils.isBlank(value)) throw new IllegalStateException();
	}

	/**
	 * @param result
	 */
	public static void assertTrue(boolean result) {
		if(result != true ) throw new IllegalStateException(); 
	}
}
