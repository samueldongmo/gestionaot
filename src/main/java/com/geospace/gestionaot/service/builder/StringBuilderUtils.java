package com.geospace.gestionaot.service.builder;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>Utility class to make some assertion on {@link StringBuilder} </p>
 */
public class StringBuilderUtils {
	
	
	/**
	 * <p>Test if the given {@link StringBuilder} ends with the specified charsquence </p>
	 * 
	 * @param builder
	 * @param str
	 * @return
	 */
	public static boolean endWith(StringBuilder builder, String str) {
		return StringUtils.endsWith(builder, str);
	}
	
	/**
	 * <p>Test if the given {@link StringBuilder} do not ends with the specified string</p>
	 * @param builder
	 * @param str
	 * @return
	 */
	public static boolean doNotEndWith(StringBuilder builder,String str) {
		return !endWith(builder, str);
	}

	/**
	 * @param qBuilder
	 * @param args
	 * @return
	 */
	public static boolean endWiths(StringBuilder qBuilder, String ... args) {
		return StringUtils.endsWithAny(qBuilder, args);
	}

	/**
	 * @param qBuilder
	 * @param args
	 * @return
	 */
	public static boolean doNotendWiths(StringBuilder qBuilder, String ... args) {
		return !endWiths(qBuilder, args);
	}
}
