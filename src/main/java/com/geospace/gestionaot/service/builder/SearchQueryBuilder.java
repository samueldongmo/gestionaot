package com.geospace.gestionaot.service.builder;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p> Dynamic search query builder </p>
 * 
 *
 */
public class SearchQueryBuilder {
	private Logger log = LoggerFactory.getLogger(SearchQueryBuilder.class);
	
	/** The qBuilder field */
	private StringBuilder qBuilder ;
	/** The em field */
	private EntityManager em;
	/** The keyParameterValues field */
	private Map<String, Object> keyParameterValues = new HashMap<String, Object>();
	/** The andOperator field */
	private String andOperator = " AND ";
	/** The orOperator field */
	private String orOperator = " OR ";
	/** The orderByExpression field */
	private String orderByExpression;
	
	private SearchQueryBuilder (EntityManager em, String baseQuery) {
		Contract.assertNotNull(em);
		Contract.assertNotEmpty(baseQuery);
		this.em = em;
		this.qBuilder = new StringBuilder(baseQuery);
	}
	
	/**
	 * @param em
	 * @param baseQuery
	 * @return
	 */
	public static SearchQueryBuilder getInstance(EntityManager em, String baseQuery) {
		return new SearchQueryBuilder(em, baseQuery);
	}
	
	/**
	 * @param key
	 * @param value
	 * @return
	 */
	public SearchQueryBuilder andStringIgnoreCaseProperty(String key, String value) {
		return stringIgnoreCaseProperty(andOperator, key, value);
	}
	

	/**
	 * @param key
	 * @param value
	 * @return
	 */
	public SearchQueryBuilder orStringIgnoreCaseProperty(String key, String value) {
		return stringIgnoreCaseProperty(key, value, orOperator);
	}
	
	public SearchQueryBuilder andIntEquals(String key, int value, boolean expression) {
		Contract.assertNotEmpty(key);
		if (expression) {
			qBuilder.append(andOperator).append("a."+key+" = :"+key);
			keyParameterValues.put(key, value);
		}
		return this;
	}

	public SearchQueryBuilder andBooleanEquals(String key, Boolean value, boolean expression) {
		if(value == null) {
			return this;
		}
		Contract.assertNotEmpty(key);
		if (expression && Boolean.TRUE.equals(value)) {
			qBuilder.append(andOperator).append("a."+key+" = :"+key);
			keyParameterValues.put(key, value);
		} else if(expression && Boolean.FALSE.equals(value)) {
			qBuilder.append(andOperator).append(" (a."+key+" = :"+key+" or a."+key+" = null) ");
			keyParameterValues.put(key, value);
		}
		return this;
	}


	public SearchQueryBuilder andBooleanEquals(String key, Boolean value) {
		Contract.assertNotEmpty(key);
		if (value != null) {
			qBuilder.append(andOperator).append("a."+key+" = :"+key);
			keyParameterValues.put(key, value);
		}
		return this;
	}
	

	/**
	 * @param key
	 * @param value
	 * @return
	 */
	public SearchQueryBuilder andEqualsProperty (String key, Object value) {
		return appendEqualsProperty(andOperator,key,value);
	}

	/**
	 * @param key
	 * @param value
	 * @return
	 */
	public SearchQueryBuilder orEqualsProperty (String key, Object value) {
		return appendEqualsProperty(orOperator,key,value);
	}

	
	/**
	 * @param orderByExpression
	 * @return
	 */
	public SearchQueryBuilder addOrderBy(String orderByExpression) {
		this.orderByExpression = orderByExpression;
		return this;
	}
	/**
	 * @param operator
	 * @param key
	 * @param value
	 * @return
	 */
	private SearchQueryBuilder appendEqualsProperty(String operator, String key, Object value) {
		Contract.assertNotEmpty(key);
		if(value != null) {
			if(value instanceof String) {
				if(StringUtils.isBlank((String)value)) return this;
			}
			String param = formatKeyToParam(key);
			appendOperator(operator);
			qBuilder.append("(a."+key+" = :"+param+")");
			keyParameterValues.put(param, value);
		}
		return this;
	}

	private String formatKeyToParam(String key) {
		return StringUtils.remove(key, ".");
	}

	/**
	 * @param operator
	 * @param key
	 * @param value
	 * @return
	 */
	private SearchQueryBuilder stringIgnoreCaseProperty(String operator, String key, String value) {
		Contract.assertNotEmpty(key);
		if(StringUtils.isNotBlank(value)) {
			String param = formatKeyToParam(key);
			appendOperator(operator);
			qBuilder.append("(LOWER(a."+key+") LIKE(LOWER(:"+param+")))");
			String updatedValue = "%"+value.toLowerCase()+"%";
			keyParameterValues.put(param, updatedValue);
		}
		return this;
	}
	
	
	private void appendOperator(String operator) {
		if(StringBuilderUtils.doNotendWiths(qBuilder,"WHERE", "WHERE ")) {
			qBuilder.append(operator);
		}
		
	}
	
	public String getFinalQuery() {
		return qBuilder.toString();
	}
	public SearchQueryBuilder andDateBetween(String field, ZonedDateTime param1, ZonedDateTime param2,
			String queryParam1, String queryParam2) {
		if(param1 == null) return this;
		appendOperator(this.andOperator);
		qBuilder.append(" (a."+field+" BETWEEN :"+queryParam1+" AND :"+queryParam2+")");
		keyParameterValues.put(queryParam1, param1);
		keyParameterValues.put(queryParam2, param2);
		return this;
	}

	public SearchQueryBuilder andDateBetween(String field, LocalDate param1, LocalDate param2,
			String queryParam1, String queryParam2) {
		if(param1 == null) return this;
		appendOperator(this.andOperator);
		qBuilder.append(" (a."+field+" BETWEEN :"+queryParam1+" AND :"+queryParam2+")");
		keyParameterValues.put(queryParam1, param1);
		keyParameterValues.put(queryParam2, param2);
		return this;
	}
	/**
	 * @param resultClass
	 * @return
	 */
	public  <T> TypedQuery<T> build(Class<T> resultClass) {
		//If the query ends with 'WHERE' exp : SELECT a FROM USER AS a WHERE,
		if(StringBuilderUtils.endWiths(qBuilder,"WHERE" , "WHERE ")) {
			//remove the WHERE part, so the query remain : SELECT a FROM USER AS a
			//TODO : IMPROVE this builder.
			qBuilder = new StringBuilder(StringUtils.remove(qBuilder.toString(), "WHERE"));
		}
		if(StringUtils.isNotBlank(this.orderByExpression)) {
			qBuilder.append(orderByExpression);
		}
		log.debug(qBuilder.toString());
		//get and instance of typed query
		TypedQuery <T> typedQuery = em.createQuery(qBuilder.toString(), resultClass);
		
		//Get all the keys with values previously saved in the map
		Set<String> params = keyParameterValues.keySet();
		
		//for every key, add it value to the typed query.
		for (String param : params) {
			typedQuery.setParameter(param, keyParameterValues.get(param));
		}
		//return the typed query
		return typedQuery;
	}

}