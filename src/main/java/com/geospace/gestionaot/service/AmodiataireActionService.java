package com.geospace.gestionaot.service;

import com.geospace.gestionaot.domain.Amodiataire;

public interface AmodiataireActionService {

	boolean updateNextEcheance(Amodiataire amodiataire);

}
