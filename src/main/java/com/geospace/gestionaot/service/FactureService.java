package com.geospace.gestionaot.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.geospace.gestionaot.domain.Facture;
import com.geospace.gestionaot.service.dto.FactureDTO;

/**
 * Service Interface for managing Facture.
 */
public interface FactureService {

    /**
     * Save a facture.
     * @return the persisted entity
     */
    public FactureDTO save(FactureDTO factureDTO);

    /**
     *  get all the factures.
     *  @return the list of entities
     */
    public Page<Facture> findAll(Pageable pageable);

    /**
     *  get the "id" facture.
     *  @return the entity
     */
    public FactureDTO findOne(Long id);

    /**
     *  delete the "id" facture.
     */
    public void delete(Long id);

    /**
     * search for the facture corresponding
     * to the query.
     */
    public List<FactureDTO> search(String query);
    
    /**
     * search for the facture corresponding to a client 
     * .
     */
    public List<FactureDTO> searchFactureFromClientId(Long idClient);

	public List<Facture> findAllWithoutPage();
}
