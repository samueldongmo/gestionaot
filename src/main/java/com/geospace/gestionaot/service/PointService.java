package com.geospace.gestionaot.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geospace.gestionaot.domain.Point;
import com.geospace.gestionaot.repository.PointRepository;

/**
 * Service Implementation for managing Point.
 */
@Service
@Transactional
public class PointService {

    private final Logger log = LoggerFactory.getLogger(PointService.class);
    
    private final PointRepository pointRepository;

    public PointService(PointRepository pointRepository) {
        this.pointRepository = pointRepository;
    }

    /**
     * Save a point.
     *
     * @param point the entity to save
     * @return the persisted entity
     */
    public Point save(Point point) {
        log.debug("Request to save Point : {}", point);
        Point result = pointRepository.save(point);
        return result;
    }

    /**
     *  Get all the points.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Point> findAll(Pageable pageable) {
        log.debug("Request to get all Points");
        Page<Point> result = pointRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one point by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Point findOne(Long id) {
        log.debug("Request to get Point : {}", id);
        Point point = pointRepository.findOne(id);
        return point;
    }

    /**
     *  Delete the  point by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Point : {}", id);
        pointRepository.delete(id);
    }
}
