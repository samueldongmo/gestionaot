package com.geospace.gestionaot.service;

import org.springframework.stereotype.Service;

import com.geospace.gestionaot.domain.Amodiataire;
import com.geospace.gestionaot.domain.Echeance;
import com.geospace.gestionaot.domain.Echeancier;
import com.geospace.gestionaot.domain.Facture;
import com.geospace.gestionaot.domain.Reglement;
import com.geospace.gestionaot.repository.AmodiataireRepository;
import com.geospace.gestionaot.repository.EcheancierRepository;
import com.geospace.gestionaot.repository.FactureRepository;
import com.geospace.gestionaot.repository.ReglementRepository;

@Service
public class EcheanceServiceUtil {


    private final EcheancierRepository echeancierRepository;

    private final AmodiataireRepository amodiataireRepository;

    private final ReglementRepository reglementRepository;

    private final FactureRepository factureRepository;
    
    public EcheanceServiceUtil(EcheancierRepository echeancierRepository, AmodiataireRepository amodiataireRepository,
			ReglementRepository reglementRepository, FactureRepository factureRepository) {
		super();
		this.echeancierRepository = echeancierRepository;
		this.amodiataireRepository = amodiataireRepository;
		this.reglementRepository = reglementRepository;
		this.factureRepository = factureRepository;
	}



	public Echeance setComplementaryData(Echeance echeance) {
        if(echeance == null) return null;


        if(echeance.getIdAmodiataire() != null) {
            Amodiataire amodiataire = amodiataireRepository.findOne(echeance.getIdAmodiataire());
            if(amodiataire != null)echeance.setNomSociete(amodiataire.getNomSociete());
        }
        if(echeance.getIdEcheancier() != null) {
            Echeancier echeancier = echeancierRepository.findOne(echeance.getIdEcheancier());
            if(echeancier != null) {
                echeance.setRefEcheancier(echeancier.getRef());
            }
        }

        if(echeance.getIdReglement() != null) {
            Reglement reglement = reglementRepository.findOne(echeance.getIdReglement());
            if(reglement != null) {
                echeance.setRefReglement(reglement.getRef());
            }
        }

        if(echeance.getIdFacture() != null) {
            Facture facture = factureRepository.findOne(echeance.getIdFacture());
            if(facture != null)echeance.setRefFacture(facture.getRef());
        }
        return echeance;
    }
    
}
