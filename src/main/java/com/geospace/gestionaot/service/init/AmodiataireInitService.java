package com.geospace.gestionaot.service.init;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.geospace.gestionaot.domain.Amodiataire;
import com.geospace.gestionaot.domain.Point;
import com.geospace.gestionaot.domain.StatutAOT;
import com.geospace.gestionaot.repository.AmodiataireRepository;
import com.geospace.gestionaot.repository.PointRepository;
import com.geospace.gestionaot.service.AmodiataireService;
import com.geospace.gestionaot.service.dto.AmodiataireCreateDTO;

@Service
public class AmodiataireInitService {
    private final AmodiataireRepository amodiataireRepository;
    private final PointRepository pointRepository;
    private final AmodiataireService amodiataireService;
    
    public AmodiataireInitService(AmodiataireRepository amodiataireRepository, PointRepository pointRepository,
			AmodiataireService amodiataireService) {
		super();
		this.amodiataireRepository = amodiataireRepository;
		this.pointRepository = pointRepository;
		this.amodiataireService = amodiataireService;
	}

	//@PostConstruct
    void init() {
    	if(amodiataireRepository.count() > 0) return;
    	
    	Amodiataire amod = saveAmodiataire("CAPCOM","M031200048180D", "RC/DLA/2012/B/4627",5,
    			"3258",12774.405579,"677558841", LocalDate.now().minusMonths(80), BigDecimal.valueOf(1500000),
    			BigDecimal.valueOf(1788750));
    	Point a1p1 = savePoint(amod,1, 4.028225697908771, 9.681195410398951);
    	Point a1p2 = savePoint(amod,2, 4.029072261253727, 9.682466743120427);
    	Point a1p3 = savePoint(amod,3, 4.028609272701305, 9.682931187080115);
    	Point a1p4 = savePoint(amod,4, 4.02767264925615, 9.681560968817577);
    	Point a1p5 = savePoint(amod,5, 4.02814132670212, 9.681251178771587);
    	Point a1p6 = savePoint(amod,6, 4.028225697908771, 9.681195410398951);
    	saveCustom(amod, a1p1,a1p2,a1p3,a1p4,a1p5,a1p6);

    	amod = saveAmodiataire("KONAMI","M030700048181A", "RC/DLA/2007/A/4614",5,
    			"6657",6038.973328,"655223335", LocalDate.now().minusMonths(90), BigDecimal.valueOf(1500000),
    			BigDecimal.valueOf(1788750));
    	saveCustom (amod,savePoint(amod,1, 4.028143155743315, 9.680463304512669),
    	savePoint(amod,2, 4.028483759741625, 9.680946509615749),
    	savePoint(amod,3, 4.028225697908771, 9.681195410398951),
    	savePoint(amod,4, 4.027998031777961, 9.681029608103248),
    	savePoint(amod,5, 4.027524710441319, 9.681317297432553),
    	savePoint(amod,6, 4.02731673057191, 9.681008166108228),
    	savePoint(amod,7, 4.028143155743315, 9.680463304512669));
    	

    	amod = saveAmodiataire("SNK","M030200048182D", "RC/DLA/2002/A/2374",10,
    			"9631",1878.332397,"655447878", LocalDate.now().minusMonths(42), BigDecimal.valueOf(1500000),
    			BigDecimal.valueOf(1788750));
    	saveCustom (amod,savePoint(amod,1, 4.028141326702126, 9.681251178771587),
    	savePoint(amod,2, 4.027672649256159, 9.681560968817577),
    	savePoint(amod,3, 4.027524710441319, 9.681317297432553),
    	savePoint(amod,4, 4.027998031777961, 9.681029608103248),
    	savePoint(amod,5, 4.028141326702126, 9.681251178771587));

    	amod = saveAmodiataire("SODECOTON","M031100048183D", "RC/DLA/2011/A/6232",10,
    			"32564",10388.782364,"677858444", LocalDate.now().minusMonths(55), BigDecimal.valueOf(1500000),
    			BigDecimal.valueOf(1788750));
    	saveCustom (amod,savePoint(amod,1, 4.026996730839358, 9.680536808354104),
    	savePoint(amod,2, 4.02656123325455, 9.679883737795501),
    	savePoint(amod,3, 4.026569678107494, 9.679812767930457),
    	savePoint(amod,4, 4.027334569640656, 9.679315172305875),
    	savePoint(amod,5, 4.027411425919985, 9.679327761383082),
    	savePoint(amod,6, 4.027842731146395, 9.679980828994824),
    	savePoint(amod,7, 4.026996730839358, 9.680536808354104));
    	

    	amod = saveAmodiataire("SOCAPALM","M039900048184D", "RC/DLA/1999/C/354",20,
    			"12547",18937.465424,"698875482", LocalDate.now().minusMonths(31), BigDecimal.valueOf(1500000),
    			BigDecimal.valueOf(1788750));
    	saveCustom (amod,savePoint(amod,1, 4.026674444114522, 9.687876356669467),
    	savePoint(amod,2, 4.027015865266167, 9.688226121233965),
    	savePoint(amod,3, 4.024774353162065, 9.690440657836676),
    	savePoint(amod,4, 4.024423689537352, 9.690100082601957),
    	savePoint(amod,5, 4.026674444114522, 9.687876356669467));
    	
    }
    
    private void saveCustom(Amodiataire amod, Point ... ps) {
    	List<Point> points = ( ps == null ? new ArrayList<>() : Arrays.asList(ps));
    	AmodiataireCreateDTO amodiataireCreateDTO = new AmodiataireCreateDTO();
    	BeanUtils.copyProperties(amod, amodiataireCreateDTO);
    	amodiataireCreateDTO.setPoints(points);
    	amodiataireService.saveCustom(amodiataireCreateDTO);
	}

	private Point savePoint(Amodiataire amod,int numBorne,  double lat, double lng) {
    	Point p = newPoint(amod, numBorne, lat, lng);
//    	return pointRepository.save(p);
    	return p;
	}

	private Amodiataire saveAmodiataire(String nomSociete, String niu, String rccm, int duree, String bp, double superficie,
			String tel, LocalDate dateSignature, BigDecimal montantHt, BigDecimal montantTtc) {
    	Amodiataire amodiataire = new Amodiataire();
    	amodiataire.setNomSociete(nomSociete);
    	amodiataire.setNumContribuable(niu);
    	amodiataire.setRccm(rccm);
    	amodiataire.setDuree(duree);
    	amodiataire.setSuperficie(superficie);
    	amodiataire.setBp(bp);
    	amodiataire.setNumeroTelephone(tel);
    	amodiataire.setStatut(StatutAOT.VALIDE);
    	amodiataire.setDateSouscription(LocalDate.now().minusYears(3));
    	amodiataire.setDateSignature(dateSignature);
    	amodiataire.setMontantTrimestrielHT(montantHt);
    	amodiataire.setMontantTrimestrielTTC(montantTtc);
//    	amodiataire = amodiataireRepository.save(amodiataire);
		return amodiataire;
	}

    public Point newPoint(Amodiataire amodiataire, Integer numBorne, Double lat, Double lng) {
        Point point = new Point();
        point.setAmodiataire(amodiataire);
        point.numBorne(numBorne);
        point.setLat(lat);
        point.setLng(lng);
        return point;
    }

    public  void savePoints(Point ... points) {
        if(points == null) return ;
        for (Point point : points) {
            pointRepository.save(point);
        }
    }
}
