package com.geospace.gestionaot.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.geospace.gestionaot.domain.Amodiataire;
import com.geospace.gestionaot.domain.Facture;
import com.geospace.gestionaot.repository.AmodiataireRepository;
import com.geospace.gestionaot.repository.FactureRepository;
import com.geospace.gestionaot.service.AmodiataireActionService;

@Service
public class AmodiataireCustomServiceImpl implements AmodiataireActionService{
	private static final Logger log = LoggerFactory.getLogger(AmodiataireCustomServiceImpl.class);
	
	private AmodiataireRepository amodiataireRepository;
	private FactureRepository factureRepository;
	
    public AmodiataireCustomServiceImpl(AmodiataireRepository amodiataireRepository,
			FactureRepository factureRepository) {
		super();
		this.amodiataireRepository = amodiataireRepository;
		this.factureRepository = factureRepository;
	}

	@Override
	public boolean updateNextEcheance(Amodiataire amodiataire) {
    	boolean result = false;
    	// Recuperer la liste des factures impayees
    	List<Facture> factureImpayees = factureRepository.getFactureImpayeOfClient(amodiataire.getId());
    	
    	// Si facture est egale a 1, alors la date prochaine facture est la date de la facture.
    	if(factureImpayees.size() >= 1) {
    		Facture facture = factureImpayees.iterator().next();
    		// Update date prochaine echeance
    		result= updateNextEcheance(amodiataire, facture);
    		log.info("Date prochaine date Echeance Amodiataire - {} modifiee avec success. Facture Orig. : {}", amodiataire.getId(), facture.getRef());
    	}
    	if(factureImpayees.size() == 0) {
    		result= updateNextEcheance(amodiataire, null);
    	}
		return result;
	}

	private boolean updateNextEcheance(Amodiataire amodiataire, Facture facture) {
		if(facture == null) {
			amodiataire.setDateEcheance(null);
		} else {
			amodiataire.setDateEcheance(facture.getDateFin());
		}
		amodiataireRepository.save(amodiataire);
		return true;
	}

}
