package com.geospace.gestionaot.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geospace.gestionaot.domain.Facture;
import com.geospace.gestionaot.repository.FactureRepository;
import com.geospace.gestionaot.service.BalancegService;
import com.geospace.gestionaot.service.dto.BalancegDTO;
import com.geospace.gestionaot.service.dto.BalancegItemDTO;

@Service
@Transactional
public class BalancegServiceImpl implements BalancegService {

	private static final Logger log = LoggerFactory.getLogger(BalancegServiceImpl.class);
	private final FactureRepository factureRepository;
	
	public BalancegServiceImpl(FactureRepository factureRepository) {
		this.factureRepository = factureRepository;
	}
	@Override
	public BalancegDTO findBanlancegByDates(LocalDate fromDate){
		// Retrouve la liste des facture impayé a la date
		List<Facture> listFactures = factureRepository.getFactureImpaye(fromDate);

		// Si la liste est vide on retourne null
		if(listFactures == null || listFactures.size() == 0) return null;

		// On construit la liste item de la Balanceg
		return getBalancegDTOs(listFactures, fromDate);
	}

	@Override
	public List<BalancegItemDTO> findByDates(LocalDate fromDate) {

		// Retrouve la liste des facture impayé a la date
		List<Facture> listFactures = factureRepository.getFactureImpaye(fromDate);

		// Si la liste est vide on retourne null
		if(listFactures == null || listFactures.size() == 0) return null;

		// On construit la liste item de la Balanceg
		return getBalancegDTOs(listFactures, fromDate).getBalancegItemDTOs();

	}


	private BalancegDTO getBalancegDTOs(List<Facture> listFactures, LocalDate fromDate){

		// Si la liste est vide on retourne null
		if(listFactures == null || listFactures.size() == 0) return null;

		// Initialisation des variables
		HashMap<Long, BalancegItemDTO> mapBalancegItemDTO = new HashMap<Long, BalancegItemDTO>();
		List<BalancegItemDTO> listBalancegItemDTOs = new ArrayList<BalancegItemDTO>();
		BalancegItemDTO totalItem = new BalancegItemDTO();
		BalancegDTO balancegDTO = new BalancegDTO();

		// Parcours la liste des factures
		for (Facture facture : listFactures) {

			// reccupere le balancegItemDTO courant dans le Map
			BalancegItemDTO currentBalancegItemDTO = mapBalancegItemDTO.get(facture.getAmodiataire().getId());
			if(currentBalancegItemDTO == null){ // S'il n'existe pas on le  cré et l'ajoute dans le map
				currentBalancegItemDTO = new BalancegItemDTO();
				currentBalancegItemDTO.setClient(facture.getAmodiataire().getNumContribuable()+"/"+facture.getAmodiataire().getNomSociete());
				currentBalancegItemDTO.setClientId(facture.getAmodiataire().getId());
				mapBalancegItemDTO.put(facture.getAmodiataire().getId(), currentBalancegItemDTO);
				listBalancegItemDTOs.add(currentBalancegItemDTO);
			}

			// Evaluation du retard de la facture : fromDate - facture.dateEcheance
			Long dureeRetard =  fromDate.toEpochDay() - facture.getDateEcheance().toEpochDay();

			// Ajouter le montant dans le cellItem correspondant
			if(dureeRetard <= 0) {
				currentBalancegItemDTO.getNonEchu().addFacture(facture);
				totalItem.getNonEchu().addFacture(facture);
			}
			if(dureeRetard >00 && dureeRetard <=90 ) {
				currentBalancegItemDTO.getEchuIntervalUn().addFacture(facture);
				totalItem.getEchuIntervalUn().addFacture(facture);
			}
			if(dureeRetard >90 && dureeRetard <=180 ) {
				currentBalancegItemDTO.getEchuIntervalDeux().addFacture(facture);
				totalItem.getEchuIntervalDeux().addFacture(facture);
			}
			if(dureeRetard >180 && dureeRetard <=270 ) {
				currentBalancegItemDTO.getEchuIntervalTrois().addFacture(facture);
				totalItem.getEchuIntervalTrois().addFacture(facture);
			}
			if(dureeRetard >270 ) {
				currentBalancegItemDTO.getEchuIntervalTroisEtPlus().addFacture(facture);
				totalItem.getEchuIntervalTroisEtPlus().addFacture(facture);
			}

			// Ajouter le montant dans le total encour
			currentBalancegItemDTO.getTotalEncour().addFacture(facture);
			totalItem.getTotalEncour().addFacture(facture);
		}

		balancegDTO.setBalancegItemDTOs(listBalancegItemDTOs);
		balancegDTO.setTotalItem(totalItem);
		balancegDTO.setFromDate(fromDate);
		return balancegDTO;
	}

}
