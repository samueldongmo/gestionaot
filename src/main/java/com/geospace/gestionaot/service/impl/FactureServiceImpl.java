package com.geospace.gestionaot.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geospace.gestionaot.domain.Amodiataire;
import com.geospace.gestionaot.domain.Echeance;
import com.geospace.gestionaot.domain.Facture;
import com.geospace.gestionaot.domain.Reglement;
import com.geospace.gestionaot.domain.ReglementFacture;
import com.geospace.gestionaot.repository.AmodiataireRepository;
import com.geospace.gestionaot.repository.EcheanceRepository;
import com.geospace.gestionaot.repository.FactureRepository;
import com.geospace.gestionaot.repository.ReglementFactureRepository;
import com.geospace.gestionaot.repository.ReglementRepository;
import com.geospace.gestionaot.service.AmodiataireActionService;
import com.geospace.gestionaot.service.FactureService;
import com.geospace.gestionaot.service.dto.FactureDTO;

/**
 * Service Implementation for managing Facture.
 */
@Service
@Transactional
public class FactureServiceImpl implements FactureService{

    private final Logger log = LoggerFactory.getLogger(FactureServiceImpl.class);
    
    private final FactureRepository factureRepository;
    private final AmodiataireRepository amodiataireRepository;
    private final AmodiataireActionService amodiataireActionService;
    private final EcheanceRepository echeanceRepository;
    private final ReglementRepository reglementRepository;
    private final ReglementFactureRepository reglementFactureRepository;

	public FactureServiceImpl(FactureRepository factureRepository, AmodiataireRepository amodiataireRepository,
			AmodiataireActionService amodiataireActionService, EcheanceRepository echeanceRepository,
			ReglementRepository reglementRepository, ReglementFactureRepository reglementFactureRepository) {
		super();
		this.factureRepository = factureRepository;
		this.amodiataireRepository = amodiataireRepository;
		this.amodiataireActionService = amodiataireActionService;
		this.echeanceRepository = echeanceRepository;
		this.reglementRepository = reglementRepository;
		this.reglementFactureRepository = reglementFactureRepository;
	}

	/**
     * Save a facture.
     * @return the persisted entity
     */
    public FactureDTO save(FactureDTO factureDTO) {
        log.debug("Request to save Facture : {}", factureDTO);
//        Facture facture = factureMapper.factureDTOToFacture(factureDTO);
        Facture facture = new Facture();
        BeanUtils.copyProperties(factureDTO, facture);
        
        // La date echeance est egale a la date de fin de la periode
        facture.setDateEcheance(facture.getDateFin());
        
        Amodiataire amodiataire = amodiataireRepository.findOne(factureDTO.getClientId());
        facture.setAmodiataire(amodiataire);
        facture = factureRepository.save(facture);
        // Marquer l'echeance comme facturee
        boolean marquerEcheance = marquerEcheanceFacturee(facture);
        log.debug("Marquer echeance reglee : {}", marquerEcheance);
        // Mettre a jour la date d'echeance de la facture
        // amodiataireActionService.updateNextEcheance(facture.getAmodiataire());
        FactureDTO result = new FactureDTO();
        BeanUtils.copyProperties(facture, result);
        return result;
    }

	/**
	 * Marquer l'echeance de cette facture comme reglee.
	 * @param facture
	 * @return
	 */
	private boolean marquerEcheanceFacturee(Facture facture) {
		Echeance echeance = echeanceRepository.findOne(facture.getIdEcheance());
		echeance.estFacture(true).idFacture(facture.getId());
		echeanceRepository.save(echeance);
		return false;
	}

	/**
     *  get all the factures.
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<Facture> findAll(Pageable pageable) {
        log.debug("Request to get all Factures");
        Page<Facture> result = factureRepository.findAll(pageable); 
        return result;
    }

    /**
     *  get one facture by id.
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public FactureDTO findOne(Long id) {
        log.debug("Request to get Facture : {}", id);
        Facture facture = factureRepository.findOne(id);
//        FactureDTO factureDTO = factureMapper.factureToFactureDTO(facture);
        FactureDTO factureDTO = new FactureDTO();
        factureDTO.setClientId(facture.getAmodiataire().getId());
        factureDTO.setClientRef(facture.getAmodiataire().getNumContribuable());
        factureDTO.setClientNom(facture.getAmodiataire().getNomSociete());
        BeanUtils.copyProperties(facture, factureDTO);
        return factureDTO;
    }

    /**
     *  delete the  facture by id.
     */
    public void delete(Long id) {
        log.debug("Request to delete Facture : {}", id);
        Facture facture = factureRepository.findOne(id);
        
        if(facture == null) {
        	log.warn("No facture with id : {}. Exiting", id);
        	return;
        }
        
        if(facture.estFacturee()) {
        	// Update Echeance
        	updateEcheanceInfo(facture);        	        	
        }
        
        boolean estRegle = facture.isEstRegle();
        
        if(estRegle) {
        	revertReglement(facture);
        }
        
        factureRepository.delete(id);
    }

    private void revertReglement(Facture facture) {
    	List<ReglementFacture> reglements = reglementFactureRepository.findReglementFactureByFacture(facture.getId());
    	if(!reglements.isEmpty()) {
    		Reglement reglement = reglements.iterator().next().getReglement();
    		reglementFactureRepository.delete(reglements);
    		reglementRepository.delete(reglement);
    		Optional<Echeance> echeanceOpt = echeanceRepository.findByIdReglement(reglement.getId());
    		
    		// Update the echeance
    		echeanceOpt.ifPresent(echeance -> {
    			echeance.setEstRegle(Boolean.FALSE);
    			echeance.setIdReglement(null);
    			echeanceRepository.save(echeance);
    		});
    	}
    	facture.getIdEcheance();
	}

	private Echeance updateEcheanceInfo(Facture facture) {
    	log.debug("Update echeance infos, before delete facture");
    	Echeance echeance = null;
    	if(facture.getIdEcheance() != null) {
        	echeance = echeanceRepository.findOne(facture.getIdEcheance());	
        }
    	if(echeance != null) {
    		echeance.setIdFacture(null);
    		echeance.setEstFacture(Boolean.FALSE);
    		echeanceRepository.save(echeance);
    	}
    	return echeance;
	}

	/**
     * search for the facture corresponding
     * to the query.
     */
    @Transactional(readOnly = true) 
    public List<FactureDTO> search(String query) {
        
        log.debug("REST request to search Factures for query {}", query);
        return factureRepository.findAll().stream().map(f -> {
        	FactureDTO dto = new FactureDTO();
        	BeanUtils.copyProperties(f, dto);
        	return dto;
        }).collect(Collectors.toList());
    }

	@Override
	public List<FactureDTO> searchFactureFromClientId(Long idClient) {
		
		// Retrouve la liste des facture impayé du client
		List<Facture> listFactures = factureRepository.getFactureImpayeOfClient(idClient);
		
		// Si la liste est vide on retourne null
		if(listFactures == null || listFactures.size() == 0) return null;
		
		// On construit la liste item de la Balanceg
		return listFactures.stream().map(f-> {
			FactureDTO dto = new FactureDTO();
			BeanUtils.copyProperties(f, dto);
			return dto;
		}).collect(Collectors.toList());
	}

	@Override
	public List<Facture> findAllWithoutPage() {
		log.debug("Request to get all Factures");
        List<Facture> result = factureRepository.findAll(); 
        return result;
	}
}
