package com.geospace.gestionaot.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geospace.gestionaot.domain.Amodiataire;
import com.geospace.gestionaot.domain.Echeance;
import com.geospace.gestionaot.domain.Facture;
import com.geospace.gestionaot.domain.Reglement;
import com.geospace.gestionaot.domain.ReglementFacture;
import com.geospace.gestionaot.repository.AmodiataireRepository;
import com.geospace.gestionaot.repository.EcheanceRepository;
import com.geospace.gestionaot.repository.FactureRepository;
import com.geospace.gestionaot.repository.ReglementFactureRepository;
import com.geospace.gestionaot.repository.ReglementRepository;
import com.geospace.gestionaot.service.AmodiataireActionService;
import com.geospace.gestionaot.service.ReglementService;
import com.geospace.gestionaot.service.dto.ReglementDTO;

/**
 * Service Implementation for managing Reglement.
 */
@Service
@Transactional
public class ReglementServiceImpl implements ReglementService{

    private final Logger log = LoggerFactory.getLogger(ReglementServiceImpl.class);
    
    @Autowired
    private ReglementRepository reglementRepository;
    
    @Autowired
    private ReglementFactureRepository reglementFactureRepository;
    
    @Autowired 
    private FactureRepository factureRepository;
    
    @PersistenceContext
    EntityManager em;
    
    @Autowired
    private AmodiataireRepository amodiataireRepository;
    
    @Autowired
    private AmodiataireActionService amodiataireActionService;
    
    @Autowired
    private EcheanceRepository echeanceRepository;
    
//    /**
//     * Save a reglement.
//     * @return the persisted entity
//     */
//    public ReglementDTO save(ReglementDTO reglementDTO) {
//        log.debug("Request to save Reglement : {}", reglementDTO);
//        Reglement reglement = reglementMapper.reglementDTOToReglement(reglementDTO);
//        reglement = reglementRepository.save(reglement);
//        ReglementDTO result = reglementMapper.reglementToReglementDTO(reglement);
//        //realisation du reglement
//        Boolean verifierReglement=paiementReglement(result);
//		log.debug("valeur de la verification du reglement {}",verifierReglement);
//        reglementSearchRepository.save(reglement);        
//        return result;
//        
//    }
    
    /**
     * Save a reglement.
     * @return the persisted entity
     */
    public ReglementDTO save(ReglementDTO reglementDTO) {
        log.debug("Request to save Reglement : {}", reglementDTO);
        List<ReglementFacture> reglementFactures = reglementDTO.getReglementFactures();
        Reglement reglement = new Reglement();
        BeanUtils.copyProperties(reglementDTO, reglement);
//        .reglementDTOToReglement(reglementDTO);
        reglement = reglementRepository.save(reglement);
//        ReglementDTO result = reglementMapper.reglementToReglementDTO(reglement);
        BeanUtils.copyProperties(reglement, reglementDTO);
        Amodiataire amodiataire = amodiataireRepository.findOne(reglementDTO.getClientId());
        reglement.setAmodiataire(amodiataire);
        //realisation du reglement
        Boolean verifierReglement=saveReglementFactures(reglement, reglementFactures);
		log.debug("valeur de la verification du reglement {}",verifierReglement);
		
		// Marquer echeance comme reglee
		boolean marquee = marquerEcheanceReglees(reglement);
		log.debug("Marquer les echeances comme reglee : {}", marquee);
		
		// Update la date de prochaine echance.
		// amodiataireActionService.updateNextEcheance(amodiataire);
        return reglementDTO;
    }

    private boolean marquerEcheanceReglees(Reglement reglement) {
    	List<ReglementFacture> reglementFactures = reglementFactureRepository.findReglementFactureByReglement(reglement.getId());
    	for (ReglementFacture reglementFacture : reglementFactures) {
    		Facture facture = reglementFacture.getFacture();
    		if(facture.isEstRegle()) {
    			Echeance echeance = echeanceRepository.findOne(facture.getIdEcheance());
    			echeance.estRegle(true)
    					.idReglement(reglement.getId());
    			echeanceRepository.save(echeance);
    		}
		}
		return true;
	}

	/**
     * Save all reglementFacture in DataBase
     * @param reglementFactures
     * @return
     */
    private Boolean saveReglementFactures(Reglement reglement,
			List<ReglementFacture> reglementFactures) {
		
    	for (ReglementFacture reglementFacture : reglementFactures) {
    		if(reglementFacture.getMontant() == 0d || reglementFacture.getMontant() == null ) continue;
		
    		// Mise a jour des informations de la facture (montant regle, date reglement, statut est_regle)
    		Facture facture = updateFacture(reglement, reglementFacture);
    		
    		//save reglementfacture
			reglementFacture.setFacture(facture);
			reglementFacture.setReglement(reglement);
			reglementFacture.setRef(reglement.getRef()+"_"+facture.getRef());
			reglementFacture.setDateReglement(reglement.getDateReglement());
			reglementFactureRepository.save(reglementFacture);		
    		
		}    	
    	return true;
    	
	}
    

    private Facture updateFacture(Reglement reglement,
			ReglementFacture reglementFacture) {
    	
    	Facture facture = reglementFacture.getFacture();
		
    	facture.setMontantRegle(facture.getMontantRegle() + reglementFacture.getMontant());
    	if(facture.getMontantRestant() == 0d){
    		// Facture entierement regle
    		 Query query=em.createQuery("UPDATE Facture f SET montant_regle= :montantRegle,dateReglement= :dateReglement,est_regle= :reglee WHERE id= :id");	
             query.setParameter("montantRegle",facture.getMontantRegle());
             query.setParameter("dateReglement",reglement.getDateReglement());
             query.setParameter("id", facture.getId());
             query.setParameter("reglee",true);
             query.executeUpdate();         
    		
    		
    	}else{
    		// Facture regle partiellement
    		Query query=em.createQuery("UPDATE Facture f SET montant_regle= :montantRegle,dateReglement= :dateReglement WHERE id=:id");	
    		query.setParameter("montantRegle",facture.getMontantRegle());
    		query.setParameter("id", facture.getId());
    		query.setParameter("dateReglement", reglement.getDateReglement());
    		query.executeUpdate();
    	}
    	
    	facture = factureRepository.getOne(facture.getId());
    	return facture;
	}

	/**
     *  get all the reglements.
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<Reglement> findAll(Pageable pageable) {
        log.debug("Request to get all Reglements");
        Page<Reglement> result = reglementRepository.findAll(pageable); 
        return result;
    }


   

    /**
     *  get one reglement by id.
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public ReglementDTO findOne(Long id) {
        log.debug("Request to get Reglement : {}", id);
        Reglement reglement = reglementRepository.findOne(id);
        ReglementDTO reglementDTO = new ReglementDTO();
        Amodiataire amodiataire = amodiataireRepository.findOne(reglement.getAmodiataire().getId());
        reglementDTO.setClientId(amodiataire.getId());
        reglementDTO.setClientRef(amodiataire.getNumContribuable());
        reglementDTO.setClientNom(amodiataire.getNomSociete());
//        .reglementToReglementDTO(reglement);
        BeanUtils.copyProperties(reglement, reglementDTO);
        return reglementDTO;
    }

    /**
     *  delete the  reglement by id.
     */
    public void delete(Long id) {
        log.debug("Request to delete Reglement : {}", id);
        // Delete Reglement Factures
        List<ReglementFacture> reglementFactures = reglementFactureRepository.findReglementFactureByReglement(id);
        if(!reglementFactures.isEmpty()) {
        	
        	Facture facture = reglementFactures.iterator().next().getFacture();
        	facture.setMontantRegle(0d);
        	facture.setDateReglement(null);
        	facture.setEstRegle(false);
        	factureRepository.save(facture);
        	
        	reglementFactureRepository.delete(reglementFactures);
        }
        
        // Update Echeance
        Optional<Echeance> echeance = echeanceRepository.findByIdReglement(id);
        
        echeance.ifPresent(e-> {
        	e.setEstRegle(Boolean.FALSE);
        	e.setIdReglement(null);
        	echeanceRepository.save(e);
        });
        reglementRepository.delete(id);
    }

    /**
     * search for the reglement corresponding
     * to the query.
     */
    @Transactional(readOnly = true) 
    public List<ReglementDTO> search(String query) {
        
        log.debug("REST request to search Reglements for query {}", query);
        return new ArrayList<>();
    }
    
    
    
    
    
    
  //effectuer un reglement
  	public Boolean paiementReglement(ReglementDTO reglement){

          
  		// Recherchern la liste des factures impayes ordonne par date de facture
  		List<Facture> listFacturesImpayees=new ArrayList();
  		listFacturesImpayees =listFactureImpayeesByClient(reglement.getClientId());

  		if(listFacturesImpayees==null){
  			return false;
  		}
  		
  		log.debug("nonbre de factures impayes {}"
  				,listFacturesImpayees.size());
  		
  		
  		// regler les factures par date de facture tant que le montant de reglement suffit
  		Double montantReglement = reglement.getMontant();
  		int i = 0;
  		while ((montantReglement > 0) && (i<listFacturesImpayees.size())) {
  			
  			montantReglement = _reglerfacture(listFacturesImpayees.get(i), montantReglement,reglement);
  		
                i++;
  		}

  		return true;
  	}
    
    
    
  //reglement d'une facture d'un client
  	private Double _reglerfacture(Facture facture, Double montantReglement,ReglementDTO reglement) {
  		
  			
  		
  	      double montantRestantARegle=facture.getMontant()-facture.getMontantRegle();
  		
  		if(montantRestantARegle>montantReglement){
  			facture.setMontantRegle(facture.getMontantRegle()+montantReglement);
  			
  			//mettre a jour la facture
  			Query query=em.createQuery("UPDATE Facture f SET montant_regle= :montantARegle,dateReglement= :dateDuReglement WHERE id=:id");	
  			query.setParameter("montantARegle",facture.getMontantRegle());
  			query.setParameter("id", facture.getId());
  			query.setParameter("dateDuReglement", reglement.getDateReglement());
  			query.executeUpdate();
  		
  			
  			//recuperer le reglement correspondant au reglementDTO
  			Reglement reglementClient = new Reglement();
//  			.reglementDTOToReglement(reglement);
  			BeanUtils.copyProperties(reglement, reglementClient);
  			
  			
  			//save reglementfacture
  			ReglementFacture reglementFacture= new ReglementFacture();
  			reglementFacture.setFacture(facture);
  			reglementFacture.setReglement(reglementClient);
  			reglementFacture.setMontant(reglement.getMontant());
  			reglementFacture.setRef(reglementClient.getRef()+facture.getRef());
  			reglementFacture.setDateReglement(reglement.getDateReglement());
  			reglementFactureRepository.save(reglementFacture);
  			
  			return 0.0;

  		}else if(montantRestantARegle<=montantReglement){
             Double resteReglement=montantReglement-montantRestantARegle;
             
             facture.setMontantRegle(facture.getMontantRegle()+montantRestantARegle);
            // facture.setDateReglement(new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
             Query query=em.createQuery("UPDATE Facture f SET montant_regle= :montantARegle,dateReglement= :finiDetteCetteFacture,est_regle= :reglee WHERE id= :id");	
             query.setParameter("montantARegle",facture.getMontantRegle());
             query.setParameter("finiDetteCetteFacture",reglement.getDateReglement());
             query.setParameter("id", facture.getId());
             query.setParameter("reglee",true);
             query.executeUpdate();
           //recuperer le reglement correspondant au reglementDTO
             Reglement reglementClient = new Reglement();
//             .reglementDTOToReglement(reglement);
             BeanUtils.copyProperties(reglement, reglementClient);
             
             
             //save reglementfacture
             ReglementFacture reglementFacture=new ReglementFacture();
             reglementFacture.setFacture(facture);
             reglementFacture.setRef(reglementClient.getRef()+facture.getRef());
             reglementFacture.setMontant(montantRestantARegle);
             reglementFacture.setReglement(reglementClient);
             reglementFacture.setDateReglement(reglement.getDateReglement());
             reglementFactureRepository.save(reglementFacture);
             
             log.debug("montant restant {}",resteReglement);
             
             return resteReglement;
             
  		}

       return null;
  	
  	}
    
    
    
    
  //list des factures d'un client par ordre croissant
	public List<Facture> listFactureImpayeesByClient(Long id) {
		
		
  		Query query=em.createQuery("SELECT f FROM Facture f WHERE client_id= :id AND montant > montant_regle ORDER BY date_facture ASC");
  		query.setParameter("id", id);
  		return query.getResultList();
  	}

	@Override
	public List<ReglementFacture> findReglementFactureByReglement(
			Long reglementId) {
		return reglementFactureRepository.findReglementFactureByReglement(reglementId);
		
	}

	@Override
	public List<ReglementFacture> findReglementFactureByFacture(Long factureId) {
		return reglementFactureRepository.findReglementFactureByFacture(factureId);
	}

    
    
    
    
    
}
