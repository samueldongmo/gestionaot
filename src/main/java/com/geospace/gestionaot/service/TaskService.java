package com.geospace.gestionaot.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import com.geospace.gestionaot.domain.Task;
import com.geospace.gestionaot.domain.User;
import com.geospace.gestionaot.domain.enumeration.TaskStatus;
import com.geospace.gestionaot.repository.TaskRepository;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service Implementation for managing Task.
 */
 
@Service
@Transactional
public class TaskService {

    private final Logger log = LoggerFactory.getLogger(TaskService.class);

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    /**
     * Save a task.
     *
     * @param task the entity to save
     * @return the persisted entity
     */
    public Task save(Task task) {
        log.debug("Request to save Task : {}", task);
        Task result = taskRepository.save(task);
        return result;
    }

    /**
     *  Get all the tasks.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Task> findAll(Pageable pageable) {
        log.debug("Request to get all Tasks");
        Page<Task> result = taskRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one task by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Task findOne(Long id) {
        log.debug("Request to get Task : {}", id);
        Task task = taskRepository.findOne(id);
        return task;
    }

    /**
     *  Delete the  task by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Task : {}", id);
        taskRepository.delete(id);
    }

    /**
     * Find tasks by user and status
     *
     * @param user
     * @param status
     * @return {@link List<Task>}
     */
    public List<Task> findByUserAndStatus(User user, TaskStatus status) {
    	return taskRepository.findByUserAndStatus(user, status);
    }

    /**
     * Count Tasks
     * @param user
     * @param status
     * @return
     */
    public Long countByUserAndStatus(User user, TaskStatus status) {
    	// TODO : Create a count query.
    	int size = findByUserAndStatus(user, status).size();
    	return Long.valueOf(size);
    }

    /**
     * Find Tasks by assignee is current user.
     * @param user
     * @param status
     * @return
     */
    public List<Task> findByAssigneeIsCurrentUserAndStatus(TaskStatus status) {
    	List<Task> tasks = taskRepository.findByAssigneeIsCurrentUserAndStatus(status);
    	return tasks;
    }

    /**
     * @param status
     * @return
     */
    public Long countByAssigneeIsCurrentUserAndStatus(TaskStatus status) {
    	// TODO : Create a count query.
    	int size = findByAssigneeIsCurrentUserAndStatus(status).size();
    	return Long.valueOf(size);
    }
}
