package com.geospace.gestionaot.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.geospace.gestionaot.domain.Reglement;
import com.geospace.gestionaot.domain.ReglementFacture;
import com.geospace.gestionaot.service.dto.ReglementDTO;

/**
 * Service Interface for managing Reglement.
 */
public interface ReglementService {

    /**
     * Save a reglement.
     * @return the persisted entity
     */
    public ReglementDTO save(ReglementDTO reglementDTO);

    /**
     *  get all the reglements.
     *  @return the list of entities
     */
    public Page<Reglement> findAll(Pageable pageable);


    /**
     *  get the "id" reglement.
     *  @return the entity
     */
    public ReglementDTO findOne(Long id);

    /**
     *  delete the "id" reglement.
     */
    public void delete(Long id);

    /**
     * search for the reglement corresponding
     * to the query.
     */
    public List<ReglementDTO> search(String query);

	public List<ReglementFacture> findReglementFactureByReglement(Long reglementId);
	
	public List<ReglementFacture> findReglementFactureByFacture(Long factureId);
}
