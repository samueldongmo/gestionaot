package com.geospace.gestionaot.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geospace.gestionaot.domain.Authority;
import com.geospace.gestionaot.repository.AuthorityRepository;

/**
 * Service Implementation for managing Authority.
 */
@Service
@Transactional
public class AuthorityService {

    private final Logger log = LoggerFactory.getLogger(AuthorityService.class);

    private final AuthorityRepository authorityRepository;

    public AuthorityService(AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    /**
     * Save a authority.
     *
     * @param authority the entity to save
     * @return the persisted entity
     */
    public Authority save(Authority authority) {
        log.debug("Request to save Authority : {}", authority);
        Authority result = authorityRepository.save(authority);
        return result;
    }

    /**
     *  Get all the roles.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Authority> findAll(Pageable pageable) {
        log.debug("Request to get all Roles");
        Page<Authority> result = authorityRepository.findAll(pageable);
        return result;
    }

    @Transactional(readOnly = true)
    public List<Authority> findAll() {
        log.debug("Request to get all Roles");
        return authorityRepository.findAll();
    }

    /**
     *  Get one authority by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Authority findOne(String id) {
        log.debug("Request to get Authority : {}", id);
        Authority authority = authorityRepository.findOne(id);
        return authority;
    }

    /**
     *  Delete the  authority by id.
     *
     *  @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Authority : {}", id);
        authorityRepository.delete(id);
    }
}
