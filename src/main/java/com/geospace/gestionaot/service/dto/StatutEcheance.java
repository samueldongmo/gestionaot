package com.geospace.gestionaot.service.dto;


public enum StatutEcheance {
	/** 
	 * La date d'echeance est deja passee. 
	 * Correspond a la couleur rouge
	 */
	ECHUE,
	/** 
	 * L'echeance est facturee, non echue et en attente de reglement.
	 * Correspond a la couleur Orange
	 */
	FACTUREE,
	/**
	 * L'amodiataire a une echeance proche, mais n'as pas encore recu de facture.
	 * Correspond a la couleur Jaune
	 */
	NON_FACTUREE,
	/**
	 * L'amodiataire a deja regle la facture de son echeance.
	 * Correspond a la couleur Verte.
	 */
	PAYEE
}
