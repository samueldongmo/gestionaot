package com.geospace.gestionaot.service.dto;

import java.util.ArrayList;
import java.util.List;

import com.geospace.gestionaot.domain.Amodiataire;
import com.geospace.gestionaot.domain.Point;

public class AmodiataireCreateDTO extends Amodiataire{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Point> points = new ArrayList<>();
	
	public List<Point> getPoints() {
		return points;
	}
	
	public void setPoints(List<Point> points) {
		this.points = points;
	}
	
	@Override
	public String toString() {
		return "AmodiataireCreateDTO [points=" + points + "]";
	}
	
}
