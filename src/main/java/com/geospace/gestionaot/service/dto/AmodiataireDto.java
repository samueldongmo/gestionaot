package com.geospace.gestionaot.service.dto;

import com.geospace.gestionaot.domain.Amodiataire;

public class AmodiataireDto extends Amodiataire{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private StatutEcheance statutEcheance;

	public StatutEcheance getStatutEcheance() {
		return statutEcheance;
	}

	public void setStatutEcheance(StatutEcheance statutEcheance) {
		this.statutEcheance = statutEcheance;
	}

	@Override
	public String toString() {
		return "AmodiataireDto [statutEcheance=" + statutEcheance + "]";
	}
	
}
