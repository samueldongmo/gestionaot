package com.geospace.gestionaot.service.dto;

import java.io.Serializable;

import com.geospace.gestionaot.domain.Facture;

public class BalancegItemDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String client;
	
	private Long clientId;
	
	private CellItem nonEchu;
	
	private CellItem echuIntervalUn; // 03 months
	
	private CellItem echuIntervalDeux; // 06 months
	
	private CellItem echuIntervalTrois; // 09 months
	
	private CellItem echuIntervalTroisEtPlus; // 09 months and more
	
	private CellItem totalEncour;
	
	
	
	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public CellItem getNonEchu() {
		if(nonEchu == null) nonEchu = new CellItem();
		return nonEchu;
	}

	public void setNonEchu(CellItem nonEchu) {
		this.nonEchu = nonEchu;
	}
	
	public CellItem getEchuIntervalUn() {
		if(echuIntervalUn == null) echuIntervalUn = new CellItem();
		return echuIntervalUn;
	}

	public void setEchuIntervalUn(CellItem echuIntervalUn) {
		this.echuIntervalUn = echuIntervalUn;
	}

	public CellItem getEchuIntervalDeux() {
		if(echuIntervalDeux == null) echuIntervalDeux = new CellItem();
		return echuIntervalDeux;
	}

	public void setEchuIntervalDeux(CellItem echuIntervalDeux) {
		this.echuIntervalDeux = echuIntervalDeux;
	}

	public CellItem getEchuIntervalTrois() {
		if(echuIntervalTrois == null) echuIntervalTrois = new CellItem();
		return echuIntervalTrois;
	}

	public void setEchuIntervalTrois(CellItem echuIntervalTrois) {
		this.echuIntervalTrois = echuIntervalTrois;
	}
	
	public CellItem getEchuIntervalTroisEtPlus() {
		if(echuIntervalTroisEtPlus == null) echuIntervalTroisEtPlus = new CellItem();
		return echuIntervalTroisEtPlus;
	}

	public void setEchuIntervalTroisEtPlus(CellItem echuIntervalTroisEtPlus) {
		this.echuIntervalTroisEtPlus = echuIntervalTroisEtPlus;
	}

	public CellItem getTotalEncour() {
		if(totalEncour == null) totalEncour = new CellItem();
		return totalEncour;
	}

	public void setTotalEncour(CellItem totalEncour) {
		this.totalEncour = totalEncour;
	}


	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}


	public class CellItem implements Serializable{		
		
		private static final long serialVersionUID = 1L;
		
		public CellItem() {
			this.montant = 0d;			
		}

		private Double montant;
		
		private long nombre;

		public Double getMontant() {
			return montant;
		}

		public void setMontant(Double montant) {
			this.montant = montant;
		}

		public long getNombre() {
			return nombre;
		}

		public void setNombre(long nombre) {
			this.nombre = nombre;
		}		
		
		public void addFacture(Facture facture){
			
			if(montant == null) montant = 0d;
			
			montant += facture.getMontantRestant();
			nombre += 1;
			
		}
	}
	

}
