package com.geospace.gestionaot.service.dto;

public class EcheancierSearchRequest {
	private Long idAmodiataire;

	public Long getIdAmodiataire() {
		return idAmodiataire;
	}

	public void setIdAmodiataire(Long idAmodiataire) {
		this.idAmodiataire = idAmodiataire;
	}

	@Override
	public String toString() {
		return "EcheancierSearchRequest [idAmodiataire=" + idAmodiataire + "]";
	}
	
}
