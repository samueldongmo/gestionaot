package com.geospace.gestionaot.service.dto;

import javax.persistence.Column;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the Client entity.
 */
public class ClientDTO implements Serializable {

    private Long id;

    @NotNull
    private String ref;


    @NotNull
    private String nom;

    
    private String nomContact;
    
    
    private String telephoneContact;
    
    
    private String emailContact;
    
    @NotNull
    @Min(value=0)
    private Long delaiEcheance;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
 

    public String getNomContact() {
		return nomContact;
	}

	public void setNomContact(String nomContact) {
		this.nomContact = nomContact;
	}

	public String getTelephoneContact() {
		return telephoneContact;
	}

	public void setTelephoneContact(String telephoneContact) {
		this.telephoneContact = telephoneContact;
	}

	public String getEmailContact() {
		return emailContact;
	}

	public void setEmailContact(String emailContact) {
		this.emailContact = emailContact;
	}

	public Long getDelaiEcheance() {
		return delaiEcheance;
	}

	public void setDelaiEcheance(Long delaiEcheance) {
		this.delaiEcheance = delaiEcheance;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientDTO clientDTO = (ClientDTO) o;

        if ( ! Objects.equals(id, clientDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
	public String toString() {
		return "ClientDTO [id=" + id + ", ref=" + ref + ", nom=" + nom
				+ ", nomContact=" + nomContact + ", telephoneContact="
				+ telephoneContact + ", emailContact=" + emailContact + "]";
	}
}
