package com.geospace.gestionaot.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import com.geospace.gestionaot.domain.Facture;

public class BalancegDTO implements Serializable {
	
	List<BalancegItemDTO> balancegItemDTOs;
	
	BalancegItemDTO totalItem;
	
	LocalDate fromDate;

	public List<BalancegItemDTO> getBalancegItemDTOs() {
		return balancegItemDTOs;
	}

	public void setBalancegItemDTOs(List<BalancegItemDTO> balancegItemDTOs) {
		this.balancegItemDTOs = balancegItemDTOs;
	}

	public BalancegItemDTO getTotalItem() {
		return totalItem;
	}

	public void setTotalItem(BalancegItemDTO totalItem) {
		this.totalItem = totalItem;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public void addFacture(Facture facture){
		
		
		
	}
}
