package com.geospace.gestionaot.service.dto;

import com.geospace.gestionaot.domain.StatutAOT;

public class AmodiataireSearchRequest {
	private String ville;
	private String numeroAutorisation;
	private String nomSociete;
	private String numeroTelephone;
	private String niu;
	private String rccm;
	private int year;
	private StatutAOT statut;
	
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getNumeroAutorisation() {
		return numeroAutorisation;
	}
	public void setNumeroAutorisation(String numeroAutorisation) {
		this.numeroAutorisation = numeroAutorisation;
	}
	public String getNomSociete() {
		return nomSociete;
	}
	public void setNomSociete(String nomSociete) {
		this.nomSociete = nomSociete;
	}
	public String getNumeroTelephone() {
		return numeroTelephone;
	}
	public void setNumeroTelephone(String numeroTelephone) {
		this.numeroTelephone = numeroTelephone;
	}
	public String getNiu() {
		return niu;
	}
	public void setNiu(String niu) {
		this.niu = niu;
	}
	public String getRccm() {
		return rccm;
	}
	public void setRccm(String rccm) {
		this.rccm = rccm;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	public StatutAOT getStatut() {
		return statut;
	}
	public void setStatut(StatutAOT statut) {
		this.statut = statut;
	}
	@Override
	public String toString() {
		return "AmodiataireSearchRequest [ville=" + ville + ", numeroAutorisation=" + numeroAutorisation
				+ ", nomSociete=" + nomSociete + ", numeroTelephone=" + numeroTelephone + ", niu=" + niu + ", rccm="
				+ rccm + ", year=" + year + ", statut=" + statut + "]";
	}
	
}
