package com.geospace.gestionaot.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.geospace.gestionaot.domain.ModeReglement;
import com.geospace.gestionaot.domain.ReglementFacture;


/**
 * A DTO for the Reglement entity.
 */
public class ReglementDTO implements Serializable {

    private Long id;

    @NotNull
    private String ref;

    @NotNull
    @Min(value = 0)
    private Double montant;


    @NotNull
    private LocalDate dateReglement;


    private Long clientId;

    private String clientRef;    
    
    private String clientNom;    
    
    @NotNull
    private String libelle;

    private ModeReglement modeReglement;
    
    private String infoReglement;
    
    private List<ReglementFacture> reglementFactures ;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }
    public Double getMontant() {
        return montant;
    }

    
 	public void setMontant(Double montant) {
        this.montant = montant;
    }
    public LocalDate getDateReglement() {
        return dateReglement;
    }

    public void setDateReglement(LocalDate dateReglement) {
        this.dateReglement = dateReglement;
    }

    public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public String getClientRef() {
		return clientRef;
	}

	public void setClientRef(String clientRef) {
		this.clientRef = clientRef;
	}

//	public List<ReglementFacture> getReglementFactures() {
//		return reglementFactures;
//	}
//
//	public void setReglementFactures(List<ReglementFacture> reglementFactures) {
//		this.reglementFactures = reglementFactures;
//	}

	public String getClientNom() {
		return clientNom;
	}

	public void setClientNom(String clientNom) {
		this.clientNom = clientNom;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<ReglementFacture> getReglementFactures() {
		return reglementFactures;
	}

	public void setReglementFactures(List<ReglementFacture> reglementFactures) {
		this.reglementFactures = reglementFactures;
	}
	
	public ModeReglement getModeReglement() {
		return modeReglement;
	}

	public void setModeReglement(ModeReglement modeReglement) {
		this.modeReglement = modeReglement;
	}

	public String getInfoReglement() {
		return infoReglement;
	}

	public void setInfoReglement(String infoReglement) {
		this.infoReglement = infoReglement;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReglementDTO reglementDTO = (ReglementDTO) o;

        if ( ! Objects.equals(id, reglementDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ReglementDTO{" +
            "id=" + id +
            ", ref='" + ref + "'" +
            ", montant='" + montant + "'" +
            ", dateReglement='" + dateReglement + "'" +
            '}';
    }
}
