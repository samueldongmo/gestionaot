package com.geospace.gestionaot.service.dto;

import javax.validation.constraints.NotNull;

public class ValiderAOT {
	@NotNull
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "ValiderAOT [id=" + id + "]";
	}
	
}
