package com.geospace.gestionaot.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.geospace.gestionaot.domain.Amodiataire;


/**
 * A DTO for the Facture entity.
 */
public class FactureDTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

    @NotNull
    private String ref;


    @NotNull
    private String libelle;


    @NotNull
    private LocalDate dateFacture;


    @NotNull
    private LocalDate dateEcheance;


    @NotNull
    @Min(value = 0)
    private Double montant;
    
    private LocalDate dateReglement;

    private Long clientId;

    private String clientRef;
    
    private String clientNom;
    
    private String clientNomContact;
    
    private int delaiEcheance;

    @NotNull
    @Min(value = 0)
    private Double montantHorsTaxe;
    
    @NotNull
    @Min(value = 0)
    private Double montantRegle;
    
    @NotNull
    private boolean estRegle = false;
    
    @NotNull
    private LocalDate dateDebut;

    @NotNull
    private LocalDate dateFin;
    
    private Long idEcheance;
    
    @NotNull
    private Amodiataire amodiataire;
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
    public LocalDate getDateFacture() {
        return dateFacture;
    }

    public void setDateFacture(LocalDate dateFacture) {
        this.dateFacture = dateFacture;
    }
    public LocalDate getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(LocalDate dateEcheance) {
        this.dateEcheance = dateEcheance;
    }
    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getClientRef() {
        return clientRef;
    }

    public void setClientRef(String clientRef) {
        this.clientRef = clientRef;
    }

	public LocalDate getDateReglement() {
		return dateReglement;
	}

	public void setDateReglement(LocalDate dateReglement) {
		this.dateReglement = dateReglement;
	}

	public Double getMontantHorsTaxe() {
		return montantHorsTaxe;
	}

	public void setMontantHorsTaxe(Double montantHorsTaxe) {
		this.montantHorsTaxe = montantHorsTaxe;
	}

	public Double getMontantRegle() {
		return montantRegle;
	}

	public void setMontantRegle(Double montantRegle) {
		this.montantRegle = montantRegle;
	}

	public boolean isEstRegle() {
		return estRegle;
	}

	public void setEstRegle(boolean estRegle) {
		this.estRegle = estRegle;
	}

	public int getDelaiEcheance() {
		return delaiEcheance;
	}

	public void setDelaiEcheance(int delaiEcheance) {
		this.delaiEcheance = delaiEcheance;
	}

	public String getClientNom() {
		return clientNom;
	}

	public void setClientNom(String clientNom) {
		this.clientNom = clientNom;
	}

	public String getClientNomContact() {
		return clientNomContact;
	}

	public void setClientNomContact(String clientNomContact) {
		this.clientNomContact = clientNomContact;
	}

	public Amodiataire getAmodiataire() {
		return amodiataire;
	}

	public void setAmodiataire(Amodiataire amodiataire) {
		this.amodiataire = amodiataire;
	}
	
	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}
	
	public Long getIdEcheance() {
		return idEcheance;
	}

	public void setIdEcheance(Long idEcheance) {
		this.idEcheance = idEcheance;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FactureDTO factureDTO = (FactureDTO) o;

        if ( ! Objects.equals(id, factureDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
	public String toString() {
		return "FactureDTO [id=" + id + ", ref=" + ref + ", libelle=" + libelle
				+ ", dateFacture=" + dateFacture + ", dateEcheance="
				+ dateEcheance + ", montant=" + montant + ", dateReglement="
				+ dateReglement + ", clientId=" + clientId + ", clientRef="
				+ clientRef + ", montantHorsTaxe=" + montantHorsTaxe
				+ ", montantRegle=" + montantRegle + ", estRegle=" + estRegle
				+ "]";
	}
}
