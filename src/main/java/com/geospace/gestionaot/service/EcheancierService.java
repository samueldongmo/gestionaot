package com.geospace.gestionaot.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import com.geospace.gestionaot.domain.Amodiataire;
import com.geospace.gestionaot.domain.Echeance;
import com.geospace.gestionaot.domain.Echeancier;
import com.geospace.gestionaot.repository.AmodiataireRepository;
import com.geospace.gestionaot.repository.EcheanceRepository;
import com.geospace.gestionaot.repository.EcheancierRepository;
import com.geospace.gestionaot.service.dto.EcheancierSearchRequest;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Echeancier.
 */
@Service
@Transactional
public class EcheancierService {

    private final Logger log = LoggerFactory.getLogger(EcheancierService.class);

    private final EcheancierRepository echeancierRepository;

    private final AmodiataireRepository amodiataireRepository;
    
    private final EcheanceRepository echeanceRepository;
    
    private final EcheanceServiceUtil echeanceServiceUtil;
    
    
	public EcheancierService(EcheancierRepository echeancierRepository, AmodiataireRepository amodiataireRepository,
			EcheanceRepository echeanceRepository, EcheanceServiceUtil echeanceServiceUtil) {
		super();
		this.echeancierRepository = echeancierRepository;
		this.amodiataireRepository = amodiataireRepository;
		this.echeanceRepository = echeanceRepository;
		this.echeanceServiceUtil = echeanceServiceUtil;
	}

	/**
     * Save a echeancier.
     *
     * @param echeancier the entity to save
     * @return the persisted entity
     */
    public Echeancier save(Echeancier echeancier) {
        log.debug("Request to save Echeancier : {}", echeancier);
        Echeancier result = echeancierRepository.save(echeancier);
        return result;
    }

    /**
     *  Get all the echeanciers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Echeancier> findAll(Pageable pageable) {
        log.debug("Request to get all Echeanciers");
        Page<Echeancier> result = echeancierRepository.findAll(pageable);
        result.map(echeancier-> {
            echeancier = loadComplementaryData(echeancier);
            return echeancier;
        });
        return result;
    }

    private Echeancier loadComplementaryData(Echeancier echeancier) {
        if(echeancier == null) return null;

        if(echeancier.getIdAmodiataire() != null) {
            Amodiataire amodiataire = amodiataireRepository.findOne(echeancier.getIdAmodiataire());
            if(amodiataire != null) echeancier.setNomSociete(amodiataire.getNomSociete());
        }
        return echeancier;
    }
    /**
     *  Get one echeancier by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Echeancier findOne(Long id) {
        log.debug("Request to get Echeancier : {}", id);
        Echeancier echeancier = echeancierRepository.findOne(id);
        echeancier = loadComplementaryData(echeancier);
        return echeancier;
    }

    /**
     *  Delete the  echeancier by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Echeancier : {}", id);
        echeancierRepository.delete(id);
    }

	public List<Echeancier> search(EcheancierSearchRequest searchRequest) {
		log.debug("Request to search echeancier : {}", searchRequest.toString());
		Long idAmodiataire = searchRequest.getIdAmodiataire();
		List<Echeancier> echeanciers = echeancierRepository.findByIdAmodiataire(idAmodiataire);
		echeanciers = echeanciers.stream().map(echeancier -> {
			echeancier = loadComplementaryData(echeancier);
			return echeancier;
		}).collect(Collectors.toList());
		return echeanciers;
	}

	public List<Echeance> findEcheances(Long idEcheancier) {
		log.debug("Request to find echeances by id echeancier : {}", idEcheancier);
		List<Echeance> echeances = echeanceRepository.findByIdEcheancier(idEcheancier);
		echeances = echeances.stream().map(echeance -> {
			echeance = echeanceServiceUtil.setComplementaryData(echeance);
			return echeance;
		}).collect(Collectors.toList());
		return echeances;
	}

	public Page<Echeance> findEcheances(Long id, int page, int max) {
		Pageable pageRequest = new PageRequest(page, max);
		Page<Echeance> result = echeanceRepository.findByIdEcheancier(id, pageRequest);
		result.map(echeance -> {
			echeance = echeanceServiceUtil.setComplementaryData(echeance);
			return echeance;
		});
		return result;
	}
}
