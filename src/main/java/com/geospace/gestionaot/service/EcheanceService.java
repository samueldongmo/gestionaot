package com.geospace.gestionaot.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geospace.gestionaot.domain.Amodiataire;
import com.geospace.gestionaot.domain.Echeance;
import com.geospace.gestionaot.domain.Echeancier;
import com.geospace.gestionaot.domain.Facture;
import com.geospace.gestionaot.domain.Reglement;
import com.geospace.gestionaot.repository.AmodiataireRepository;
import com.geospace.gestionaot.repository.EcheanceRepository;
import com.geospace.gestionaot.repository.EcheancierRepository;
import com.geospace.gestionaot.repository.FactureRepository;
import com.geospace.gestionaot.repository.ReglementRepository;

/**
 * Service Implementation for managing Echeance.
 */
@Service
@Transactional
public class EcheanceService {

    private final Logger log = LoggerFactory.getLogger(EcheanceService.class);

    private final EcheanceRepository echeanceRepository;

    private final EcheancierRepository echeancierRepository;

    private final AmodiataireRepository amodiataireRepository;

    private final ReglementRepository reglementRepository;

    private final FactureRepository factureRepository;
    
    private final EcheanceServiceUtil echeanceServiceUtil;
    
    
    public EcheanceService(EcheanceRepository echeanceRepository, EcheancierRepository echeancierRepository,
			AmodiataireRepository amodiataireRepository, ReglementRepository reglementRepository,
			FactureRepository factureRepository, EcheanceServiceUtil echeanceServiceUtil) {
		super();
		this.echeanceRepository = echeanceRepository;
		this.echeancierRepository = echeancierRepository;
		this.amodiataireRepository = amodiataireRepository;
		this.reglementRepository = reglementRepository;
		this.factureRepository = factureRepository;
		this.echeanceServiceUtil = echeanceServiceUtil;
	}

	/**
     * Save a echeance.
     *
     * @param echeance the entity to save
     * @return the persisted entity
     */
    public Echeance save(Echeance echeance) {
        log.debug("Request to save Echeance : {}", echeance);
        Echeance result = echeanceRepository.save(echeance);
        return result;
    }

    /**
     *  Get all the echeances.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Echeance> findAll(Pageable pageable) {
        log.debug("Request to get all Echeances");
        Page<Echeance> result = echeanceRepository.findAll(pageable);
        result.map(echeance-> {
            echeance = echeanceServiceUtil.setComplementaryData(echeance);
            return echeance;
        });
        return result;
    }


    /**
     *  Get one echeance by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Echeance findOne(Long id) {
        log.debug("Request to get Echeance : {}", id);
        Echeance echeance = echeanceRepository.findOne(id);
        echeance = echeanceServiceUtil.setComplementaryData(echeance);
        return echeance;
    }

    /**
     *  Delete the  echeance by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Echeance : {}", id);
        echeanceRepository.delete(id);
    }

	public List<Echeance> findNonRegleeByAmodiataire(Long idAmodiataire) {
		return echeanceRepository.findEcheanceNonReglee(idAmodiataire, Boolean.FALSE);
	}

	public List<Echeance> findNonFactureeByAmodiataire(Long idAmodiataire) {
		return echeanceRepository.findNonFactureeByAmodiataire(idAmodiataire, Boolean.FALSE);
	}
}
