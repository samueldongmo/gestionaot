package com.geospace.gestionaot.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.geospace.gestionaot.domain.Echeance;
import com.geospace.gestionaot.domain.Echeancier;
import com.geospace.gestionaot.service.EcheancierService;
import com.geospace.gestionaot.service.dto.EcheancierSearchRequest;
import com.geospace.gestionaot.web.rest.util.HeaderUtil;
import com.geospace.gestionaot.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Echeancier.
 */
@RestController
@RequestMapping("/api")
public class EcheancierResource {

    private final Logger log = LoggerFactory.getLogger(EcheancierResource.class);

    private static final String ENTITY_NAME = "echeancier";
        
    private final EcheancierService echeancierService;

    public EcheancierResource(EcheancierService echeancierService) {
        this.echeancierService = echeancierService;
    }

    /**
     * POST  /echeanciers : Create a new echeancier.
     *
     * @param echeancier the echeancier to create
     * @return the ResponseEntity with status 201 (Created) and with body the new echeancier, or with status 400 (Bad Request) if the echeancier has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/echeanciers")
    @Timed
    public ResponseEntity<Echeancier> createEcheancier(@Valid @RequestBody Echeancier echeancier) throws URISyntaxException {
        log.debug("REST request to save Echeancier : {}", echeancier);
        if (echeancier.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new echeancier cannot already have an ID")).body(null);
        }
        Echeancier result = echeancierService.save(echeancier);
        return ResponseEntity.created(new URI("/api/echeanciers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /echeanciers : Updates an existing echeancier.
     *
     * @param echeancier the echeancier to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated echeancier,
     * or with status 400 (Bad Request) if the echeancier is not valid,
     * or with status 500 (Internal Server Error) if the echeancier couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/echeanciers")
    @Timed
    public ResponseEntity<Echeancier> updateEcheancier(@Valid @RequestBody Echeancier echeancier) throws URISyntaxException {
        log.debug("REST request to update Echeancier : {}", echeancier);
        if (echeancier.getId() == null) {
            return createEcheancier(echeancier);
        }
        Echeancier result = echeancierService.save(echeancier);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, echeancier.getId().toString()))
            .body(result);
    }

    /**
     * GET  /echeanciers : get all the echeanciers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of echeanciers in body
     */
    @GetMapping("/echeanciers")
    @Timed
    public ResponseEntity<List<Echeancier>> getAllEcheanciers(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Echeanciers");
        Page<Echeancier> page = echeancierService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/echeanciers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /echeanciers/:id : get the "id" echeancier.
     *
     * @param id the id of the echeancier to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the echeancier, or with status 404 (Not Found)
     */
    @GetMapping("/echeanciers/{id}")
    @Timed
    public ResponseEntity<Echeancier> getEcheancier(@PathVariable Long id) {
        log.debug("REST request to get Echeancier : {}", id);
        Echeancier echeancier = echeancierService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(echeancier));
    }

    /**
     * DELETE  /echeanciers/:id : delete the "id" echeancier.
     *
     * @param id the id of the echeancier to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/echeanciers/{id}")
    @Timed
    public ResponseEntity<Void> deleteEcheancier(@PathVariable Long id) {
        log.debug("REST request to delete Echeancier : {}", id);
        echeancierService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
    /**
     * POST /echeanciers/search : search echeanciers by idAmodiataires
     * 
     * @param searchRequest the holder of the search parameters
     * @return the List<Echeancier>
     */
    @PostMapping("/echeanciers/search")
    @Timed
    public ResponseEntity<List<Echeancier>> searchEcheancier(@RequestBody EcheancierSearchRequest  searchRequest) {
    	log.debug("REST request to search echenciers : {}", searchRequest.toString());
    	List<Echeancier> echeanciers = echeancierService.search(searchRequest);
    	return ResponseEntity. <List<Echeancier>> ok(echeanciers);
    }
    
    
    @GetMapping(value="/echeanciers/{id}/echeances", params = {"page", "max"})
    @Timed
    public ResponseEntity<Page<Echeance>> findEcheances(@PathVariable Long id, @RequestParam("page")int page, @RequestParam("max") int max) {
    	log.debug("REST request to search echeances by id echeancier : {}, page = {}, max = {}", id, page, max);
    	Page<Echeance> result = echeancierService.findEcheances(id, page, max);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(result, "/api/echeanciers");
    	return ResponseEntity. <Page<Echeance>> ok(result);
    }
}
