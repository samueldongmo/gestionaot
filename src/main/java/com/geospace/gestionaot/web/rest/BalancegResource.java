package com.geospace.gestionaot.web.rest;

import java.time.LocalDate;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.geospace.gestionaot.service.BalancegService;
import com.geospace.gestionaot.service.dto.BalancegDTO;
import com.geospace.gestionaot.service.dto.BalancegItemDTO;


/**
 * REST controller for getting the audit events.
 */
@RestController
@RequestMapping(value = "/api/balanceg", produces = MediaType.APPLICATION_JSON_VALUE)
public class BalancegResource {

    private final BalancegService balancegService;

    public BalancegResource(BalancegService balancegService) {
        this.balancegService = balancegService;
    }

 
    @RequestMapping(
    		value = "/listitem",
    		method = RequestMethod.GET,
        params = {"fromDate"})
    public List<BalancegItemDTO> getByDates(
        @RequestParam(value = "fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate) {

        return balancegService.findByDates(fromDate);
    }
    
    @RequestMapping(value = "/object",
    		method = RequestMethod.GET,
            params = {"fromDate"})
    public BalancegDTO getBalancegByDates(
        @RequestParam(value = "fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate) {

        return balancegService.findBanlancegByDates(fromDate);
    }

}
