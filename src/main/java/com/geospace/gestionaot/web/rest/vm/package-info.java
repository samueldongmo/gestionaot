/**
 * View Models used by Spring MVC REST controllers.
 */
package com.geospace.gestionaot.web.rest.vm;
