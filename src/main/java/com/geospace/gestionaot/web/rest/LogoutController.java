package com.geospace.gestionaot.web.rest;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.geospace.gestionaot.domain.PersistentAuditEvent;
import com.geospace.gestionaot.repository.PersistenceAuditEventRepository;
import com.geospace.gestionaot.security.SecurityUtils;
import com.geospace.gestionaot.web.vm.Logout;

@RestController
@RequestMapping("/api")
public class LogoutController {

    private final Logger log = LoggerFactory.getLogger(LogoutController.class);
    
    private final PersistenceAuditEventRepository persistenceAuditEventRepository;

    public LogoutController(PersistenceAuditEventRepository persistenceAuditEventRepository) {
    	this.persistenceAuditEventRepository = persistenceAuditEventRepository;
    }
    
    @PostMapping("/logout")
    @Timed
    public ResponseEntity logout(Logout logout) {
    	PersistentAuditEvent auditEvent = new PersistentAuditEvent();
    	auditEvent.setAuditEventDate(LocalDateTime.now());
    	auditEvent.setAuditEventType("LOGOUT_SUCCESS");
    	auditEvent.setPrincipal(SecurityUtils.getCurrentUserLogin());
    	persistenceAuditEventRepository.save(auditEvent);
    	return ResponseEntity.ok().build();
    }
    
}
