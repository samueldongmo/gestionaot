package com.geospace.gestionaot.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.geospace.gestionaot.domain.Echeance;
import com.geospace.gestionaot.service.EcheanceService;
import com.geospace.gestionaot.web.rest.util.HeaderUtil;
import com.geospace.gestionaot.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Echeance.
 */
@RestController
@RequestMapping("/api")
public class EcheanceResource {

    private final Logger log = LoggerFactory.getLogger(EcheanceResource.class);

    private static final String ENTITY_NAME = "echeance";
        
    private final EcheanceService echeanceService;

    public EcheanceResource(EcheanceService echeanceService) {
        this.echeanceService = echeanceService;
    }

    /**
     * POST  /echeances : Create a new echeance.
     *
     * @param echeance the echeance to create
     * @return the ResponseEntity with status 201 (Created) and with body the new echeance, or with status 400 (Bad Request) if the echeance has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/echeances")
    @Timed
    public ResponseEntity<Echeance> createEcheance(@Valid @RequestBody Echeance echeance) throws URISyntaxException {
        log.debug("REST request to save Echeance : {}", echeance);
        if (echeance.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new echeance cannot already have an ID")).body(null);
        }
        Echeance result = echeanceService.save(echeance);
        return ResponseEntity.created(new URI("/api/echeances/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /echeances : Updates an existing echeance.
     *
     * @param echeance the echeance to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated echeance,
     * or with status 400 (Bad Request) if the echeance is not valid,
     * or with status 500 (Internal Server Error) if the echeance couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/echeances")
    @Timed
    public ResponseEntity<Echeance> updateEcheance(@Valid @RequestBody Echeance echeance) throws URISyntaxException {
        log.debug("REST request to update Echeance : {}", echeance);
        if (echeance.getId() == null) {
            return createEcheance(echeance);
        }
        Echeance result = echeanceService.save(echeance);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, echeance.getId().toString()))
            .body(result);
    }

    /**
     * GET  /echeances : get all the echeances.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of echeances in body
     */
    @GetMapping("/echeances")
    @Timed
    public ResponseEntity<List<Echeance>> getAllEcheances(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Echeances");
        Page<Echeance> page = echeanceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/echeances");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /echeances/:id : get the "id" echeance.
     *
     * @param id the id of the echeance to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the echeance, or with status 404 (Not Found)
     */
    @GetMapping("/echeances/{id}")
    @Timed
    public ResponseEntity<Echeance> getEcheance(@PathVariable Long id) {
        log.debug("REST request to get Echeance : {}", id);
        Echeance echeance = echeanceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(echeance));
    }

    /**
     * DELETE  /echeances/:id : delete the "id" echeance.
     *
     * @param id the id of the echeance to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/echeances/{id}")
    @Timed
    public ResponseEntity<Void> deleteEcheance(@PathVariable Long id) {
        log.debug("REST request to delete Echeance : {}", id);
        echeanceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    


    /**
     * GET  /echeances/amodiataire/:id/nonreglee : get all the echeances non reglee of amodiataire with :id.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of echeances in body
     */
    @GetMapping("/echeances/amodiataire/{id}/nonfacture")
    @Timed
    public ResponseEntity<List<Echeance>> getAllEcheancesNonReglee(@PathVariable("id") Long idAmodiataire) {
        log.debug("REST request to get all echeances non reglee of amodiataire with id : ", idAmodiataire);
        List<Echeance> echeances = echeanceService.findNonFactureeByAmodiataire(idAmodiataire);
        return new ResponseEntity<>(echeances, HttpStatus.OK);
    }

}
