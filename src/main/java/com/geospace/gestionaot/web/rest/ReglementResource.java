package com.geospace.gestionaot.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.geospace.gestionaot.domain.Reglement;
import com.geospace.gestionaot.domain.ReglementFacture;
import com.geospace.gestionaot.service.ReglementService;
import com.geospace.gestionaot.service.dto.ReglementDTO;
import com.geospace.gestionaot.web.rest.util.HeaderUtil;
import com.geospace.gestionaot.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Reglement.
 */
@RestController
@RequestMapping("/api")
public class ReglementResource {

    private final Logger log = LoggerFactory.getLogger(ReglementResource.class);
        
    private final ReglementService reglementService;

    
    public ReglementResource(ReglementService reglementService) {
		this.reglementService = reglementService;
	}

	/**
     * POST  /reglements -> Create a new reglement.
     */
    @RequestMapping(value = "/reglements",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ReglementDTO> createReglement(@Valid @RequestBody ReglementDTO reglementDTO) throws URISyntaxException {
        log.debug("REST request to save Reglement : {}", reglementDTO);
        if (reglementDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("reglement", "idexists", "A new reglement cannot already have an ID")).body(null);
        }
        ReglementDTO result = reglementService.save(reglementDTO);
        return ResponseEntity.created(new URI("/api/reglements/" + result.getId()))	
            .headers(HeaderUtil.createEntityCreationAlert("reglement", result.getId().toString()))
            .body(result);
        
    }

    /**
     * PUT  /reglements -> Updates an existing reglement.
     */
    @RequestMapping(value = "/reglements",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ReglementDTO> updateReglement(@Valid @RequestBody ReglementDTO reglementDTO) throws URISyntaxException {
        log.debug("REST request to update Reglement : {}", reglementDTO);
        if (reglementDTO.getId() == null) {
            return createReglement(reglementDTO);
        }
        ReglementDTO result = reglementService.save(reglementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("reglement", reglementDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /reglements -> get all the reglements.
     */
    @RequestMapping(value = "/reglements",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ReglementDTO>> getAllReglements(Pageable pageable, @RequestParam(required = false) String filter)
        throws URISyntaxException {
        
        log.debug("REST request to get a page of Reglements");
        Page<Reglement> page = reglementService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/reglements");
        return new ResponseEntity<>(page.getContent().stream()
            .map(reglement -> {
            	return convertToDTO(reglement);
            })
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

	private ReglementDTO convertToDTO(Reglement reglement) {
		ReglementDTO reglementDTO = new ReglementDTO();
		BeanUtils.copyProperties(reglement, reglementDTO);
		reglementDTO.setClientId(reglement.getAmodiataire().getId());
		reglementDTO.setClientNom(reglement.getAmodiataire().getNomSociete());
		reglementDTO.setClientRef(reglement.getAmodiataire().getNumContribuable());
		return reglementDTO;
	}

    /**
     * GET  /reglements/:id -> get the "id" reglement.
     */
    @RequestMapping(value = "/reglements/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ReglementDTO> getReglement(@PathVariable Long id) {
        log.debug("REST request to get Reglement : {}", id);
        ReglementDTO reglementDTO = reglementService.findOne(id);
        return Optional.ofNullable(reglementDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /reglements/:id -> delete the "id" reglement.
     */
    @RequestMapping(value = "/reglements/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteReglement(@PathVariable Long id) {
        log.debug("REST request to delete Reglement : {}", id);
        reglementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("reglement", id.toString())).build();
    }

    /**
     * SEARCH  /_search/reglements/:query -> search for the reglement corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/reglements/{query:.+}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ReglementDTO> searchReglements(@PathVariable String query) {
        log.debug("Request to search Reglements for query {}", query);
        return reglementService.search(query);
    }
    
    @RequestMapping(
    		value = "/reglementfactures/reglement/{reglementId}",
    		method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReglementFacture> getReglementFacturesByReglement(
    		@PathVariable Long reglementId) {

        return reglementService.findReglementFactureByReglement(reglementId);
    }
    
    @RequestMapping(
    		value = "/reglementfactures/facture/{factureId}",
    		method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReglementFacture> getReglementFacturesByFacture(
    		@PathVariable Long factureId) {

        return reglementService.findReglementFactureByFacture(factureId);
    }
    
}
