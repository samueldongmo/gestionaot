package com.geospace.gestionaot.web.rest;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.websocket.server.PathParam;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.geospace.gestionaot.domain.Amodiataire;
import com.geospace.gestionaot.domain.Point;
import com.geospace.gestionaot.repository.PointRepository;
import com.geospace.gestionaot.service.AmodiataireService;
import com.geospace.gestionaot.service.dto.AmodiataireCreateDTO;
import com.geospace.gestionaot.service.dto.AmodiataireDto;
import com.geospace.gestionaot.service.dto.AmodiataireSearchRequest;
import com.geospace.gestionaot.service.dto.ValiderAOT;
import com.geospace.gestionaot.web.rest.util.HeaderUtil;
import com.geospace.gestionaot.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Amodiataire.
 */
@RestController
@RequestMapping("/api")
public class AmodiataireResource {

    private final Logger log = LoggerFactory.getLogger(AmodiataireResource.class);

    private static final String ENTITY_NAME = "amodiataire";

    private final AmodiataireService amodiataireService;
    private final PointRepository pointRepository;

    public AmodiataireResource(AmodiataireService amodiataireService, PointRepository pointRepository) {
        this.amodiataireService = amodiataireService;
        this.pointRepository = pointRepository;
    }

    /**
     * POST  /amodiataires : Create a new amodiataire.
     *
     * @param amodiataire the amodiataire to create
     * @return the ResponseEntity with status 201 (Created) and with body the new amodiataire, or with status 400 (Bad Request) if the amodiataire has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/amodiataires")
    @Timed
    public ResponseEntity<Amodiataire> createAmodiataire(@RequestBody Amodiataire amodiataire) throws URISyntaxException {
        log.debug("REST request to save Amodiataire : {}", amodiataire);
        if (amodiataire.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new amodiataire cannot already have an ID")).body(null);
        }
        Amodiataire result = amodiataireService.save(amodiataire);
        return ResponseEntity.created(new URI("/api/amodiataires/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping("/amodiataires/dto")
    @Timed
    public ResponseEntity<Amodiataire> createAmodiataire(@RequestBody AmodiataireCreateDTO amodiataireDto) throws URISyntaxException {
        log.debug("REST request to save Amodiataire with points: {}", amodiataireDto);
        if (amodiataireDto.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new amodiataire cannot already have an ID")).body(null);
        }
        Amodiataire result = amodiataireService.saveCustom(amodiataireDto);
        return ResponseEntity.created(new URI("/api/amodiataires/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /amodiataires : Updates an existing amodiataire.
     *
     * @param amodiataire the amodiataire to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated amodiataire,
     * or with status 400 (Bad Request) if the amodiataire is not valid,
     * or with status 500 (Internal Server Error) if the amodiataire couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/amodiataires")
    @Timed
    public ResponseEntity<Amodiataire> updateAmodiataire(@RequestBody AmodiataireCreateDTO amodiataire) throws URISyntaxException {
        log.debug("REST request to update Amodiataire : {}", amodiataire);
        if (amodiataire.getId() == null) {
            return createAmodiataire(amodiataire);
        }
        Amodiataire result = amodiataireService.saveCustom(amodiataire);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, amodiataire.getId().toString()))
            .body(result);
    }

    @PutMapping("/amodiataires/annuler")
    @Timed
    public ResponseEntity<Amodiataire> annulerAmodiataire(@RequestBody Amodiataire amodiataire) throws URISyntaxException {
        log.debug("REST request to update Amodiataire : {}", amodiataire);
        if (amodiataire.getId() == null) {
            return createAmodiataire(amodiataire);
        }
        Amodiataire result = amodiataireService.annulerAmodiataire(amodiataire);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, amodiataire.getId().toString()))
            .body(result);
    }

    /**
     * GET  /amodiataires : get all the amodiataires.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of amodiataires in body
     */
    @GetMapping("/amodiataires")
    @Timed
    public ResponseEntity<List<Amodiataire>> getAllAmodiataires(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Amodiataires");
        Page<Amodiataire> page = amodiataireService.findAmodiataireValider(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/amodiataires");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /amodiataires/enattente : get all the amodiataires en attente.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of amodiataires in body
     */
    @GetMapping("/amodiataires/enattente")
    @Timed
    public ResponseEntity<List<Amodiataire>> getAllAmodiatairesEnAttente(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Amodiataires");
        Page<Amodiataire> page = amodiataireService.findAmodiataireEnAttente(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/amodiataires");
        return new ResponseEntity<>(page.getContent(), HttpStatus.OK);
    }

    /**
     * GET  /amodiataires/dto : get all the amodiataires.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of amodiataires in body
     */
    @GetMapping("/amodiataires/dto")
    @Timed
    public ResponseEntity<List<AmodiataireDto>> getAllAmodiataireDtos(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Amodiataires");
//        Page<Amodiataire> page = amodiataireService.findAll(pageable);
        Page<AmodiataireDto> page = amodiataireService.findAllDto(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/amodiataires");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /amodiataires/:id : get the "id" amodiataire.
     *
     * @param id the id of the amodiataire to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the amodiataire, or with status 404 (Not Found)
     */
    @GetMapping("/amodiataires/{id}")
    @Timed
    public ResponseEntity<AmodiataireCreateDTO> getAmodiataire(@PathVariable Long id) {
        log.debug("REST request to get Amodiataire : {}", id);
        Amodiataire amodiataire = amodiataireService.findOne(id);
        List<Point> points = pointRepository.findByAmodiataire(amodiataire);
        AmodiataireCreateDTO dto = new AmodiataireCreateDTO();
        BeanUtils.copyProperties(amodiataire, dto);
        dto.setPoints(points);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dto));
    }
    /**
     * GET  /amodiataires/:id : get the "id" amodiataire.
     *
     * @param id the id of the amodiataire to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the amodiataire, or with status 404 (Not Found)
     */
    @GetMapping("/amodiataires/{id}/dto")
    @Timed
    public ResponseEntity<AmodiataireDto> getAmodiataireDto(@PathVariable Long id) {
        log.debug("REST request to get Amodiataire Dto : {}", id);
        AmodiataireDto dto =  amodiataireService.findOneAmodiataireDto(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dto));
    }

    /**
     * DELETE  /amodiataires/:id : delete the "id" amodiataire.
     *
     * @param id the id of the amodiataire to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/amodiataires/{id}")
    @Timed
    public ResponseEntity<Void> deleteAmodiataire(@PathVariable Long id) {
        log.debug("REST request to delete Amodiataire : {}", id);
        amodiataireService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  /amodiataires/ol/:id/geojson : get the "id" geojson informations.
     *
     * @param id the id of the amodiataire to retrieve's json
     * @return the ResponseEntity with status 200 (OK) and with body the amodiataire, or with status 404 (Not Found)
     */
    @GetMapping("/amodiataires/ol/{id}/geojson")
    @Timed
    public ResponseEntity<String> getAmodiataireGeoJson(@PathVariable Long id) throws JSONException{
        log.debug("REST request to get Amodiataire : {}", id);
        Amodiataire amodiataire = amodiataireService.findOne(id);
        JSONObject main = amodiataireService.getAmodiataireGeoJson(amodiataire);
        return ResponseEntity.ok(main.toString());
    }

    /**
     * GET  /amodiataires/ol/all/geojson : get all geojsons informations.
     *
     * @param
     * @return the ResponseEntity with status 200 (OK) and with body the list of JSONObjects available, or with status 404 (Not Found)
     */
    @GetMapping("/amodiataires/ol/all/geojson")
    @Timed
    public ResponseEntity<JSONObjectWrapper> getAmodiataireGeoJsons() throws JSONException{
        log.debug("REST request to get all GeoJsons of all Amodiataire");
        List<JSONObject> geoJsons = amodiataireService.getAmodiataireGeoJsons();
        JSONObjectWrapper wrapper = new JSONObjectWrapper(geoJsons);
        return ResponseEntity.ok(wrapper);
    }


    @PostMapping("/amodiataires/search")
    @Timed
    public ResponseEntity<List<Amodiataire>> search(@RequestBody AmodiataireSearchRequest searchDTO) {
    	List<Amodiataire> amodiataires = amodiataireService.search(searchDTO);
    	return ResponseEntity.ok(amodiataires);
    }

    @GetMapping("/amodiataire/{id}/points")
    @Timed
    public ResponseEntity<List<Point>> loadPoints(@PathParam("id") Long id) {
    	List<Point> points = amodiataireService.loadByAmodiataireId(id);
    	return ResponseEntity.ok(points);
    }


    @PostMapping("/amodiataires/valider")
    @Timed
    public ResponseEntity<List<Amodiataire>> valider(@RequestBody ValiderAOT validerAOT) {
    	amodiataireService.validerAOT(validerAOT.getId());
    	return ResponseEntity.ok().build();
    }


    @GetMapping("/amodiataires/filter/{filter}")
    @Timed
    public ResponseEntity<List<Amodiataire>> loadAmodiatiares(@PathParam("filter") String filter) {
        List<Amodiataire> amodiataires = amodiataireService.filterAmodiataires(filter);
    	return ResponseEntity.ok(amodiataires);
    }


    /**
     * @author bwa
     *
     */
    public class JSONObjectWrapper implements Serializable{
    	/**
		 *
		 */
		private static final long serialVersionUID = 1L;
		private List<JSONObject> geoJsons = new ArrayList<>();

		public JSONObjectWrapper(List<JSONObject> geoJsons) {
			super();
			this.geoJsons = geoJsons;
		}

		public List<JSONObject> getGeoJsons() {
			return geoJsons;
		}

		public void setGeoJsons(List<JSONObject> geoJsons) {
			this.geoJsons = geoJsons;
		}

		@Override
		public String toString() {
			return "JSONObjectWrapper [geoJsons=" + geoJsons + "]";
		}
    }
}
