package com.geospace.gestionaot.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.geospace.gestionaot.domain.Facture;
import com.geospace.gestionaot.service.FactureService;
import com.geospace.gestionaot.service.dto.FactureDTO;
import com.geospace.gestionaot.web.rest.util.HeaderUtil;
import com.geospace.gestionaot.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Facture.
 */
@RestController
@RequestMapping("/api")
public class FactureResource {

    private final Logger log = LoggerFactory.getLogger(FactureResource.class);
        
    private final FactureService factureService;
    
    public FactureResource(FactureService factureService) {
		this.factureService = factureService;
	}

	/**
     * POST  /factures -> Create a new facture.
     */
    @RequestMapping(value = "/factures",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FactureDTO> createFacture(@Valid @RequestBody FactureDTO factureDTO) throws URISyntaxException {
        log.debug("REST request to save Facture : {}", factureDTO);
        if (factureDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("facture", "idexists", "A new facture cannot already have an ID")).body(null);
        }
        FactureDTO result = factureService.save(factureDTO);
        return ResponseEntity.created(new URI("/api/factures/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("facture", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /factures -> Updates an existing facture.
     */
    @RequestMapping(value = "/factures",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FactureDTO> updateFacture(@Valid @RequestBody FactureDTO factureDTO) throws URISyntaxException {
        log.debug("REST request to update Facture : {}", factureDTO);
        if (factureDTO.getId() == null) {
            return createFacture(factureDTO);
        }
        FactureDTO result = factureService.save(factureDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("facture", factureDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /factures -> get all the factures.
     */
    @RequestMapping(value = "/factures",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<FactureDTO>> getAllFactures(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Factures");
        Page<Facture> page = factureService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/factures");
        return new ResponseEntity<>(page.getContent().stream()
            .map(facture -> {
            	return convertToDTO(facture);
            })
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

	private FactureDTO convertToDTO(Facture facture) {
		FactureDTO factureDTO = new FactureDTO();
		BeanUtils.copyProperties(facture, factureDTO,"amodiataire");
    	factureDTO.setClientId(facture.getAmodiataire().getId());
    	factureDTO.setClientNom(facture.getAmodiataire().getNomSociete());
    	factureDTO.setClientRef(facture.getAmodiataire().getNumContribuable());
		return factureDTO;
	}

    /**
     * GET  /factures -> get all the factures.
     */
    @RequestMapping(value = "/factures/withoutpage",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public List<FactureDTO> getAllFacturesWithoutPage()
        throws URISyntaxException {
        log.debug("REST request to get all Clients");
        List<Facture> result = factureService.findAllWithoutPage(); 
        return result.stream().map(facture -> {
        	return convertToDTO(facture);
        }).collect(Collectors.toList());
        //return result;
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/clients");
//        return new ResponseEntity<>(r.stream()
//            .map(clientMapper::clientToClientDTO)
//            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }
    
    /**
     * GET  /factures/:id -> get the "id" facture.
     */
    @RequestMapping(value = "/factures/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FactureDTO> getFacture(@PathVariable Long id) {
        log.debug("REST request to get Facture : {}", id);
        FactureDTO factureDTO = factureService.findOne(id);
        return Optional.ofNullable(factureDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /factures/:id -> delete the "id" facture.
     */
    @RequestMapping(value = "/factures/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteFacture(@PathVariable Long id) {
        log.debug("REST request to delete Facture : {}", id);
        factureService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("facture", id.toString())).build();
    }

    /**
     * SEARCH  /_search/factures/:query -> search for the facture corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/factures/{query:.+}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<FactureDTO> searchFactures(@PathVariable String query) {
        log.debug("Request to search Factures for query {}", query);
        return factureService.search(query);
    }
    
    /**
     * GET  /factures/client/:idclient -> get the "idclient" client.
     */
    @RequestMapping(value = "/factures/client/",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE,
        params = {"idclient"})
    @Timed
    public List<FactureDTO> getFacturesFromClient( @RequestParam(value = "idclient") @NumberFormat Long idclient) {
        log.debug("REST request to get Facture : {}", idclient);
        return factureService.searchFactureFromClientId(idclient);
    }
}
